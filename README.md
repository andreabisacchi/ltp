# Unibo-LTP

Unibo-LTP is an experimental implementation of the LTP protcol for DTN space networks, carried out at University of Bologna. 
It is essentially compliant with RFC5326 and CCSDS blue book on LTP, but it supports only monochrome blocks.
It also contains a few experimental features, such as:
1) a new experimental color (orange)
2) max session timers (to close stalled sessions after the max session timer as elapsed)
3) max intersegment timer (to close green and orange Rx-sessions after the max intersegment timer has elapsed)
4) the possibility to set the default session color in a specific span
5) a token-bucket traffic shaper, to control both the avaerage Tx rate and the max burst dimension 
 
Unibo-LTP works as an ION plug-in. It is independently built (it has its own Makefile), configured (spans.json file) and launched (in alternative to ION-LTP, which must be disabled first). Details on how to compile and use are given in next sessions.

#### Copyright

Copyright (c) 2021, University of Bologna. 

#### License

Unibo-LTP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.  
Unibo-CGR is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.  
You should have received a copy of the GNU General Public License along with Unibo-LTP. If not, see <http://www.gnu.org/licenses/>.

#### Additional information
_Compilation_

Unibo-LTP requires a libjson parser. On Debian machines you can install it with the follwing command
sudo apt-get install libjson-c-dev

Then from the ltp directory, which contains the Unibo-LTP Makefile:
make ION_DIR=/path/to/ION (e.g. make ION_DIR=/home/jhon/sources/ion-3.7.3)
add DEBUG=1 to build for debugging
sudo make install 

_Compilation Switches_

A few defaults, such as max session timer, max inter-segment timer, etc. are set in /ltp/src/config.h 

_Configuration file (spans.json)_
Unibo-LTP configuration is in the file "spans.json" (it must be in your working directory). Settings here are the same 
usually inserted in the the "ltpadmin" section of ION configuration files. Although the content is basically the same, 
but new features, the format is different. See the example file. 
Note that you can alternatively select UDP or ECLSA as lower protcols for each outgoing span, while UDP and ECLSA can be 
concurrently in use ingoing. This is justified by the fact that, as in ION, in Unibo-LTP there is one receiver serving 
all neighbors and a dedicated "span" to each neighbor (which can alternatively use UDP or ECLSA).    

_Use in ION_

Preliminary steps:
1) Configure ION as if you wanted to use ION-LTP and test the correctness of your configuration before going on. 
2) Translate your "ltpadmin" configuration into the spans.json file (start from the template).
3) Disable ION-LTP. A convenienty way is just to comment the "enable" line in ltpadmin.

Regular steps:
4) start ION as usual
5) start Unibo-LTP with this syntax (add --log to enable logs)
prompt>UniboLTP nodenumber --log
e.g. UniboLTP 2 --log

_Logs_

Unibo-LTP was designed with research and teaching in mind, thus logs are one of the most distinctive features.
They can be enabled at run-time.

#### Credits

Andrea Bisacchi (main author), andrea.bisacchi5@studio.unibo.it

Davide Filoni (timers), davide.filoni2@studio.unibo.it

Carlo Caini (supervisor), carlo.caini@unibo.it
