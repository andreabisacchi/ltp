/** \file span.c
 *
 * \brief This file contains the implementations of the LTP span functions
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 * \authors Davide Filoni, davide.filoni2@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <stdint.h>
#include <pthread.h>
#include <sys/eventfd.h>
#include <sys/select.h>

#include "span.h"

#include "../logger/logger.h"
#include "../list/list.h"
#include "../LTPsegment/segment.h"
#include "../LTPsession/sessionStruct.h"
#include "../LTPsession/session.h"
#include "../generic/generic.h"
#include "../adapters/protocol/lowerProtocol.h"
#include "../adapters/protocol/upperProtocol.h"
#include "../adapters/protocol/upperProtocolReceivedData.h"
#include "../config.h"
#include "../timer/timer.h"
#include "../timer/timerStruct.h"
#include "../contact/contact.h"
#include "../range/range.h"
#include "../sender/sender.h"

static List activeSpans = empty_list;
static unsigned int ownqtime;

/***** PRIVATE FUNCTIONS *****/
static void _checkSpanStatus(TimerID timerID, void* _span);
static bool _contactIsInThePast(void* data, size_t size);
static bool _rangeIsInThePast(void* data, size_t size);
static void _freezeAllSessions(void* span);
static void _resumeAllSessions(void* span);
static void _destroyAllSessions(void* _span);
static void* _mainCongestionControl(void* _span);
static void* _mainGetterDataFromUpperProtocol(void* _span);
static void* _mainSpanThread(void* _span);
static void _handleSendNewBlock(LTPSpan* span);
static void _handleSignalSegment(LTPSpan* span);
static void _handleSegmentToSend(LTPSpan* span);
/***** HANDLER FUNCTIONS TO SEGMENT TO SEND *****/
static void _handlerResendCS(TimerID timer, void* sentStruct);
static void _handlerResentCS(SendStruct* sentStruct);
static void _handlerResendCR(TimerID timer, void* sentStruct);
static void _handlerResentCR(SendStruct* sentStruct);
static void _handlerResendCP(TimerID timer, void* sentStruct);
static void _handlerResentCP(SendStruct* sentStruct);
static void _handlerResendRS(TimerID timer, void* sentStruct);
static void _handlerResentRS(SendStruct* sentStruct);
/***** HANDLER FUNCTIONS *****/
static int _compareContacts(void* _newContact, size_t newContactSize, void* _contact, size_t contactSize);
static int _compareRanges(void* _newRange, size_t newRangeSize, void* _range, size_t rangeSize);
static void _handlerCheckForTerminatedSession(TimerID timerID, void* _span);
static bool _checkIsToRemoveTerminatedSession(void* data, size_t size);
static void _cleanSession(void *session, size_t size);
static void _closeSpan(void *span, size_t size);
/***** COMPARE FUNCTIONS *****/
static int _findActiveSpanFromNodeNumber(void* _nodeNumber, size_t nodeNumberSize, void* _span, size_t spanSize);
static int _findSpanFromTXSessionID(void* _sessionID, size_t sessionIDSize, void* _span, size_t spanSize);
static int _findActiveTXSessionFromSessionID(void* _sessionID, size_t sessionIDSize, void* _session, size_t sessionSize);
static int _findSpanFromTerminatedTXSessionID(void* _sessionID, size_t sessionIDSize, void* _span, size_t spanSize);
static int _findTerminatedTXSessionFromSessionID(void* _sessionID1, size_t sessionID1Size, void* _sessionID2, size_t sessionID2Size);


void setOwnqTime(unsigned int _ownqtime) {
	ownqtime = _ownqtime;
}

unsigned int getOwnqTime() {
	return ownqtime;
}

LTPSpan* getSpanFromNodeNumber(unsigned long long nodeNumber) {
	LTPSpan* span = (LTPSpan*) list_get_pointer_data(activeSpans, &nodeNumber, sizeof(nodeNumber), _findActiveSpanFromNodeNumber);
	return span;
}

void closeAllSpans() {
	list_for_each(activeSpans, _closeSpan);
	list_destroy(&activeSpans);
}

void closeSpan(LTPSpan* span) {
	if ( span == NULL ) return;

	doLog("Span %d:\tClosing...", span->nodeNumber);

	pthread_mutex_lock(&(span->lock));

	span->isActive = false; // No more active
	span->isRemoved = true; // Span removed

	stopTimer(span->timerCleanerTerminatedSessions); // Stop timer for cleaning

	destroyUpperProtocolReceivedData(&span->dataPassedFromUpperProtocol);

	// Cancel the threads
	pthread_cancel(span->mainThread);
	pthread_cancel(span->upperProtocolThread);
	pthread_cancel(span->congestionControlThread);

	// Close the FD used for signaling
	close(span->dataFromUpperProtocolEventFD);
	close(span->signalSegmentsEventFD);
	close(span->segmentsToSendEventFD);

	//destroy the list
	list_destroy(&(span->structsToSendBySpanThread));
	list_destroy(&(span->receivedSignalSegmentsForSpanThread));


	// Destroy the condition variable
	pthread_cond_destroy(&(span->stoppedCondition));
	pthread_cond_destroy(&(span->waitingForCongestionControlCondition));

	sem_destroy(&span->freeTxConcurrentSessionsSemaphore);

	stopTimer(span->timerCheckChangesOnContactsAndRanges); // Stop timer for changeStatus event on contacts & ranges
	list_destroy(&(span->contactsList)); // Destroy contact list
	list_destroy(&(span->rangesListOut)); // Destroy range list
	list_destroy(&(span->rangesListIn)); // Destroy range list

	// Remove active RX sessions
	list_for_each(span->activeRxSessions, _cleanSession);
	list_destroy(&(span->activeRxSessions));

	// Remove active TX sessions
	list_for_each(span->activeTxSessions, _cleanSession);
	list_destroy(&(span->activeTxSessions));

	// Remove terminated RX sessions
	list_destroy(&(span->recentlyClosedRxSessionIDs));

	// Remove terminated TX sessions
	list_destroy(&(span->recentlyClosedTxSessionIDs));

	_free(span->lowerProtocol);

	pthread_mutex_destroy(&(span->lock));
}

void closeAndRemoveSpan(unsigned long long nodeNumber) {
	LTPSpan* span = getSpanFromNodeNumber(nodeNumber);
	closeSpan(span);
	list_remove_data(&activeSpans, &nodeNumber, sizeof(nodeNumber), _findActiveSpanFromNodeNumber);
}

LTPSpan* createSpan(unsigned long long nodeNumber, unsigned int maxTxConcurrentSessions, unsigned int maxRxConcurrentSessions, unsigned int payloadSegmentSize, unsigned int remoteDelay, unsigned int ipAddress, unsigned short portNumber, long unsigned int maxBurstSize, LTPSessionColor sessionColor, const char* lowerProtocol, const char* lowerProtocolInitString) {
	LTPSpan span;
	// Set new span data
	span.nodeNumber = nodeNumber;
	span.maxTxConcurrentSessions = maxTxConcurrentSessions;
	span.maxRxConcurrentSessions = maxRxConcurrentSessions;
	span.payloadSegmentSize = payloadSegmentSize;
	span.ipAddress = ipAddress;
	span.portNumber = portNumber;
	span.currentMaxBurstSize = span.maxBurstSize = maxBurstSize;
	span.sessionColor = sessionColor;

	span.lowerProtocol = _malloc(strlen(lowerProtocol) + 1);
	memcpy(span.lowerProtocol, lowerProtocol, strlen(lowerProtocol) + 1);

	if ( !initOutLowerLevel(lowerProtocol, nodeNumber, ipAddress, portNumber, lowerProtocolInitString) ) {
		_free(span.lowerProtocol);
		return NULL;
	}

	span.currentRxBufferUsed = 0;
	sem_init(&span.freeTxConcurrentSessionsSemaphore, 0, maxTxConcurrentSessions);

	span.remoteDelay = remoteDelay;
	span.rtoTime = ownqtime + remoteDelay;

	span.xmitRate = 0;
	span.bytesToSend = 0;
	pthread_cond_init(&span.waitingForCongestionControlCondition, NULL);

	initUpperProtocolReceivedData(&span.dataPassedFromUpperProtocol);

	// Set defaults & init components

	span.contactsList = empty_list;
	span.rangesListOut = empty_list;
	span.rangesListIn = empty_list;
	span.timerCheckChangesOnContactsAndRanges = 0;

	span.activeRxSessions = empty_list;
	span.activeTxSessions = empty_list;

	span.recentlyClosedRxSessionIDs = empty_list;
	span.recentlyClosedTxSessionIDs = empty_list;

	span.receivedSignalSegmentsForSpanThread = empty_list;
	span.dataFromUpperProtocolEventFD = eventfd(0, 0);

	span.structsToSendBySpanThread = empty_list;
	span.segmentsToSendEventFD= eventfd(0, 0);

	span.signalSegmentsEventFD = eventfd(0, 0);

	pthread_mutexattr_t ma;
	pthread_mutexattr_init(&ma);
	pthread_mutexattr_settype(&ma, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&(span.lock), &ma);

	span.isActive = true;
	span.isRemoved = false;

	span.isStopped = true;
	pthread_cond_init(&span.stoppedCondition, NULL);

	#if DELETE_SESSION_WHEN_SPAN_PAUSED
		span.Functions.handlerSpanPaused = _destroyAllSessions;
		span.Functions.handlerSpanResumed = NULL;
 	#else
		span.Functions.handlerSpanPaused = _freezeAllSessions;
		span.Functions.handlerSpanResumed = _resumeAllSessions;
	#endif

	// Append span to activeSpan list & save new pointer
	LTPSpan* newSpan = (LTPSpan*) list_append(&activeSpans, &span, sizeof(span));

	// Start span threads

	pthread_attr_t pthreadAttribute;
	pthread_attr_init(&pthreadAttribute);
	pthread_attr_setdetachstate(&pthreadAttribute, PTHREAD_CREATE_DETACHED);

	doLog("Span %d:\tCreating...", nodeNumber);

	pthread_create(&(newSpan->mainThread), &pthreadAttribute, _mainSpanThread, newSpan);
	pthread_create(&(newSpan->upperProtocolThread), &pthreadAttribute, _mainGetterDataFromUpperProtocol, newSpan);
	pthread_create(&(newSpan->congestionControlThread), &pthreadAttribute, _mainCongestionControl, newSpan);

	// Start timer for cleaning terminated sessions
	newSpan->timerCleanerTerminatedSessions = startTimer(1000*RECENTLY_CLOSED_LIST_UPDATE_TIME, _handlerCheckForTerminatedSession, newSpan);

	return newSpan;
}

void signalSpanNewSegmentReceived(LTPSegment* receivedSegment) {
	LTPSpan* span = NULL;

	// Find the span. If the span is not found the segment is dropped
	// For signalling segments the knowledge of the span is necessary to know which span thread
	// the segment must be passed
	// For data segments to contribute to identify the RX_buffer
	if ( isRXSession(*receivedSegment)) { // If the segment is related to a local RX_SESSION
		span = getSpanFromNodeNumber(receivedSegment->header.sessionID.sessionOriginator); // find the span using sessionOriginator
		if ( span == NULL ) { // span NULL -> span not found
			doLog("Info:\tNot found span for %d-%d. Ignoring segment", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
			destroySegment(receivedSegment);
			return;
		}

	} else if ( isTXSession(*receivedSegment) && getMyNodeNumber() == receivedSegment->header.sessionID.sessionOriginator) { // If the segment is related to a local TX_SESSION

		span = (LTPSpan*) list_get_pointer_data(activeSpans, &receivedSegment->header.sessionID, sizeof(receivedSegment->header.sessionID), _findSpanFromTXSessionID);

		if ( span == NULL ) { // If span is not found -> reply if is in recently terminated sessions
				span = (LTPSpan*) list_get_pointer_data(activeSpans, &receivedSegment->header.sessionID, sizeof(receivedSegment->header.sessionID), _findSpanFromTerminatedTXSessionID);
		}
		if ( span == NULL ) { // span NULL -> span not found
			doLog("Info:\tNot found span for %d-%d. Ignoring segment", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
			destroySegment(receivedSegment);
			return;
		}
	} else {
		doLog("Error:\tRecieved malformed segment or segment destineted to another node");
		destroySegment(receivedSegment);
		return;
	}
	// Now we know the span



	// Handle Red Data segments
	if ( isRedDataSegment(*receivedSegment) ) { // If is a data segment -> add it to the session or create one if first segment
		if ( ! signalSessionNewDataSegmentReceived(span, receivedSegment, SessionColorRed) ) { // If false -> ignore this segment
			destroySegment(receivedSegment);
			return;
		}
	}

	// Handle Green Data segments
	if ( isGreenDataSegment(*receivedSegment) ) { // If is a green data segment
		if ( ! signalSessionNewDataSegmentReceived(span, receivedSegment, SessionColorGreen) ) { // If false -> ignore this segment
			destroySegment(receivedSegment);
			return;
		}
	}

	// Handle orange data segments
	if ( isOrangeDataSegment(*receivedSegment) ) { // If is an orange data segment
		if ( ! signalSessionNewDataSegmentReceived(span, receivedSegment, SessionColorOrange) ) { // If false -> ignore this segment
			destroySegment(receivedSegment);
			return;
		}
	}

	// Pass signal segments to span thread
	if ( isSignalSegment(*receivedSegment) ) {
		// Queue the signal segment to let span thread handle it
		pthread_mutex_lock(&span->lock);
		list_append(&(span->receivedSignalSegmentsForSpanThread), receivedSegment, sizeof(LTPSegment)); // Add the segment to the list
		pthread_mutex_unlock(&span->lock);

		// Signal the presence of a new segment in the list
		uint64_t dummy = 1; // 1 because I have 1 new segment to process
		write(span->signalSegmentsEventFD, &dummy, sizeof(dummy));
	}

}


void forwordingSpanNewSendStructToSend(TimerID timerID, void* _sendStruct) {
	SendStruct* sendStruct= (SendStruct*) _sendStruct;
	LTPSpan* span = sendStruct->span;

	pthread_mutex_lock(&(span->lock));
	list_append(&(span->structsToSendBySpanThread), &sendStruct, sizeof(SendStruct)); // Add the send struct to the list
	pthread_mutex_unlock(&(span->lock));

	// Signal the presence of a new segment in the list
	uint64_t dummy = 1; // 1 because I have 1 new segment to process
	write(span->segmentsToSendEventFD, &dummy, sizeof(dummy));

}


void forwordingSpanNewCancelStructToSend(CancelStruct* cancelStruct) {
	LTPSpan* span = cancelStruct->session->span;

	pthread_mutex_lock(&(span->lock));
	list_append(&(span->structsToSendBySpanThread), cancelStruct, sizeof(CancelStruct)); // Add the cancel struct to the list
	pthread_mutex_unlock(&(span->lock));

	// Signal the presence of a new segment in the list
	uint64_t dummy = 1; // 1 because I have 1 new segment to process
	write(span->segmentsToSendEventFD, &dummy, sizeof(dummy));

}


void addContactToSpan(Contact* contact, LTPSpan* span) { // It adds only contacts to the wanted node (span). Example: contact 1 -> 2 is added only if the current node is 1 and the span is to 2
	if ( contact == NULL || span == NULL ) return;

	if ( contact->fromNode == getMyNodeNumber() && contact->toNode == span->nodeNumber ) {
		pthread_mutex_lock(&span->lock);
		list_push_ordered(&(span->contactsList), contact, sizeof(Contact), _compareContacts);
		pthread_mutex_unlock(&span->lock);
		_checkSpanStatus(0, span);
	}
}

void removeContactFromSpan(Contact* contact, LTPSpan* span) {
	if ( contact == NULL || span == NULL ) return;

	pthread_mutex_lock(&span->lock);
	list_remove_data(&(span->contactsList), contact, sizeof(Contact), _compareContacts);
	pthread_mutex_unlock(&span->lock);
	_checkSpanStatus(0, span);
}

void removeAllContactsFromSpan(LTPSpan* span) {
	if ( span == NULL ) return;

	pthread_mutex_lock(&span->lock);
	list_destroy(&(span->contactsList));
	pthread_mutex_unlock(&span->lock);
	_checkSpanStatus(0, span);
}

void addRangeToSpan(Range* range, LTPSpan* span) {
	if ( range == NULL || span == NULL ) return;

	pthread_mutex_lock(&span->lock);
	if ( range->fromNode == span->nodeNumber && range->toNode == getMyNodeNumber() )
		list_push_ordered(&(span->rangesListOut), range, sizeof(Range), _compareRanges);
	if ( range->fromNode == getMyNodeNumber() && range->toNode == span->nodeNumber )
		list_push_ordered(&(span->rangesListIn), range, sizeof(Range), _compareRanges);
	pthread_mutex_unlock(&span->lock);
	_checkSpanStatus(0, span);
}

void removeRangeFromSpan(Range* range, LTPSpan* span) {
	if ( range == NULL || span == NULL ) return;

	pthread_mutex_lock(&span->lock);
	if ( range->fromNode == span->nodeNumber )
		list_remove_data(&(span->rangesListOut), range, sizeof(Range), _compareRanges);
	if ( range->toNode == span->nodeNumber )
		list_remove_data(&(span->rangesListIn), range, sizeof(Range), _compareRanges);
	pthread_mutex_unlock(&span->lock);
	_checkSpanStatus(0, span);
}

void removeAllRangesFromSpan(LTPSpan* span) {
	if ( span == NULL ) return;

	pthread_mutex_lock(&span->lock);
	list_destroy(&(span->rangesListOut));
	list_destroy(&(span->rangesListIn));
	pthread_mutex_unlock(&span->lock);
	_checkSpanStatus(0, span);
}

void waitUntillSpanIsActive(LTPSpan* span) {
	pthread_mutex_lock(&(span->lock));
	while ( span->isStopped ) {
		pthread_cond_wait(&(span->stoppedCondition), &(span->lock));
	}
	pthread_cond_signal(&(span->stoppedCondition));
	pthread_mutex_unlock(&(span->lock));
}

void waitUntillCanSend(LTPSpan* span, unsigned int bytesToSend) {
	if ( span==NULL ) return;

	pthread_mutex_lock(&(span->lock));
	while ( bytesToSend > span->bytesToSend ) {
		pthread_cond_wait(&(span->waitingForCongestionControlCondition), &(span->lock));
	}
	span->bytesToSend -= bytesToSend;
	pthread_cond_signal(&(span->waitingForCongestionControlCondition));
	pthread_mutex_unlock(&(span->lock));
}

/***** PRIVATE FUNCTIONS *****/

static inline int _compareContacts(void* _newContact, size_t newContactSize, void* _contact, size_t contactSize) {
	return compareContacts((Contact*) _newContact, (Contact*)_contact);
}

static inline int _compareRanges(void* _newRange, size_t newRangeSize, void* _range, size_t rangeSize) {
	return compareRanges((Range*) _newRange, (Range*)_range);
}

static void _checkSpanStatus(TimerID timerID, void* _span) {
	LTPSpan* span = (LTPSpan*) _span;
	if ( span == NULL ) return;

	pthread_mutex_lock(&(span->lock));

	stopTimer(span->timerCheckChangesOnContactsAndRanges); // Stop the timer

	// Remove all old contacts
	list_remove_if(&(span->contactsList), _contactIsInThePast);

	// Remove all old ranges
	list_remove_if(&(span->rangesListOut), _rangeIsInThePast);
	list_remove_if(&(span->rangesListIn), _rangeIsInThePast);

	// No more contacts or ranges
	if ( span->contactsList == empty_list || span->rangesListOut == empty_list || span->rangesListIn == empty_list ) {
		span->isStopped = true;
		_freezeAllSessions(span);
		doLog("Span %d:\tInfinite pause", span->nodeNumber);
		pthread_cond_signal(&(span->stoppedCondition)); // Signal waiting threads
		pthread_mutex_unlock(&(span->lock));
		return;
	}

	// Looking for current contact & current range
	const time_t currentTime = time(NULL);

	// Now the first contact is the first to be scheduled (contacts are ordered)
	Contact* firstContact = (Contact*) span->contactsList->data;
	time_t nextEventContact;
	bool contactIsNow;
	if ( firstContact->fromTime <= currentTime && currentTime < firstContact->toTime ) { // The contact is now (currently active)
		nextEventContact = firstContact->toTime - currentTime;
		contactIsNow = true;
	} else { // The contact is in the future
		nextEventContact = firstContact->fromTime - currentTime;
		contactIsNow = false;
	}

	// Now the first range "out" is the first to be scheduled (ranges are ordered)
	Range* firstRangeOut = (Range*) span->rangesListOut->data;
	time_t nextEventRangeOut;
	bool rangeOutIsNow;
	if ( firstRangeOut->fromTime <= currentTime && currentTime < firstRangeOut->toTime ) { // The range "out" is now (currently valid)
		nextEventRangeOut = firstRangeOut->toTime - currentTime;
		rangeOutIsNow = true;
	} else { // The validity interval of the range "out" is in the future
		nextEventRangeOut = firstRangeOut->fromTime - currentTime;
		rangeOutIsNow = false;
	}

	// Now the first range "in" is the first to be scheduled (ranges are ordered)
	Range* firstRangeIn = (Range*) span->rangesListIn->data;
	time_t nextEventRangeIn;
	bool rangeInIsNow;
	if ( firstRangeIn->fromTime <= currentTime && currentTime < firstRangeIn->toTime ) { // The range "in" is now (currently valid)
		nextEventRangeIn = firstRangeIn->toTime - currentTime;
		rangeInIsNow = true;
	} else { // The validity interval of the range "in" is in the future
		nextEventRangeIn = firstRangeIn->fromTime - currentTime;
		rangeInIsNow = false;
	}

	const time_t nextEvent = min( min(nextEventRangeOut, nextEventRangeIn) , nextEventContact );

	if ( contactIsNow && rangeOutIsNow && rangeInIsNow ) { // Contact, range "in" and range "out" are all now
		span->rtoTime = ownqtime + firstRangeOut->seconds + firstRangeIn->seconds + span->remoteDelay;
		span->xmitRate = firstContact->xmitRate;
		span->isStopped = false;

		span->currentMaxBurstSize = max(span->maxBurstSize, span->xmitRate * ( TOKEN_UPDATE_TIME / 1000.0 ));

		if ( span->currentMaxBurstSize != span->maxBurstSize ) {
			doLog("Warning:\tchanging max burst size because of xmitRate too fast!");
		}

		if(span->Functions.handlerSpanResumed != NULL) { // It depends on the option chosen
			span->Functions.handlerSpanResumed(span);
		}
		pthread_cond_signal(&(span->stoppedCondition)); // Signal waiting threads

		doLog("Span %d:\tEnabled. Next event in %d seconds", span->nodeNumber, nextEvent);
	} else { // At least one of the 2 is in the future
		span->isStopped = true;
		span->Functions.handlerSpanPaused(span);

		doLog("Span %d:\tIn pause. Next event in %d seconds", span->nodeNumber, nextEvent);
	}

	if ( nextEvent > 0 ) // Restart the timer
		span->timerCheckChangesOnContactsAndRanges = startTimer(1000 * nextEvent, _checkSpanStatus, span);

	pthread_mutex_unlock(&(span->lock));
}

static void _freezeAllSessions(void* _span) {
	LTPSpan* span = (LTPSpan*) _span;
	// Freeze all RX sessions
	List current = span->activeRxSessions;
	while ( current != empty_list ) {
		freezeSession((LTPSession*)current->data);
		current = current->next;
	}

	// Freeze all TX sessions
	current = span->activeTxSessions;
	while ( current != empty_list ) {
		freezeSession((LTPSession*)current->data);
		current = current->next;
	}
}

static void _resumeAllSessions(void* _span) {
	LTPSpan* span = (LTPSpan*) _span;
	// Resume all RX sessions
	List current = span->activeRxSessions;
	while ( current != empty_list ) {
		resumeSession((LTPSession*)current->data);
		current = current->next;
	}

	// Resume all TX sessions
	current = span->activeTxSessions;
	while ( current != empty_list ) {
		resumeSession((LTPSession*)current->data);
		current = current->next;
	}
}

static void _destroyAllSessions(void* _span) {
	LTPSpan* span = (LTPSpan*) _span;
	// Destroy all RX sessions
	List current = span->activeRxSessions;
	List next = NULL;
	while ( current != empty_list ) {
		next = current->next;
		cleanAndCloseSession((LTPSession*)current->data);
		current = next;
	}

	// Destroy all TX sessions
	current = span->activeTxSessions;
	while ( current != empty_list ) {
		next = current->next;
		cleanAndCloseSession((LTPSession*)current->data);
		current = next;
	}
}

static bool _contactIsInThePast(void* data, size_t size) {
	Contact* contact = (Contact*) data;
	if ( contact->toTime <= time(NULL) ) {
		doLog("Info:\tRemoving contact from %d to %d", contact->fromNode, contact->toNode);
		return true;
	} else {
		return false;
	}
}

static bool _rangeIsInThePast(void* data, size_t size) {
	Range* range = (Range*) data;
	if ( range->toTime <= time(NULL) ) {
		doLog("Info:\tRemoving range from %d to %d", range->fromNode, range->toNode);
		return true;
	} else {
		return false;
	}
}

static void* _mainCongestionControl(void* _span) {
	LTPSpan* span = (LTPSpan*) _span;

	while ( span->isActive && !span->isRemoved ) {
			waitUntillSpanIsActive(span);

			pthread_mutex_lock(&(span->lock));
			unsigned int xmitRateToIncrease = span->xmitRate * ( TOKEN_UPDATE_TIME / 1000.0 );

			span->bytesToSend = min(span->bytesToSend + xmitRateToIncrease, span->currentMaxBurstSize);
			pthread_cond_signal(&(span->waitingForCongestionControlCondition));
			pthread_mutex_unlock(&(span->lock));

			usleep(1000 * TOKEN_UPDATE_TIME);
	}

	doLog("Span %d:\tmainCongestionControl ended", span->nodeNumber);
	return NULL;
}

static void* _mainGetterDataFromUpperProtocol(void* _span) {
	LTPSpan* span = (LTPSpan*) _span;

	while ( span->isActive && !span->isRemoved ) {
		waitUntillSpanIsActive(span);
		if ( !( span->isActive && !span->isRemoved ) ) break;

		if ( canReceiveDataFromUpperProtocol(span->nodeNumber, &span->dataPassedFromUpperProtocol) ) {
			if ( !( span->isActive && !span->isRemoved ) ) break;

			sem_wait(&(span->freeTxConcurrentSessionsSemaphore));
			// Signal new data is available
			uint64_t dummy = 1; // 1 because I have 1 new data
			write(span->dataFromUpperProtocolEventFD, &dummy, sizeof(dummy));
		}
	}

	doLog("Span %d:\tgetterDataFromUpperProtocol ended", span->nodeNumber);

	return NULL;
}

static void* _mainSpanThread(void* _span) {
	LTPSpan* span = (LTPSpan*) _span;

	// Prepare the select struct
	fd_set fdstruct; // cache the value to avoid to re-calculate every time
	FD_ZERO(&fdstruct);
	FD_SET(span->dataFromUpperProtocolEventFD, &fdstruct);
	FD_SET(span->signalSegmentsEventFD, &fdstruct);
	FD_SET(span->segmentsToSendEventFD, &fdstruct);


	int max = max(span->signalSegmentsEventFD, max(span->segmentsToSendEventFD,span->dataFromUpperProtocolEventFD));

	while ( span->isActive && !span->isRemoved ) {
		waitUntillSpanIsActive(span);
		if ( !( span->isActive && !span->isRemoved ) ) break;

		uint64_t numberOfRead;
		fd_set fdset = fdstruct;
		struct timeval timeout = {1,0}; // seconds of timeout
		select(max+1, &fdset, NULL, NULL, &timeout);

		if ( FD_ISSET(span->signalSegmentsEventFD, &fdset) ) { // signalSegment available
			read(span->signalSegmentsEventFD, &numberOfRead, sizeof(numberOfRead)); // read to remove the element from the queue. The number read is the number of queued elements
			for (int i = 1; i <= numberOfRead; i++) {
				_handleSignalSegment(span);
			}
		} else if ( FD_ISSET(span->segmentsToSendEventFD, &fdset) ) { // segmentsToSendEventFD available.NOTE: this will be executed only when no signal segments are available (they have priority on the other)!
			read(span->segmentsToSendEventFD, &numberOfRead, sizeof(numberOfRead)); // read to remove the element from the queue. The number read is the number of queued elements
			for (int i = 1; i <= numberOfRead; i++) {
				_handleSegmentToSend(span);
			}
		} else if ( FD_ISSET(span->dataFromUpperProtocolEventFD, &fdset) ) { // dataFromUpperProtocol available. NOTE: this will be executed only when no signal segments and resend signal are available!
			read(span->dataFromUpperProtocolEventFD, &numberOfRead, sizeof(numberOfRead)); // read to remove the element from the queue. The number read is the number of queued elements
			for (int i = 1; i <= numberOfRead; i++) {
				_handleSendNewBlock(span);
			}
		}
	}

	doLog("Span %d:\tspanThread ended", span->nodeNumber);
	return NULL;
}


static void _handleSendNewBlock(LTPSpan* span) {
	UpperProtocolDataToSend dataToSend;
	dataToSend.buffer = NULL;
	dataToSend.bufferLength = 0;
	dataToSend.dataNumber = 0;
	dataToSend.handlerSessionTerminated = NULL;
	dataToSend.isUnreliableBundle = false;

	passedDataFromUpperProtocol(span->nodeNumber, &dataToSend, &span->dataPassedFromUpperProtocol);

	if ( dataToSend.buffer != NULL ) {
		LTPSessionColor color;
		if(dataToSend.isUnreliableBundle && span->sessionColor != SessionColorGreen){
			doLog("Span %d: Unreliable bundle on non-green span",span->nodeNumber);
			color = SessionColorGreen;
		}
		else {
			color = span->sessionColor;
		}

		createNewSendSession(span, dataToSend.buffer, dataToSend.bufferLength, dataToSend.dataNumber, dataToSend.handlerSessionTerminated, color );
	}
}

static void _handleSignalSegment(LTPSpan* span) { // Handle received signal segments (not handled yet)
	pthread_mutex_lock(&span->lock);
	LTPSegment* segment = (LTPSegment*) list_pop_front(&(span->receivedSignalSegmentsForSpanThread), NULL); // Remove and get first segment
	pthread_mutex_unlock(&span->lock);
	if ( segment == NULL ) {
		return; // The segment can't be NULL, it should never happen
	}

	// Receiver signals (see RFC5326; CP + RA + CS + CAR)
	if ( isSegmentCheckpoint(*segment) ) {
		signalSessionCheckpointReceived(span, segment); 				// CP
	} else if ( isReportAckSegment(*segment) ) {
		signalSessionReportAckSegmentReceived(span, segment); 			// RA
	} else if ( isCancelSegment(*segment) ) {
		signalSessionCancelSegmentReceived(span, segment); 				// CS
	} else if ( isCancelReceiverAckSegment(*segment) ) {
		signalSessionCancelAckReceiverReceived(span, segment); 			// CAR
	} else if ( isGreenEOBDataSegment(*segment) ) {
		signalSessionGreenEOBDataSegmentReceived(span, segment);		// Green EOB
	} else if ( isOrangeEOBDataSegment(*segment) ) {
		signalSessionOrangeEOBDataSegmentReceived(span, segment);		// Orange EOB
	}

	// Sender signals (RS + CAS + CR)
	if ( isReportSegment(*segment) ) {
		signalSessionReportSegmentReceived(span, segment); 				// RS
	} else if ( isCancelAckSegment(*segment) ) {
		signalSessionCancelAckSegmentReceived(span, segment); 			// CAS
	} else if ( isCancelReceiverSegment(*segment) ) {
		signalSessionCancelReceiverSegmentReceived(span, segment); 		// CR
	}

	_free(segment);
}


static void _handleSegmentToSend(LTPSpan* span) { // Handle resend signal segments (not handled yet)

	size_t dim;

	pthread_mutex_lock(&span->lock);
	void* data =  list_pop_front(&(span->structsToSendBySpanThread), &dim); // Remove and get first send structure
	pthread_mutex_unlock(&span->lock);
	if(data == NULL){
		return; // The segment can't be NULL, it should never happen
	}

	if(dim == sizeof(CancelStruct)){ // Generate CX

		CancelStruct* cancelStruct = (CancelStruct*) data;
		LTPSession* session = cancelStruct->session;

		if(cancelStruct->typeFlag == CR)
			generateCR(session, cancelStruct->cancelCode, session->span->ipAddress,session->span->portNumber); // Generate CR segment and change state in CR_SENT
		else if (cancelStruct->typeFlag == CS)
			generateCS(session, cancelStruct->cancelCode, session->span->ipAddress,session->span->portNumber); // Generate CS segment and change state in CS_SENT

		_free(cancelStruct); // free memory

	}
	else{ //Retrasmission
		// Double cast because in the list there is the pointer of the pointer to SendStruct
		// this solution allows to memory leak problems
		SendStruct** temp = data;
		SendStruct* sendStruct = (SendStruct*) *temp;

		if ( isCancelSegment(*(sendStruct->segment)) ) { // CS
			_handlerResendCS(sendStruct->timerID,sendStruct);
		}
		else if ( isCancelReceiverSegment(*(sendStruct->segment) )){ // CR
			_handlerResendCR(sendStruct->timerID,sendStruct);
		}
		else if ( isSegmentCheckpoint(*(sendStruct->segment) )) { // CP
			_handlerResendCP(sendStruct->timerID,sendStruct);
		}
		else if ( isReportSegment(*(sendStruct->segment)) ) { // RS
			_handlerResendRS(sendStruct->timerID,sendStruct);
		}

		_free(temp);

	}



}


/***** HANDLER FUNCTIONS TO RESEND SEGMENT *****/


static void _handlerResendCS(TimerID timerID, void* _sentStruct){
	SendStruct* sentStruct = (SendStruct*) _sentStruct;
	int maxReTxNumber = MAX_RETX_NUMBER * (DISTRIBUTED_COPIES==1 ? SIGNAL_SEGMENTS_COPIES : 1);
		if ( sentStruct->numberOfSent >= maxReTxNumber ) {
			LTPSession* session = sentStruct->session;
			doLog("Session (E%d,#%d):\tCS resent too many times. Giving up", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, sentStruct->segment->dataSegment.checkpointSerialNumber);
			_free(session->TxSession.CSWaitingForCAS); // free memory
			closeSession(session); // Put session in Recently Closed
		} else {
			resendSegment(sentStruct, _handlerResentCS);
		}
}

static void _handlerResentCS(SendStruct* sentStruct) {
	sentStruct->timerID = startTimer(1000 * sentStruct->session->span->rtoTime / (DISTRIBUTED_COPIES ? SIGNAL_SEGMENTS_COPIES : 1), forwordingSpanNewSendStructToSend, sentStruct);
	doLog("Session (E%d,#%d):\tCS resent #%d", sentStruct->session->sessionID.sessionOriginator, sentStruct->session->sessionID.sessionNumber, sentStruct->numberOfSent);
}

static void _handlerResendCR(TimerID timerID, void* _sentStruct){
	SendStruct* sentStruct = (SendStruct*) _sentStruct;
	int maxReTxNumber = MAX_RETX_NUMBER * (DISTRIBUTED_COPIES==1 ? SIGNAL_SEGMENTS_COPIES : 1);
		if ( sentStruct->numberOfSent >= maxReTxNumber ) {
			LTPSession* session = sentStruct->session;
			doLog("Session (E%d,#%d):\tCR resent too many times. Giving up", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, sentStruct->segment->dataSegment.checkpointSerialNumber);
			_free(session->RxSession.CRWaitingForCAR);
			closeSession(session); // Put in recently closed
		} else {
			resendSegment(sentStruct, _handlerResentCR);
		}
}

static void _handlerResentCR(SendStruct* sentStruct) {
	sentStruct->timerID = startTimer(1000 * sentStruct->session->span->rtoTime / (DISTRIBUTED_COPIES ? SIGNAL_SEGMENTS_COPIES : 1), forwordingSpanNewSendStructToSend, sentStruct);
	doLog("Session (E%d,#%d):\tCR resent #%d", sentStruct->session->sessionID.sessionOriginator, sentStruct->session->sessionID.sessionNumber, sentStruct->numberOfSent);
}

static void _handlerResendCP(TimerID timerID, void* _sentStruct){
	SendStruct* sentStruct = (SendStruct*) _sentStruct;
	int maxReTxNumber = MAX_RETX_NUMBER * (DISTRIBUTED_COPIES==1 ? SIGNAL_SEGMENTS_COPIES : 1);
	pthread_mutex_lock(&(sentStruct->session->RTXTimersLock));
	list_remove_data(&(sentStruct->session->RTXTimers), &timerID, sizeof(timerID), NULL);
	pthread_mutex_unlock(&(sentStruct->session->RTXTimersLock));
	if ( sentStruct->numberOfSent >= maxReTxNumber ) {
		LTPSession* session = sentStruct->session;
		doLog("Session (E%d,#%d):\tCP %d resent too many times, signaling CANCEL to upper protocol", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, sentStruct->segment->dataSegment.checkpointSerialNumber);
		if ( session->TxSession.handlerSessionTerminated != NULL )
			session->TxSession.handlerSessionTerminated(session->TxSession.dataNumber, SessionResultCancel); // Signal the upper protocol this session is cancelled

		CancelStruct CSStruct = createCancelStruct(CS, RLEXC, session);
		forwordingSpanNewCancelStructToSend(&CSStruct);
	} else {
		resendSegment(sentStruct, _handlerResentCP);
	}
}

static void _handlerResentCP(SendStruct* sentStruct) {
	sentStruct->timerID = startTimer(1000 * sentStruct->session->span->rtoTime / (DISTRIBUTED_COPIES ? SIGNAL_SEGMENTS_COPIES : 1), forwordingSpanNewSendStructToSend, sentStruct);
	list_append(&(sentStruct->session->RTXTimers), &sentStruct->timerID, sizeof(sentStruct->timerID));
	doLog("Session (E%d,#%d):\tCP %d resent #%d", sentStruct->session->sessionID.sessionOriginator, sentStruct->session->sessionID.sessionNumber, sentStruct->segment->dataSegment.checkpointSerialNumber, sentStruct->numberOfSent);

}

static void _handlerResendRS(TimerID timerID, void* _sentStruct){
	SendStruct* sentStruct = (SendStruct*) _sentStruct;
	pthread_mutex_lock(&(sentStruct->session->RTXTimersLock));
	list_remove_data(&(sentStruct->session->RTXTimers), &timerID, sizeof(timerID), NULL);
	pthread_mutex_unlock(&(sentStruct->session->RTXTimersLock));
	int maxReTxNumber = MAX_RETX_NUMBER * (DISTRIBUTED_COPIES==1 ? SIGNAL_SEGMENTS_COPIES : 1);

		if ( sentStruct->numberOfSent >= maxReTxNumber ) {
			doLog("Session (E%d,#%d):\tRS %d resent too many times", sentStruct->session->sessionID.sessionOriginator, sentStruct->session->sessionID.sessionNumber, sentStruct->segment->reportSegment.reportSerialNumber);
			// Remove the sendStruct from the list
			list_remove_data(&(sentStruct->session->RxSession.RSWaitingForRA), &(sentStruct->segment->reportSegment.reportSerialNumber), sizeof(sentStruct->segment->reportSegment.reportSerialNumber), findReportSegmentFromReportSegmentSerialNumber);
			CancelStruct CRStruct = createCancelStruct(CR, RLEXC, sentStruct->session);
			forwordingSpanNewCancelStructToSend(&CRStruct);
		} else {
			resendSegment(sentStruct, _handlerResentRS);
		}
}

static void _handlerResentRS(SendStruct* sentStruct) {
	sentStruct->timerID = startTimer(1000 * sentStruct->session->span->rtoTime / (DISTRIBUTED_COPIES ? SIGNAL_SEGMENTS_COPIES : 1), forwordingSpanNewSendStructToSend,sentStruct);
	pthread_mutex_lock(&(sentStruct->session->RTXTimersLock));
	list_append(&(sentStruct->session->RTXTimers), &sentStruct->timerID, sizeof(sentStruct->timerID));
	pthread_mutex_unlock(&(sentStruct->session->RTXTimersLock));
	doLog("Session (E%d,#%d):\tRS %d resent #%d", sentStruct->session->sessionID.sessionOriginator, sentStruct->session->sessionID.sessionNumber, sentStruct->segment->reportSegment.reportSerialNumber, sentStruct->numberOfSent);
}


/***** HANDLER FUNCTIONS *****/

static void _handlerCheckForTerminatedSession(TimerID timerID, void* _span) {
	LTPSpan* span = (LTPSpan*) _span;

	if ( span == NULL || !span->isActive || span->isRemoved ) {
		return;
	}

	list_remove_if(&(span->recentlyClosedRxSessionIDs), _checkIsToRemoveTerminatedSession);
	list_remove_if(&(span->recentlyClosedTxSessionIDs), _checkIsToRemoveTerminatedSession);

	span->timerCleanerTerminatedSessions = startTimer(1000*RECENTLY_CLOSED_LIST_UPDATE_TIME, _handlerCheckForTerminatedSession, span);
}

static bool _checkIsToRemoveTerminatedSession(void* data, size_t size) {
	EndedSession* endedSession = (EndedSession*) data;
	struct timeval currentTime;
	gettimeofday(&currentTime, NULL);

	if ( timercmp(&currentTime, &(endedSession->endTime), >=) ) { // If time is over -> remove
		doLog("Session (E%d,#%d):\tRemoving the session from the recently closed session list", endedSession->sessionID.sessionOriginator, endedSession->sessionID.sessionNumber);
		return true;
	}

	return false;
}

static inline void _cleanSession(void* _session, size_t size) {
	cleanSession((LTPSession*)_session);
}

static inline void _closeSpan(void *span, size_t size) {
	closeSpan((LTPSpan*) span);
}

/***** COMPARE FUNCTIONS *****/

static int _findActiveSpanFromNodeNumber(void* _nodeNumber, size_t nodeNumberSize, void* _span, size_t spanSize) {
	unsigned long long* nodeNumber = (unsigned long long*) _nodeNumber;
	LTPSpan* span = (LTPSpan*) _span;
	if ( span->nodeNumber == *nodeNumber )
		return 0; // Equals
	else
		return -1; // Not equals
}

static int _findSpanFromTXSessionID(void* _sessionID, size_t sessionIDSize, void* _span, size_t spanSize) {
	LTPSessionID* sessionID = (LTPSessionID*) _sessionID;
	LTPSpan* span = (LTPSpan*) _span;
	if ( list_find(span->activeTxSessions, sessionID, sizeof(LTPSessionID), _findActiveTXSessionFromSessionID) > 0 )
		return 0; // Found
	else
		return -1; // Not found
}

static int _findActiveTXSessionFromSessionID(void* _sessionID, size_t sessionIDSize, void* _session, size_t sessionSize) {
	LTPSessionID* sessionID = (LTPSessionID*) _sessionID;
	LTPSession* session = (LTPSession*) _session;
	if ( session->sessionID.sessionNumber == sessionID->sessionNumber
		&& session->sessionID.sessionOriginator == sessionID->sessionOriginator)
		return 0; // Equals
	else
		return -1; // Not equals
}

static int _findSpanFromTerminatedTXSessionID(void* _sessionID, size_t sessionIDSize, void* _span, size_t spanSize) {
	LTPSessionID* sessionID = (LTPSessionID*) _sessionID;
	LTPSpan* span = (LTPSpan*) _span;
	if ( list_find(span->recentlyClosedTxSessionIDs, sessionID, sizeof(LTPSessionID), _findTerminatedTXSessionFromSessionID) > 0 )
		return 0; // Found
	else
		return -1; // Not found
}

static int _findTerminatedTXSessionFromSessionID(void* _sessionID1, size_t sessionID1Size, void* _sessionID2, size_t sessionID2Size) {
	LTPSessionID* sessionID1 = (LTPSessionID*) _sessionID1;
	LTPSessionID* sessionID2 = (LTPSessionID*) _sessionID2;
	if ( sessionID1->sessionNumber == sessionID2->sessionNumber
		&& sessionID1->sessionOriginator == sessionID2->sessionOriginator)
		return 0; // Equals
	else
		return -1; // Not equals
}
