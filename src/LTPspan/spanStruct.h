/** \file spanStruct.h
 *
 * \brief This file contains the defininition of the Span structure
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 * \authors Davide Filoni, davide.filoni2@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *
 *
 ***********************************************/

#ifndef SRC_LTPSPAN_SPANSTRUCT_H_
#define SRC_LTPSPAN_SPANSTRUCT_H_


#include <semaphore.h>
#include <pthread.h>
#include <stdbool.h>

#include "../list/list.h"
#include "../timer/timerStruct.h"
#include "../adapters/protocol/upperProtocolReceivedData.h"
#include "../LTPsession/sessionColor.h"

/*!
 * \brief LTP span
 */
typedef struct {
	/**
	 * \brief The node number toward this span is opened with
	 */
	unsigned long long 		nodeNumber;

	/**
	 * \brief The max number of concurrently opened TX sessions
	 */
	unsigned int 			maxTxConcurrentSessions;
	/**
	 * \brief The max number of concurrently opened RX sessions
	 */
	unsigned int 			maxRxConcurrentSessions;

	/**
	 * \brief The semaphore of available TX sessions
	 */
	sem_t 					freeTxConcurrentSessionsSemaphore;
	/**
	 * \brief The numer of currently RX buffer used
	 */
	unsigned int 			currentRxBufferUsed;

	/**
	 * \brief The payload segment size
	 */
	unsigned int 			payloadSegmentSize;

	/**
	 * \brief The IP ddress toward this span is opened with
	 */
	unsigned int 			ipAddress;
	/**
	 * \brief The port number toward this span is opened with
	 */
	unsigned short			portNumber;

	/**
	 * \brief The remote delay time (in seconds)
	 */
	unsigned int 			remoteDelay;
	/**
	 * \brief The RTO time (is updated when contact plan changes)
	 */
	unsigned int 			rtoTime;

	/**
	 * \brief The current xmit rate based on contacts
	 */
	long unsigned int 		xmitRate;
	/**
	 * \brief The bytes available to be sent. This is increased by congestion control based on xmit rate.
	 */
	long unsigned int 		bytesToSend;
	/**
	 * \brief The max burst size
	 */
	long unsigned int 		maxBurstSize;
	/**
	 * \brief Current max burst size (can be > maxBurstSize in case xmitRate * TOKEN_BUCKET_CONGESTION_CONTROL_UPDATE_MILLIS / 1000 is bigger than maxBustSize)
	 */
	long unsigned int 		currentMaxBurstSize;
	/**
	 * \brief The condition to wait for congestion control
	 */
	pthread_cond_t 			waitingForCongestionControlCondition;

	/**
	 * \brief The list of contacts
	 */
	List 					contactsList;
	/**
	 * \brief The list of out ranges
	 */
	List 					rangesListOut;
	/**
	 * \brief The list of in ranges
	 */
	List 					rangesListIn;

	/**
	 * \brief The timer ID of timer which checks for changes on contacts and ranges
	 */
	TimerID 				timerCheckChangesOnContactsAndRanges;

	/**
	 * \brief The list of currently active RX sessions
	 */
	List 					activeRxSessions;
	/**
	 * \brief The list of currently active TX sessions
	 */
	List 					activeTxSessions;

	/**
	 * \brief The list of recently closed RX sessions
	 */
	List 					recentlyClosedRxSessionIDs;
	/**
	 * \brief The list of recently closed TX sessions
	 */
	List 					recentlyClosedTxSessionIDs;

	/**
	 * \brief The list of signal segments queue to be handled by span thread
	 */
	List					receivedSignalSegmentsForSpanThread;
	/**
	* \brief The list of send struct, used to resend segment, and cancel struct used to generate CX
	*/
	List					structsToSendBySpanThread;


	/**
	 * \brief The file descriptor used to signal the span thread new signal segments are available
	 */
	int 					signalSegmentsEventFD;

	/**
	* \brief The file descriptor used to signal the span thread new segments to send are available
	*/
	int 					segmentsToSendEventFD;
	/**
	 * \brief The file descriptor used to signal the span thread new data is available to be sent
	 */
	int 					dataFromUpperProtocolEventFD;

	/**
	 * \brief The mutex to lock this span. This is used to apply changes on this
	 */
	pthread_mutex_t			lock;
	/**
	 * \brief The span thread
	 */
	pthread_t				mainThread;
	/**
	 * \brief The thread which receives data from the upper protocol
	 */
	pthread_t				upperProtocolThread;
	/**
	 * \brief The thread which will increase the BytesToSend
	 */
	pthread_t 				congestionControlThread;

	/**
	 * \brief The data passed from the upper protocol (LTP)
	 */
	UpperProtocolReceivedData dataPassedFromUpperProtocol;

	/**
	 * \brief This indicates if this span is active
	 */
	bool					isActive;
	/**
	 * \brief This indicates if this span is removed
	 */
	bool 					isRemoved;

	/**
	 * \brief This indicates if this span is stopped
	 */
	bool 					isStopped;
	/**
	 * \brief The condition when the span is stopped
	 */
	pthread_cond_t 			stoppedCondition;

	/**
	 * \brief The timer ID of the timer which clean the recently closed sessions
	 */
	TimerID					timerCleanerTerminatedSessions;

	/**
	 * \brief The session color
	 */
	LTPSessionColor 		sessionColor;

	/**
	 * \brief The protocol to be used in the lower protocol
	 */
	char*					lowerProtocol;

	/**
	* \brief The struct used to define how handle the session after and during the pause
	*/
	struct {
		void (*handlerSpanPaused)(void*);

		void (*handlerSpanResumed)(void*);
	} Functions;

} LTPSpan;


#endif /* SRC_LTPSPAN_SPANSTRUCT_H_ */
