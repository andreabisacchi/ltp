/** \file main.c
 *
 * \brief This file contains the main of the LTP engine
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 * \authors Davide Filoni, davide.filoni2@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <stdio.h>
#include <pthread.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>
#include <math.h>
#include <errno.h>
#include <limits.h>
#include <netdb.h>

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#ifndef __USE_GNU
#define __USE_GNU
#endif
#include <ucontext.h>
#include <execinfo.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "logger/logger.h"
#include "adapters/protocol/lowerProtocol.h"
#include "adapters/protocol/upperProtocol.h"
#include "receiver/receiver.h"
#include "sender/sender.h"
#include "sdnv/sdnv.h"
#include "timer/timer.h"

#include "generic/generic.h"
#include "config.h"

#include "list/list.h"
#include "spanFile/spanFile.h"

/**
 * \brief Handler for INT signal
 *
 * \param code 		Signal code number
 */
void intHandler(int code) {
	signal(SIGINT, SIG_DFL);

	printf("CTRL+C received\n");

	closeAllSpans();
	stopReceiver();

	for (int prints = 3; prints > 0; prints--) {
		printf("Closing... waiting %d seconds\n", prints);
		sleep(1);
	}
    printf("Closing!\n");
}

static void sigHandler(int sig, siginfo_t *si, void* unused)
{
	fprintf(stderr, "Segmentation fault!\n\nStack trace:\n");
    char **strings;
    size_t i, size;
    void *array[1024];
    size = backtrace(array, 1024);
    backtrace_symbols_fd(array, size, STDERR_FILENO);
    exit(EXIT_FAILURE);
}

static void printHelp(char* program) {
	fprintf(stderr, "Usage: %s <node number> [Options]\nOptions:\n\t--log\tEnable log prints\n\t--help\tPrints this help\n\nArguments can be inserted in any order\n", program);
}

/**
 * \brief Main function
 *
 * \param argc		Number of arguments
 * \param argv		Arguments
 */
int main(int argc, char **argv) {
 	srand(time(NULL));
	signal(SIGINT, intHandler);


	// Set the SEGFAULT handler
	{
		struct sigaction sa;
		sa.sa_flags = SA_SIGINFO;
		sigemptyset(&sa.sa_mask);
		sa.sa_sigaction = sigHandler;
		sigaction(SIGSEGV, &sa, NULL);
	}
	// start timer
	initTimer();

	unsigned long long myNodeNumber = 0;
	disableLogger();

	for (int i = 1; i < argc; i++) {
		if ( strcmp(argv[i], "--help") == 0 ) {
			printHelp(argv[0]);
			exit(EXIT_SUCCESS);
		} else if ( strcmp(argv[i], "--log") == 0 ) {
			enableLogger();
		} else if ( strncmp(argv[i], "--", 2) != 0 ) {
			// This is the nodeNumber
			if ( myNodeNumber != 0 ) {
				fprintf(stderr, "Error: node number inserted twice!\n\n");
				printHelp(argv[0]);
				exit(EXIT_FAILURE);
			}
			char* endptr;
			errno = 0;    /* To distinguish success/failure after call */
			myNodeNumber = strtoll(argv[i], &endptr, 10);

			/* Check for various possible errors */
			if ((errno == ERANGE && (myNodeNumber == LLONG_MAX || myNodeNumber == LLONG_MIN)) || (errno != 0 && myNodeNumber == 0) || endptr == argv[i]) {
				fprintf(stderr, "Error: node number is not a valid number!\n\n");
				printHelp(argv[0]);
				exit(EXIT_FAILURE);
			}
		} else {
			fprintf(stderr, "Error: %s not recognized!\n\n", argv[i]);
			printHelp(argv[0]);
			exit(EXIT_FAILURE);
		}
	}

	if ( myNodeNumber == 0 ) {
		fprintf(stderr, "Error: node number not inserted!\n\n");
		printHelp(argv[0]);
		exit(EXIT_FAILURE);
	}

	setMyNodeNumber(myNodeNumber);

	// Init components
	if ( !initUpperProtocol() ) {
		fprintf(stderr, "Error on initialize upperProtocol\n");
		exit(-1);
	}

	if ( !initLowerLevel() ) {
		fprintf(stderr, "Error on initialize lowerProtocol\n");
		exit(-2);
	}

	if ( !loadSpansFromConfigFile() ) {
		fprintf(stderr, "Error on parsing json file\n");
		exit(-1);
	}

	// Start threads
	pthread_t receiverThread;
	pthread_create(&receiverThread, NULL, mainReceiver, NULL);


	loadContactPlanUpperProtocol();

	// Wait untill everything is done
	pthread_join(receiverThread,  NULL);



	closeAllSpans();
	stopReceiver();

	//NEW
	destroyTimer();

	// Dispose everything
	disposeUpperProtocol();
	disposeLowerProtocol();

	sleep(1);

	printMallocFreeStat();
	doLog("End");
	return 0;
}
