/** \file session.c
 *
 * \brief This file contains the implementations of the LTPsession functions
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 * \authors Davide Filoni, davide.filoni2@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>

#include "session.h"

#include "../list/list.h"
#include "../config.h"

#include "../logger/logger.h"
#include "../LTPsegment/segment.h"
#include "../LTPspan/span.h"
#include "../sender/sender.h"
#include "../adapters/protocol/upperProtocol.h"
#include "../generic/generic.h"
#include "../timer/timer.h"

/***** PRIVATE FUNCTIONS *****/
static bool _existsSegmentInSession(LTPSegment* segment, LTPSession *session);
static bool _reportSegmentResendNotReceivedData(LTPSession* session, LTPSegment* reportSegment);
static void _sendSegmentsInRange(LTPSession* session, unsigned int from, unsigned int to, unsigned int reportSerialNumber, LTPSegmentTypeCode normalType, LTPSegmentTypeCode lastType);
static void _saveAckedRanges(LTPSession* session, LTPSegment* reportSegment);
static bool _haveGapInAckedRanges(LTPSession* session);
static void _generateSegmentToSend(LTPSession* session, unsigned int offset, unsigned int length, unsigned int reportSerialNumber, LTPSegmentTypeCode type);
static void _checkAllDataReceived(LTPSession* session);
static void _generateRS(LTPSession* session, LTPSpan* span, List claims, unsigned int checkpointSerialNumber, const unsigned int lowerByteToAck);
static void _generateRA(LTPSession* session, LTPSpan* span, LTPSegment* receivedSegment);
static void _generateCAR(LTPSession* session, LTPSpan* span, LTPSegment* cancelSegment);
static void _generateCAS(LTPSession* session, LTPSpan* span, LTPSegment* checkpoint);
static List _getClaimsFromSession(LTPSession* session, unsigned int lowerByteToAck, unsigned int upperByteToAck, bool* isLastRS);
static LTPBlock* _newBlock();
static void _destroyBlock(LTPSession* session);
static void _clearRXBuffer(LTPSession* session);
static bool	_addSegmentToSession(LTPSession *session, LTPSegment* segment);
/***** FOR EACH FUNCTION *****/
static void _forEachDestroySegment(void* data, size_t size);
/***** HANDLER FUNCTIONS *****/
static void _handlerSessionTimeoutTX(TimerID timerID, void* _session);
static void _handlerSessionTimeoutRX(TimerID timerID, void* _session);
static void _handlerCheckpointSent(SendStruct sentStruct);
static void _handlerReportSegmentSent(SendStruct sentStruct);
static void _handlerCancelSegmentSent(SendStruct sentStruct);
static void _handlerCancelReceiverSent(SendStruct sentStruct);
// the signature of these methods is required by the implementation of the Bisacchi's list library
static void _destroyRSWaitingForRetransmission(void* data, size_t size);
static void _destroyRSWaitingForCP(void* data, size_t size);
static void _destroyCPWaitingForRS(void* data, size_t size);
static void _freezeSessionTimer(void* data, size_t size);
static void _resumeSessionTimer(void* data, size_t size);
static void _stopSessionTimer(void* data, size_t size);
/***** COMPARE FUNCTIONS *****/
static int _insertOrderedSegment(void* newSegment, size_t newSegmentSize, void* segment, size_t segmentSize);
static int _insertOrderedAckedRange(void* _newAckedRange, size_t newAckedRangeSize, void* _currentAckedRange, size_t currentAckedSize);
static int _findCheckpointFromCheckpointNumber(void* _checkpointNumber, size_t checkpointNumberSize, void* _sendStruct, size_t sendStructSize);
static int _findReportSegmentFromCheckpoint(void* _reportSerialNumber, size_t _reportSerialNumberSize, void* _reportSegment, size_t _reportSegmentSize);
static int _findSessionFromSessionID(void* _sessionID, size_t _sessionID_size, void* _session, size_t _sessionSize);
static int _findAssociateCP (void* _RSSegment, size_t RSSize, void* _data, size_t dataSize);
static int _findAssociateRSforRA (void* _RASegment, size_t RASize, void* _data, size_t dataSize);
static int _findAssociateRSforCP (void* _CPSegment, size_t CPSize, void* _data, size_t dataSize);
static int _findAssociateCX (void* _CAXSegment, size_t CAXSize, void* _data, size_t dataSize);

/***** PRIVATE VARIABLES *****/

static unsigned long long myNodeNumber = 0;

/*!
 * \brief Acked Ranges
 */
typedef struct {
	/**
	 * \brief Byte from
	 */
	unsigned int from;
	/**
	 * \brief Byte to
	 */
	unsigned int to;
} AckedRange;

/***** PUBLIC FUNCTIONS *****/

void setMyNodeNumber(unsigned long long _myNodeNumber) {
	myNodeNumber = _myNodeNumber;
}

unsigned long long getMyNodeNumber() {
	return myNodeNumber;
}

void freezeSession(LTPSession* session) {
	pthread_mutex_lock(&(session->RTXTimersLock));
	list_for_each(session->RTXTimers, _freezeSessionTimer);
	pthread_mutex_unlock(&(session->RTXTimersLock));
	doLog("Session (E%d,#%d):\tFreezed", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
}

void resumeSession(LTPSession* session) {
	pthread_mutex_lock(&(session->RTXTimersLock));
	list_for_each(session->RTXTimers, _resumeSessionTimer);
	pthread_mutex_unlock(&(session->RTXTimersLock));
	doLog("Session (E%d,#%d)\tResumed", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
}

void cleanAndCloseSession(LTPSession* session) {
	cleanSession(session);
	closeSession(session);
}

void createSession(LTPSession* session, LTPSpan* span, unsigned long long sessionOriginator, unsigned int sessionNumber, LTPSessionColor sessionColor, SessionType sessionType) {
	session->sessionID.sessionOriginator = sessionOriginator;
	session->sessionID.sessionNumber 	 = sessionNumber;

	session->sessionColor = sessionColor;

	session->RTXTimers = empty_list;

	session->block = _newBlock();
	session->span = span;

	session->sessionType = sessionType;

	pthread_mutexattr_t ma;
	pthread_mutexattr_init(&ma);
	pthread_mutexattr_settype(&ma, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&(session->RTXTimersLock), &ma);

	if ( sessionType == RX_SESSION ) {
		session->RxSession.segmentList = empty_list;
		session->RxSession.RSWaitingForRA = empty_list;
		session->RxSession.RSWaitingForCP = empty_list;
		session->RxSession.isEOBarrived = false;
		session->RxSession.CPReceived = empty_list;
		session->RxSession.CRWaitingForCAR = NULL;
		session->RxSession.state = RX_NORMAL;
		session->RxSession.lastRSSentNumber = 0;
		session->RxSession.timerCancelRXSession = 0;
		session->RxSession.timerMaxIntersegment = 0;

		pthread_mutexattr_t ma;
		pthread_mutexattr_init(&ma);
		pthread_mutexattr_settype(&ma, PTHREAD_MUTEX_RECURSIVE);
		pthread_mutex_init(&(session->RxSession.segmentListLock), &ma);

	} else if ( sessionType == TX_SESSION ) {
		session->TxSession.isFullySent = false;
		session->TxSession.destination = 0;
		session->TxSession.CPWaitingForRS = empty_list;
		session->TxSession.CSWaitingForCAS = NULL;
		session->TxSession.listRangeAcked = empty_list;
		session->TxSession.RSReceived = empty_list;
		session->TxSession.dataNumber = 0;
		session->TxSession.handlerSessionTerminated = NULL;
		session->TxSession.state = TX_NORMAL;
		session->TxSession.timerCancelTXSession = 0;
	}
}

void cleanSession(LTPSession* session) {
	_destroyBlock(session); // if not already destroyed

	pthread_mutex_lock(&(session->RTXTimersLock));
	list_for_each(session->RTXTimers, _stopSessionTimer);
	list_destroy(&(session->RTXTimers));
	pthread_mutex_unlock(&(session->RTXTimersLock));

	pthread_mutex_destroy(&(session->RTXTimersLock));

	if (session->sessionType == RX_SESSION) {
		// segmentList
		_clearRXBuffer(session);
		// RSWaitingForRA
		list_for_each(session->RxSession.RSWaitingForRA, _destroyRSWaitingForRetransmission);
		list_destroy(&(session->RxSession.RSWaitingForRA));
		// RSWaitingForCP
		list_for_each(session->RxSession.RSWaitingForCP, _destroyRSWaitingForCP);
		list_destroy(&(session->RxSession.RSWaitingForCP));
		// CPReceived
		list_destroy(&(session->RxSession.CPReceived));
		// stop intersegment timer and cancel timer
		stopTimer(session->RxSession.timerMaxIntersegment);
		stopTimer(session->RxSession.timerCancelRXSession);

		pthread_mutex_destroy(&(session->RxSession.segmentListLock));

	} else if (session->sessionType == TX_SESSION) {
		// CPWaitingForRS
		list_for_each(session->TxSession.CPWaitingForRS, _destroyCPWaitingForRS);
		list_destroy(&(session->TxSession.CPWaitingForRS));
		// listRangeAcked
		list_destroy(&(session->TxSession.listRangeAcked));
		// RSReceived
		list_destroy(&(session->TxSession.RSReceived));
		// stop cancel timer
		stopTimer(session->TxSession.timerCancelTXSession);
	}
}

void createNewSendSession(LTPSpan* span, void* data, int dataLength, unsigned int dataNumber, void (*handlerSessionTerminated)(unsigned int, LTPSessionResultCode), LTPSessionColor sessionColor) {
	static unsigned int currentSessionNumber = 1;
	LTPSession newSession;
	createSession(&newSession, span, myNodeNumber, currentSessionNumber, sessionColor, TX_SESSION);
	currentSessionNumber++; if (currentSessionNumber == 0) currentSessionNumber++; // To avoid using 0

	// Set block data
	newSession.block->data = data;
	newSession.block->blockLength = dataLength;

	newSession.TxSession.dataNumber = dataNumber;
	newSession.TxSession.handlerSessionTerminated = handlerSessionTerminated;

	// Set destination
	newSession.TxSession.destination = span->nodeNumber;

	LTPSegmentTypeCode firstSegments;
	LTPSegmentTypeCode lastSegment;
	char* colorString;

	switch( sessionColor ) {
	case SessionColorRed:
		firstSegments = LTPDSRed;
		lastSegment = LTPDSRedEOB;
		colorString = "red";
		break;

	case SessionColorGreen:
		firstSegments = LTPDSGreen;
		lastSegment = LTPDSGreenEOB;
		colorString = "green";
		break;

	case SessionColorOrange:
		firstSegments = LTPDSOrange;
		lastSegment = LTPDSOrangeEOB;
		colorString = "orange";
		break;

	default:
		fprintf(stderr, "Color not found in creating new send session\n");
		return;
	}

	LTPSession* session = (LTPSession*) list_append(&(span->activeTxSessions), &newSession, sizeof(LTPSession));

	doLog("Session (E%d,#%d):\tCreated (%s TX)", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, colorString);

	_sendSegmentsInRange(session, 0, session->block->blockLength, 0, firstSegments, lastSegment);

	if ( sessionColor == SessionColorOrange ) {
		session->TxSession.timerCancelTXSession = startTimer(1000 * span->rtoTime * MAX_RETX_NUMBER, _handlerSessionTimeoutTX, session);
		// To do for other BP implementation: Initial Trasmission Completion (ORANGE) (RFC 5326 7.7)
	}
	else if ( sessionColor == SessionColorGreen ) {
		doLog("Session (E%d,#%d):\tSignaling SUCCESS to upper protocol (green session)", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		if ( session->TxSession.handlerSessionTerminated != NULL )
			session->TxSession.handlerSessionTerminated(session->TxSession.dataNumber, SessionResultSuccess); // Signal the upper protocol this session is completed correctly (RFC 5326 7.4)
		cleanAndCloseSession(session);
	}
	else { // red session
		session->TxSession.timerCancelTXSession = startTimer(CANCEL_TX_RED_SESSION_TIME, _handlerSessionTimeoutTX, session); // 24 hours
	}
}

bool signalSessionNewDataSegmentReceived(LTPSpan* span, LTPSegment* receivedSegment, LTPSessionColor sessionColor) {
	LTPSession* session;
	session = list_get_pointer_data(span->activeRxSessions, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), _findSessionFromSessionID); // Get the session from the active sessions

	// Check that the segment session is not among the terminated sessions; otherwise -> skip the received segment
	if ( (session == NULL) && (list_find(span->recentlyClosedRxSessionIDs, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), NULL /* use default compare*/) > 0) ) {/*if session is null -> check not to be in terminatedSessionList*/
		doLog("Session (E%d,#%d):\tIgnoring segment data of a recently-closed session", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
		return false;
	}

	// Green segments will never satisfy this condition
	if((session != NULL) && (session->RxSession.state == CR_SENT || session->RxSession.state == ALL_DATA_RECEIVED)){ // if session state is CR_SENT or ALL_DATA_RECEIVED -> skip the received segment
		doLog("Session (E%d,#%d):\tIgnoring segment data of a CR_SENT or ALL_DATA_RECEIVED session", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
		return false;
	}

	if ( session == NULL ) { // If the segment session does not exist -> try to create it
		pthread_mutex_lock(&(span->lock)); // Lock the span
		if ( span->currentRxBufferUsed >= span->maxRxConcurrentSessions ) { // Too many Rx session -> discard segment
			fprintf(stderr, "Too many Rx sessions for span %lld\n", span->nodeNumber);
			pthread_mutex_unlock(&(span->lock)); // Unlock the span
			return false;
		}
		span->currentRxBufferUsed++; // Increase the number of cuncurrent Rx sessions
		LTPSession newSession;
		createSession(&newSession, span, receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber, sessionColor, RX_SESSION);
		session = list_append(&(span->activeRxSessions), &newSession, sizeof(LTPSession)); // Add the new session to the list of active sessions

		// Set cancel timer valids for all type of session color (24 hours)
		session->RxSession.timerCancelRXSession = startTimer(CANCEL_RX_SESSION_TIME, _handlerSessionTimeoutRX, session);

		// According to the session color set the intersegment timer
		if(sessionColor == SessionColorRed){
			 // red haven't got intersegment timer
			doLog("Session (E%d,#%d):\tCreated (red RX)", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		}
		else if (sessionColor == SessionColorOrange){
			session->RxSession.timerMaxIntersegment = startTimer(1000 * SESSION_INTERSEGMENT_MAX_TIME, _handlerSessionTimeoutRX, session);
			doLog("Session (E%d,#%d):\tCreated (orange RX)", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		}
		else if (sessionColor == SessionColorGreen ){
			session->RxSession.timerMaxIntersegment = startTimer(1000 * SESSION_INTERSEGMENT_MAX_TIME, _handlerSessionTimeoutRX, session);
			doLog("Session (E%d,#%d):\tCreated (green RX)", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		}

		pthread_mutex_unlock(&(span->lock)); // Unlock the span

	} else {
		if(session->sessionColor != sessionColor){
			if(sessionColor == SessionColorOrange || sessionColor == SessionColorRed){ // orange and red session -> send CR
				doLog("Session (E%d,#%d):\tReceived segment color different from session color... Send CR (miscolored)", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
				_clearRXBuffer(session);
				CancelStruct CRStruct = createCancelStruct(CR, MISCOLORED, session);
				forwordingSpanNewCancelStructToSend(&CRStruct);
				return false;
			}
			else { // green session -> clean and close Session
				doLog("Session (E%d,#%d):\tReceived segment color different from session color... close session", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
				cleanAndCloseSession(session);
				return false;
			}
		}
		else
			if(sessionColor == SessionColorGreen || sessionColor == SessionColorOrange){
				restartTimer(session->RxSession.timerMaxIntersegment);
			}
		}

	_addSegmentToSession(session, receivedSegment); // Try to add the segment to the session list (i.e. the Rx buffer for the session)

	return true;
}

void signalSessionGreenEOBDataSegmentReceived(LTPSpan* span, LTPSegment* receivedSegment) {
	LTPSession* session = list_get_pointer_data(span->activeRxSessions, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), _findSessionFromSessionID); // Get the session from the active sessions
	if ( session == NULL ) {	// If the EOB session does not exist -> ignore the segment
		destroySegment(receivedSegment);
		return;
	}


	session->block->blockLength = receivedSegment->dataSegment.offset + receivedSegment->dataSegment.length;
	session->RxSession.isEOBarrived = true;

	doLog("Session (E%d,#%d):\tGreen EOB received", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
//CCaini added stop inter-segment timer
        stopTimer(session->RxSession.timerMaxIntersegment);

	_checkAllDataReceived(session); // Check block integrity & try to deliver the block to the upper protcol (e.g. BP)

	if (session->RxSession.state != ALL_DATA_RECEIVED ) { // If not all data received
		doLog("Session (E%d,#%d):\tGreen block uncompleted; block discarded", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
	}

	cleanAndCloseSession(session);
}


void signalSessionOrangeEOBDataSegmentReceived(LTPSpan* span, LTPSegment* receivedSegment) {
	LTPSession* session = list_get_pointer_data(span->activeRxSessions, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), _findSessionFromSessionID); // Get the session from the active sessions
	if ( session == NULL ) {	// If the EOB session does not exist -> ignore segment
		destroySegment(receivedSegment);
		return;
	}

	if(session->RxSession.state == CR_SENT || session->RxSession.state == ALL_DATA_RECEIVED){ // if session state is not NORMAL -> do nothing
		destroySegment(receivedSegment);
		return;
	}


	session->block->blockLength = receivedSegment->dataSegment.offset + receivedSegment->dataSegment.length;
	session->RxSession.isEOBarrived = true;


	if ( list_find(session->RxSession.CPReceived, &(receivedSegment->dataSegment.checkpointSerialNumber), sizeof(receivedSegment->dataSegment.checkpointSerialNumber), NULL /*default compare*/) > 0 ) { // Checkpoint is already received
		doLog("Session (E%d,#%d):\tOrange EOB %u duplicated, ignoring it", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->dataSegment.checkpointSerialNumber);
		destroySegment(receivedSegment);
		return; // Already received -> skip it
	} else { // This Orange EOB was never received;
		// Treat it as it were a CP and add it to the list of CP received
		list_push_front(&(session->RxSession.CPReceived), &(receivedSegment->dataSegment.checkpointSerialNumber), sizeof(receivedSegment->dataSegment.checkpointSerialNumber));
	}

	doLog("Sesison %d-%d:\tOrange EOB received", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
//CCaini added stop inter-segment timer
        stopTimer(session->RxSession.timerMaxIntersegment);

	_checkAllDataReceived(session); // Check the integrity of the blcok & try to deliver it to the upper protcol (e.g. BP)

	if ( session->RxSession.state == ALL_DATA_RECEIVED ) { // If all data received -> generate an RS, then destroy the claim list
		// Generate an "all received" RS
		List claims = _getClaimsFromSession(session, 0, session->block->blockLength, NULL);
		_generateRS(session, span, claims, receivedSegment->dataSegment.checkpointSerialNumber, 0);
		list_destroy(&claims);

	} else {
		doLog("Session (E%d,#%d):\tOrange block uncompleted; block discarded; sending CR", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		_clearRXBuffer(session); // XXX
		CancelStruct CRStruct = createCancelStruct(CR, INCOMPLETE_ORANGE_BLOCK, session);
		forwordingSpanNewCancelStructToSend(&CRStruct); // It sends the CR struct to the span thread that will send it and clean the session
	}

}

void signalSessionCheckpointReceived(LTPSpan* span, LTPSegment* receivedSegment) {
	LTPSession* session = list_get_pointer_data(span->activeRxSessions, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), _findSessionFromSessionID); // Get the session from the active sessions
	if ( session == NULL ) {	// If the segment session does not exist or in recently closed-> skip it
		destroySegment(receivedSegment);
		return;
	}

	if ( session->RxSession.state == ALL_DATA_RECEIVED ||  session->RxSession.state == CR_SENT) {// If the session state is CR_SENT or ALL_DATA_RECEIVED -> skip it
		 doLog("INFO:\tReceived CP of a CR_SENT or ALL DATA RECETVED session.");
		 destroySegment(receivedSegment);
		return;
		}

	if ( isSegmentEOB(*receivedSegment) ) { // End of block -> save lastByteOfBlock & set EOB arrived
		session->block->blockLength = receivedSegment->dataSegment.offset + receivedSegment->dataSegment.length;
		session->RxSession.isEOBarrived = true;
	}

	if ( list_find(session->RxSession.CPReceived, &(receivedSegment->dataSegment.checkpointSerialNumber), sizeof(receivedSegment->dataSegment.checkpointSerialNumber), NULL /*default compare*/) > 0 ) { // Checkpoint is already received
		doLog("Session (E%d,#%d):\tCP %u duplicated, ignoring it", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->dataSegment.checkpointSerialNumber);
		destroySegment(receivedSegment);
		return; // Already received -> skip it
	} else { // otherwise
		// Add this checkpoint to the list of CP received
		list_push_front(&(session->RxSession.CPReceived), &(receivedSegment->dataSegment.checkpointSerialNumber), sizeof(receivedSegment->dataSegment.checkpointSerialNumber));
	}

	if ( receivedSegment->dataSegment.reportSerialNumber > 0 ) {
		doLog("Session (E%d,#%d):\tCP %d received in response to RS %d", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->dataSegment.checkpointSerialNumber, receivedSegment->dataSegment.reportSerialNumber);
	} else {
		doLog("Session (E%d,#%d):\tCP %d received", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->dataSegment.checkpointSerialNumber);
	}

	// If the CP is in response to a previous RS (i.e. CP following reTx segments), destroy the RS
	if ( receivedSegment->dataSegment.reportSerialNumber > 0 ) { // If the CP is in response to an RS
		// Get the index -> remove the RS from the list -> destroy segments (dealloc memory) -> free the segment (list_remove_index_get_pointer requires to free)
		int index = list_find(session->RxSession.RSWaitingForRA, &(receivedSegment->dataSegment.reportSerialNumber), sizeof(receivedSegment->dataSegment.reportSerialNumber), findReportSegmentFromReportSegmentSerialNumber);
		if ( index > 0 ) { // Not found -> Skip
			SendStruct* sendStructReportSegmentToBeRemoved = (SendStruct*) list_remove_index_get_pointer(&(session->RxSession.RSWaitingForRA), index, NULL);
			while (sendStructReportSegmentToBeRemoved->timerID==0); // wait for timerID is valid
			stopTimer(sendStructReportSegmentToBeRemoved->timerID);
			pthread_mutex_lock(&(session->RTXTimersLock));
			list_remove_data(&(session->RTXTimers), &sendStructReportSegmentToBeRemoved->timerID, sizeof(sendStructReportSegmentToBeRemoved->timerID), NULL);
			pthread_mutex_unlock(&(session->RTXTimersLock));
			int indexRS = list_find(span->structsToSendBySpanThread, &(receivedSegment), sizeof(receivedSegment), _findAssociateRSforCP);
			if(indexRS > 0){
				// remove the pointer of RS from retrasmission list and free the memory
				void* pointer = list_remove_index_get_pointer(&(span->structsToSendBySpanThread), indexRS, NULL);
				_free(pointer);
				uint64_t dummy = -1; // -1 because I have remove an element
				write(span->segmentsToSendEventFD, &dummy, sizeof(dummy));
			}
			_destroyRSWaitingForRetransmission(sendStructReportSegmentToBeRemoved, 0); // Destroy the RS Send Data
			_free(sendStructReportSegmentToBeRemoved); // Free the sendStruct
		}
	}

	// Calculate the upper & lower bytes of the new RS
	unsigned int upperByteToAck = receivedSegment->dataSegment.offset + receivedSegment->dataSegment.length;
	unsigned int lowerByteToAck = 0;

	if ( receivedSegment->dataSegment.reportSerialNumber > 0 ) { // Check if is checkpoint in response of a RS
		// Look for RS to get upper & lower byte to ack
		int index = list_find(session->RxSession.RSWaitingForCP, &(receivedSegment->dataSegment.reportSerialNumber), sizeof(receivedSegment->dataSegment.reportSerialNumber), _findReportSegmentFromCheckpoint);
		if ( index > 0 ) { // If found remove & destroy
			LTPSegment* reportSegment = (LTPSegment*) list_remove_index_get_pointer(&(session->RxSession.RSWaitingForCP), index, NULL);
			lowerByteToAck = reportSegment->reportSegment.lowerBound;
			upperByteToAck = reportSegment->reportSegment.upperBound;
			destroySegment(reportSegment);
			_free(reportSegment);
		}
	}

	_checkAllDataReceived(session); // Check block integrity & try to deliver to upper protcol

	// Calculate the claims & generate the RS
	bool isLastRS;
	List claims = _getClaimsFromSession(session, lowerByteToAck, upperByteToAck, &isLastRS);
	_generateRS(session, span, claims, receivedSegment->dataSegment.checkpointSerialNumber, (isLastRS ? 0 : lowerByteToAck));
	list_destroy(&claims);
}

void signalSessionReportAckSegmentReceived(LTPSpan* span, LTPSegment* receivedSegment) {
	LTPSession* session = list_get_pointer_data(span->activeRxSessions, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), _findSessionFromSessionID); // Get the session from the active sessions
	if ( session == NULL ) {	// If the segment session does not exist -> skip the segment
		doLog("Session (E%d,#%d):\tRA %d received of session which doesn't exist", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber);
		destroySegment(receivedSegment);
		return;
	}

	// TODO RA --> RAS (seguendo l'RFC)
	doLog("Session (E%d,#%d):\tRA received confirming RS %d", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber, receivedSegment->reportAckSegment.reportSerialNumber);

	bool lastRSConfirmed = false;

	// Get the index -> remove the acknowleged RS from the list -> destroy segments (dealloc memory) -> free the segment (list_remove_index_get_pointer requires to free)
	int index = list_find(session->RxSession.RSWaitingForRA, &(receivedSegment->reportAckSegment.reportSerialNumber), sizeof(receivedSegment->reportAckSegment.reportSerialNumber), findReportSegmentFromReportSegmentSerialNumber);
	if ( index > 0 ) { // Not found -> Skip
		SendStruct* sendStruct = (SendStruct*) list_remove_index_get_pointer(&(session->RxSession.RSWaitingForRA), index, NULL);
		while (sendStruct->timerID==0); // wait for timerID is valid     XXX serve ancora coi nuovi timer?
		stopTimer(sendStruct->timerID);
		pthread_mutex_lock(&(session->RTXTimersLock));
		list_remove_data(&(session->RTXTimers), &sendStruct->timerID, sizeof(sendStruct->timerID), NULL);
		pthread_mutex_unlock(&(session->RTXTimersLock));
		int indexRS = list_find(span->structsToSendBySpanThread, &(receivedSegment), sizeof(receivedSegment), _findAssociateRSforRA);
		if(indexRS > 0){
			// remove the pointer of RS from retrasmission list and free the memory
			void* pointer = list_remove_index_get_pointer(&(span->structsToSendBySpanThread), indexRS, NULL);
			_free(pointer);
			uint64_t dummy = -1; // -1 because I have remove an element
			write(span->segmentsToSendEventFD, &dummy, sizeof(dummy));
		}

		lastRSConfirmed = (session->RxSession.lastRSSentNumber == sendStruct->segment->reportSegment.reportSerialNumber);

		destroySegment(sendStruct->segment);
		_free(sendStruct->segment);
		_free(sendStruct); // Free the sendStruct
	}

	if ( session->RxSession.state == ALL_DATA_RECEIVED && lastRSConfirmed ) { // Block delivered && no more RS waiting for ack -> session is closed
		doLog("Session (E%d,#%d):\tCompleted", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		cleanAndCloseSession(session);
	}

	destroySegment(receivedSegment);
}

void signalSessionCancelSegmentReceived(LTPSpan* span, LTPSegment* receivedSegment) {
	LTPSession* session = list_get_pointer_data(span->activeRxSessions, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), _findSessionFromSessionID); // Get the session from the active sessions

	if ((session == NULL) && (list_find(span->recentlyClosedRxSessionIDs, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), _findSessionFromSessionID) > 0)){
			session = list_get_pointer_data(span->recentlyClosedRxSessionIDs, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), _findSessionFromSessionID); // Get the session from the recently closed sessions
			doLog("INFO:\tReceived CS of a terminated session. Sending CAS...");
			_generateCAS(session, span, receivedSegment); // Send CAS
			destroySegment(receivedSegment);
			return;
		}

	if(session == NULL){ // This is not RFC 5326 compliant because we do not send a CAS
			doLog("INFO:\tReceived CS of a unknown session.");
			destroySegment(receivedSegment);
			return;
		}

	if ( session != NULL ) { // If session exists -> destroy it
		_generateCAS(session, span, receivedSegment); // Send CAS
		doLog("Session (E%d,#%d):\tCS received for session with code %d", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber, receivedSegment->cancelSegment.cancelCode);
		cleanAndCloseSession(session);
	}
	destroySegment(receivedSegment);
}

void signalSessionCancelAckReceiverReceived(LTPSpan* span, LTPSegment* receivedSegment) {
	LTPSession* session = list_get_pointer_data(span->activeRxSessions, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), _findSessionFromSessionID); // Get the session from the active sessions

	if ( session == NULL ) {  // If the segment session does not exist -> skip the segment
		doLog("CAR received of an unknown Session (E%d,#%d)", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
		destroySegment(receivedSegment);
		return;
	}

	doLog("Session (E%d,#%d):\tCAR received", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);

	if ( session->RxSession.state == CR_SENT) {
		// stop retrasmission timer
		stopTimer(((SendStruct*)session->RxSession.CRWaitingForCAR)->timerID);
		pthread_mutex_lock(&(session->RTXTimersLock));
		list_remove_data(&(session->RTXTimers), &(((SendStruct*)session->RxSession.CRWaitingForCAR)->timerID), sizeof(((SendStruct*)session->RxSession.CRWaitingForCAR)->timerID), NULL);
		pthread_mutex_unlock(&(session->RTXTimersLock));
		int indexCR = list_find(span->structsToSendBySpanThread, &(receivedSegment), sizeof(receivedSegment), _findAssociateCX);
		if(indexCR > 0){
			// remove the pointer of CS from retrasmission list and free the memory
			void* pointer = list_remove_index_get_pointer(&(span->structsToSendBySpanThread), indexCR, NULL);
			_free(pointer);
			uint64_t dummy = -1; // -1 because I have remove an element
			write(span->segmentsToSendEventFD, &dummy, sizeof(dummy));
		}
		// free the memory
		_free(((SendStruct*)session->RxSession.CRWaitingForCAR)->segment);
		_free(session->RxSession.CRWaitingForCAR);
		closeSession(session);  // Put the session in recently close
	}

	destroySegment(receivedSegment);
}

void signalSessionReportSegmentReceived(LTPSpan* span, LTPSegment* receivedSegment) {
	LTPSession* session = list_get_pointer_data(span->activeTxSessions, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), _findSessionFromSessionID); // Get the session from the active sessions


	if ((session == NULL) && (list_find(span->recentlyClosedTxSessionIDs, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID),_findSessionFromSessionID) > 0)){
		session = list_get_pointer_data(span->recentlyClosedTxSessionIDs, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), _findSessionFromSessionID); // Get the session from the recently closed sessions
		doLog("INFO:\tReceived RS of a terminated session. Sending RA...");
		_generateRA(session, span, receivedSegment); // Send RA
		destroySegment(receivedSegment);
		return;
	}

	if ( session == NULL ) {	// This is not RFC 5326 compliant because we do not send a RA
		doLog("INFO:\tReceived RS of a unknown session.");
		destroySegment(receivedSegment);
		return;
	}

	if ( session->TxSession.state == CS_SENT) {	// If the session state is CS_SENT -> send RA
		doLog("INFO:\tReceived RS of a CS_SENT session. Sending RA...");
		_generateRA(session, span, receivedSegment); // Send RA
		destroySegment(receivedSegment);
		return;
	}


	doLog("Session (E%d,#%d):\tRS received %d in response to CP %d\tclaimCount = %d\t\tbounds %d-%d", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber, receivedSegment->reportSegment.checkpointSerialNumber, receivedSegment->reportSegment.receptionClaimCount, receivedSegment->reportSegment.lowerBound, receivedSegment->reportSegment.upperBound);

	_generateRA(session, span, receivedSegment); // Send RA

	if ( list_find(session->TxSession.RSReceived, &(receivedSegment->reportSegment.reportSerialNumber), sizeof(receivedSegment->reportSegment.reportSerialNumber), NULL /*default compare*/) > 0 ) { // RS is already received
		doLog("Session (E%d,#%d):\tRS %u duplicated, RA sent then ignored", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->reportSegment.reportSerialNumber);
		destroySegment(receivedSegment);
		return; // Already received -> skip it
	} else { // Otherwise
		// add it to the list of RS received
		list_push_front(&(session->TxSession.RSReceived), &(receivedSegment->reportSegment.reportSerialNumber), sizeof(receivedSegment->reportSegment.reportSerialNumber));
	}

	if ( receivedSegment->reportSegment.checkpointSerialNumber > 0 ) { // If checkpoint > 0 -> stop the CP Retransmission timer
		int index = list_find(session->TxSession.CPWaitingForRS, &(receivedSegment->reportSegment.checkpointSerialNumber), sizeof(receivedSegment->reportSegment.checkpointSerialNumber), _findCheckpointFromCheckpointNumber);
		if (index > 0) {
			SendStruct* sendStruct = (SendStruct*) list_remove_index_get_pointer(&(session->TxSession.CPWaitingForRS), index, NULL);
			while (sendStruct->timerID==0); // wait for timerID is valid
			stopTimer(sendStruct->timerID);
			pthread_mutex_lock(&(session->RTXTimersLock));
			list_remove_data(&(sendStruct->session->RTXTimers), &sendStruct->timerID, sizeof(sendStruct->timerID), NULL);
			pthread_mutex_unlock(&(session->RTXTimersLock));
			int indexCP = list_find(span->structsToSendBySpanThread, &(receivedSegment), sizeof(receivedSegment), _findAssociateCP);
			if(indexCP > 0){
			// remove the pointer of CP from retrasmission list and free the memory
				void* pointer = list_remove_index_get_pointer(&(span->structsToSendBySpanThread), indexCP, NULL);
				_free(pointer);
				uint64_t dummy = -1; // -1 because I have remove an element
				write(span->segmentsToSendEventFD, &dummy, sizeof(dummy));
			}
			_free(sendStruct->segment);
			_free(sendStruct);
		}
	}

	// Save acked ranges
	_saveAckedRanges(session, receivedSegment);

	// Resend gaps
	_reportSegmentResendNotReceivedData(session, receivedSegment);

	// Check if everything is received
	if ( !_haveGapInAckedRanges(session) ) {
		doLog("Session (E%d,#%d):\tBlock acked, signaling SUCCESS to upper protocol", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		if ( session->TxSession.handlerSessionTerminated != NULL )
			session->TxSession.handlerSessionTerminated(session->TxSession.dataNumber, SessionResultSuccess); // Signal the upper protocol that this session is completed correctly (RFC 5326 7.4)
		cleanAndCloseSession(session);
	}

	destroySegment(receivedSegment); // Destroy the RS received, no more useful
}

void signalSessionCancelAckSegmentReceived(LTPSpan* span, LTPSegment* receivedSegment) {
	LTPSession* session = list_get_pointer_data(span->activeTxSessions, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), _findSessionFromSessionID); // Get the session from the active sessions

	if ( session == NULL ) {
		doLog("CAS received of an unknown Session (E%d,#%d)", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber);
		destroySegment(receivedSegment);
		return;
	}

	doLog("Session (E%d,#%d):\tCAS received", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);

	if ( session->TxSession.state == CS_SENT) {
		// stop retrasmission timer
		stopTimer(((SendStruct*)session->TxSession.CSWaitingForCAS)->timerID);
		pthread_mutex_lock(&(session->RTXTimersLock));
		list_remove_data(&(session->RTXTimers), &(((SendStruct*)session->TxSession.CSWaitingForCAS)->timerID), sizeof(((SendStruct*)session->TxSession.CSWaitingForCAS)->timerID), NULL);
		pthread_mutex_unlock(&(session->RTXTimersLock));
		int indexCS = list_find(span->structsToSendBySpanThread, &(receivedSegment), sizeof(receivedSegment), _findAssociateCX);
		if(indexCS > 0){
			// remove the pointer of CS from retrasmission list and free the memory
			void* pointer = list_remove_index_get_pointer(&(span->structsToSendBySpanThread), indexCS, NULL);
			_free(pointer);
			uint64_t dummy = -1; // -1 because I have remove an element
			write(span->segmentsToSendEventFD, &dummy, sizeof(dummy));
		}
		// free the memory
		_free(((SendStruct*)session->TxSession.CSWaitingForCAS)->segment);
		_free(session->TxSession.CSWaitingForCAS);
		closeSession(session);  // Put the session in recently close

	}

	destroySegment(receivedSegment);
}

void signalSessionCancelReceiverSegmentReceived(LTPSpan* span, LTPSegment* receivedSegment) {
	LTPSession* session = list_get_pointer_data(span->activeTxSessions, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), _findSessionFromSessionID); // Get the session from the active sessions

	if ((session == NULL) && (list_find(span->recentlyClosedTxSessionIDs, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), _findSessionFromSessionID) > 0)){
		session = list_get_pointer_data(span->recentlyClosedTxSessionIDs, &(receivedSegment->header.sessionID), sizeof(receivedSegment->header.sessionID), _findSessionFromSessionID); // Get the session from the recently closed sessions
		doLog("INFO:\tReceived CR of a terminated session. Sending CAR...");
		_generateCAR(session, span, receivedSegment); // Send CAR
		destroySegment(receivedSegment);
		return;
		}

	if(session == NULL){ // This is not RFC 5326 compliant because we do not send a CAR
		doLog("INFO:\tReceived CR of a unknown session.");
		destroySegment(receivedSegment);
		return;
	}
	else { // If session exists -> destroy it
		_generateCAR(session, span, receivedSegment); // Send CAR
		doLog("Session (E%d,#%d):\tCR received with code %d, signaling CANCEL to upper protocol", receivedSegment->header.sessionID.sessionOriginator, receivedSegment->header.sessionID.sessionNumber, receivedSegment->cancelSegment.cancelCode);
		if ( session->TxSession.handlerSessionTerminated != NULL )
			session->TxSession.handlerSessionTerminated(session->TxSession.dataNumber, SessionResultCancel); // Signal the upper protocol this session is cancelled (RFC 5326 7.5)
		cleanAndCloseSession(session);
	}
	destroySegment(receivedSegment);
}

void closeSession(LTPSession* session){
	//Remove session from active session
	pthread_mutex_t* lock = &(session->span->lock); // Cache because it will be destroyed
		pthread_mutex_lock(lock); // Lock the span

		LTPSessionID sessionID = session->sessionID;

		if ( session->sessionType == RX_SESSION ) { // select the right list
			// README session is no more allocated after the following function
			list_remove_data(&session->span->activeRxSessions, &session->sessionID, sizeof(session->sessionID), _findSessionFromSessionID);
		} else if ( session->sessionType == TX_SESSION ) {
			// README session is no more allocated after the following function
			list_remove_data(&session->span->activeTxSessions, &session->sessionID, sizeof(session->sessionID), _findSessionFromSessionID);
		}

		doLog("Session (E%d,#%d):\tClosed", sessionID.sessionOriginator, sessionID.sessionNumber);

		pthread_mutex_unlock(lock); // Unlock the span

		//Put session in recently closed list
		EndedSession endedSession;
		endedSession.sessionID = session->sessionID;
		gettimeofday(&endedSession.endTime, NULL);
		endedSession.endTime.tv_sec += 2*(session->span->rtoTime * MAX_RETX_NUMBER);



		pthread_mutex_lock(lock); // Lock the span

		if ( session->sessionType == RX_SESSION ) { // select the right list
			list_push_front(&session->span->recentlyClosedRxSessionIDs, &endedSession, sizeof(endedSession));
			/*if(session->RxSession.state != ALL_DATA_RECEIVED) // if all data received the buffer was freed at change of state
				session->span->currentRxBufferUsed--; // Free one RX buffer*/
		} else if ( session->sessionType == TX_SESSION ) {
			list_push_front(&session->span->recentlyClosedTxSessionIDs, &endedSession, sizeof(endedSession));
			sem_post(&(session->span->freeTxConcurrentSessionsSemaphore));
			doLog("Session (E%d,#%d):\tTX buffer freed", sessionID.sessionOriginator, sessionID.sessionNumber);
		}
		doLog("Session (E%d,#%d):\tInserted in recently closed session list", sessionID.sessionOriginator, sessionID.sessionNumber);

		pthread_mutex_unlock(lock); // Unlock the span
}


void generateCR(LTPSession* session, LTPCancelCode cancelCode, unsigned int ipAddress, unsigned short portNumber) {
	LTPSegment cancelReceiverSegment;
	SendStruct sendStruct;

	if(session->TxSession.state == CR_SENT){
			doLog("Session (E%d,#%d):\t CR has already been sent... Do nothing ", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
			return;
		}

	session->RxSession.state = CR_SENT; //change state
	_clearRXBuffer(session);


	cancelReceiverSegment.header.sessionID = session->sessionID;
	cancelReceiverSegment.header.headerExtensionsCount = 0;
	cancelReceiverSegment.header.trailerExtensionsCount = 0;
	cancelReceiverSegment.header.typeFlag = LTPCR;

	cancelReceiverSegment.cancelSegment.cancelCode = cancelCode;

	sendStruct.segment = &cancelReceiverSegment;
	sendStruct.session = session;
	sendStruct.span = session->span;
	sendStruct.numberOfSent = 0;
	sendStruct.ipAddress = ipAddress;
	sendStruct.portNumber = portNumber;

	cleanSession(session);

	sendSegmentWithHandler(sendStruct, _handlerCancelReceiverSent);
}


void generateCS(LTPSession* session, LTPCancelCode cancelCode, unsigned int ipAddress, unsigned short portNumber) {
	LTPSegment cancelSegment;
	SendStruct sendStruct;

	if(session->TxSession.state == CS_SENT){
		doLog("Session (E%d,#%d):\t CS has already been sent... Do nothing ", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		return;
	}

	session->TxSession.state = CS_SENT; //change state

	cancelSegment.header.sessionID = session->sessionID;
	cancelSegment.header.headerExtensionsCount = 0;
	cancelSegment.header.trailerExtensionsCount = 0;
	cancelSegment.header.typeFlag = LTPCS;

	cancelSegment.cancelSegment.cancelCode = cancelCode;

	sendStruct.segment = &cancelSegment;
	sendStruct.session = session;
	sendStruct.span = session->span;
	sendStruct.numberOfSent = 0;
	sendStruct.ipAddress = ipAddress;
	sendStruct.portNumber = portNumber;
	sendStruct.timerID = 0;

	cleanSession(session);

	sendSegmentWithHandler(sendStruct, _handlerCancelSegmentSent); // Here to enable CS re-TX (probably not useful)
}

int findReportSegmentFromReportSegmentSerialNumber(void* _reportSerialNumber, size_t _reportSerialNumberSize, void* _sendStruct, size_t _sendStructSize) {
	unsigned int reportSerialNumber = *((int*) _reportSerialNumber);
	SendStruct* sendStruct = (SendStruct*) _sendStruct;
	if ( sendStruct->segment->reportSegment.reportSerialNumber == reportSerialNumber ) {
		return 0;
	} else {
		return 1;
	}
}


/***** PRIVATE FUNCTIONS *****/

static bool _haveGapInAckedRanges(LTPSession* session) {
	unsigned int lastTo 	= 0;

	for(List current = session->TxSession.listRangeAcked; current != empty_list; current = current->next) {
		AckedRange* currentackedRange = (AckedRange*) current->data;
		if ( currentackedRange->from > lastTo ) { // Gap found
			return true;
		}

		lastTo 		= (lastTo >= currentackedRange->to ? lastTo : currentackedRange->to); // Save the max of the 2
	}

	return ( lastTo != session->block->blockLength );
}

static void _saveAckedRanges(LTPSession* session, LTPSegment* reportSegment) {
	for (int i = 0; i < reportSegment->reportSegment.receptionClaimCount; i++) {
		AckedRange currentAckedRange;
		currentAckedRange.from 	= reportSegment->reportSegment.receptionClaims[i].offset;
		currentAckedRange.to 	= currentAckedRange.from + reportSegment->reportSegment.receptionClaims[i].length;
		list_push_ordered(&(session->TxSession.listRangeAcked), &currentAckedRange, sizeof(currentAckedRange), _insertOrderedAckedRange);
	}
}

static void _sendSegmentsInRange(LTPSession* session, unsigned int rangeFrom, unsigned int rangeTo, unsigned int reportSerialNumber, LTPSegmentTypeCode normalType, LTPSegmentTypeCode lastType) {
	unsigned int oldOffset = rangeFrom;
	unsigned int currentSegmentSentSize;
	for (unsigned int toSend = (rangeTo-rangeFrom); toSend > 0; toSend -= currentSegmentSentSize, oldOffset += currentSegmentSentSize) {

		if ( session->span != NULL ) { // Check if the span is stopped and if it is wait untill it becomes
			waitUntillSpanIsActive(session->span);
		}

		bool isLastSegment = false;
		if ( toSend <= session->span->payloadSegmentSize ) { // have to send more than the segment max size
			currentSegmentSentSize = toSend;
			isLastSegment = true;
		}
		else {
			currentSegmentSentSize = session->span->payloadSegmentSize;
		}

		LTPSegmentTypeCode segmentType = isLastSegment ? lastType : normalType;

		_generateSegmentToSend(session, oldOffset, currentSegmentSentSize, reportSerialNumber, segmentType);
	}
}

static void _generateSegmentToSend(LTPSession* session, unsigned int offset, unsigned int length, unsigned int reportSerialNumber, LTPSegmentTypeCode type) {
	LTPSegment segmentToSend;
	segmentToSend.header.headerExtensionsCount = segmentToSend.header.trailerExtensionsCount = 0;
	segmentToSend.header.typeFlag = type;
	segmentToSend.header.sessionID = session->sessionID;
	segmentToSend.dataSegment.reportSerialNumber = reportSerialNumber;
	segmentToSend.dataSegment.clientServiceID = getUpperProtocolClientServiceID();

	segmentToSend.dataSegment.offset = offset;
	segmentToSend.dataSegment.length = length;
	segmentToSend.dataSegment.data = session->block->data + offset;

	SendStruct sendStruct;
	sendStruct.numberOfSent = 0;
	sendStruct.segment = &segmentToSend;
	sendStruct.session = session;
	sendStruct.span = session->span;
	sendStruct.ipAddress = session->span->ipAddress;
	sendStruct.portNumber = session->span->portNumber;
	sendStruct.timerID = 0;

	if ( isSegmentCheckpoint(segmentToSend) || isOrangeEOBDataSegment(segmentToSend) || isGreenEOBDataSegment(segmentToSend) )
		doLog("Session (E%d,#%d):\tData segment(s) sent", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);


	if ( isSegmentCheckpoint(segmentToSend) || isOrangeEOBDataSegment(segmentToSend) ) {
		static unsigned int currentCheckpointNumber = 1;
		segmentToSend.dataSegment.checkpointSerialNumber = currentCheckpointNumber;
		currentCheckpointNumber++; if (currentCheckpointNumber==0) currentCheckpointNumber++; // To avoid using 0
	}

	if ( isSegmentCheckpoint(segmentToSend) )
		sendSegmentWithHandler(sendStruct, _handlerCheckpointSent);
	else
		sendSegment(sendStruct);
}

static bool _reportSegmentResendNotReceivedData(LTPSession* session, LTPSegment* reportSegment) {
	bool result = false;
	unsigned int currentOffset = reportSegment->reportSegment.lowerBound;

	struct {
		unsigned int from;
		unsigned int to;
	} lastRangeToRetransmit;
	bool isLastRangeSetted = false;

	for (int i = 0; i < reportSegment->reportSegment.receptionClaimCount; i++) {
		if ( reportSegment->reportSegment.lowerBound+reportSegment->reportSegment.receptionClaims[i].offset > currentOffset ) { // Gap, something is missing
			if ( !isLastRangeSetted ) { // is first gap I found
				isLastRangeSetted = true;
			} else { // Is not the first gap
				doLog("Session (E%d,#%d):\tGap in RS between %d and %d", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, lastRangeToRetransmit.from+1, lastRangeToRetransmit.to);
				_sendSegmentsInRange(session, lastRangeToRetransmit.from, lastRangeToRetransmit.to, reportSegment->reportSegment.reportSerialNumber, LTPDSRed, LTPDSRed);
			}
			lastRangeToRetransmit.from = currentOffset;
			lastRangeToRetransmit.to = reportSegment->reportSegment.lowerBound+reportSegment->reportSegment.receptionClaims[i].offset;
			result = true;
		}
		currentOffset = reportSegment->reportSegment.lowerBound+reportSegment->reportSegment.receptionClaims[i].offset + reportSegment->reportSegment.receptionClaims[i].length;
	}

	if ( reportSegment->reportSegment.upperBound > currentOffset ) {
		if ( !isLastRangeSetted ) { // is first gap I found
			isLastRangeSetted = true;
		} else { // Is not the first gap
			doLog("Session (E%d,#%d):\tGap in RS between %d and %d", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, lastRangeToRetransmit.from+1, lastRangeToRetransmit.to);
			_sendSegmentsInRange(session, lastRangeToRetransmit.from, lastRangeToRetransmit.to, reportSegment->reportSegment.reportSerialNumber, LTPDSRed, LTPDSRed);
		}
		lastRangeToRetransmit.from = currentOffset;
		lastRangeToRetransmit.to = reportSegment->reportSegment.upperBound;
		result = true;
	}

	if ( isLastRangeSetted ) { // send the last gap + a checkpoint
		doLog("Session (E%d,#%d):\tGap in RS between %d and %d", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, lastRangeToRetransmit.from+1, lastRangeToRetransmit.to);
		_sendSegmentsInRange(session, lastRangeToRetransmit.from, lastRangeToRetransmit.to, reportSegment->reportSegment.reportSerialNumber, LTPDSRed, LTPDSRedCheckpoint);
	}

	return result;
}



static bool _addSegmentToSession(LTPSession *session, LTPSegment* segment) {
	if ( session == NULL ) return false;
	if ( _existsSegmentInSession(segment, session) ) {
		doLog("Session (E%d,#%d):\tArrived doubled segment, ignorig it", segment->header.sessionID.sessionOriginator, segment->header.sessionID.sessionNumber);
		return false;
	} else { // not exists -> add
		pthread_mutex_lock(&(session->RxSession.segmentListLock));
		list_push_ordered(&(session->RxSession.segmentList), segment, sizeof(LTPSegment), _insertOrderedSegment);
		pthread_mutex_unlock(&(session->RxSession.segmentListLock));
		return true;
	}
}

static bool _existsSegmentInSession(LTPSegment* newSegment, LTPSession *session) {
	List 			current;
	unsigned int 	oldOffset = 0;
	unsigned int 	oldEnd = 0;
	bool			isFirstRow = true;

	pthread_mutex_lock(&(session->RxSession.segmentListLock));

	if ( session->RxSession.segmentList == empty_list ) { // List empty -> not exists
		pthread_mutex_unlock(&(session->RxSession.segmentListLock));
		return false;
	}

	unsigned int 	newSegmentEnd = newSegment->dataSegment.offset+newSegment->dataSegment.length;

	for (current = session->RxSession.segmentList; current != empty_list; current = current->next) {
		LTPSegment* currentSegment = (LTPSegment*) current->data;
		const unsigned int currentSegmentEnd = currentSegment->dataSegment.offset+currentSegment->dataSegment.length;
		const unsigned int currentSegmentOffset = currentSegment->dataSegment.offset;

		if ( isFirstRow ) {
			if ( newSegment->dataSegment.offset >= currentSegmentEnd ) {
				pthread_mutex_unlock(&(session->RxSession.segmentListLock));
				return false; // Segment doesn't exist
			}
			oldOffset = currentSegmentOffset;
			oldEnd = currentSegmentEnd;
			isFirstRow = false;
			continue;
		}

		if ( currentSegmentEnd < oldOffset ) { // Gap found
			if ( ( 			newSegment->dataSegment.offset >= currentSegmentEnd			// Segment is in the gap
						&&	newSegmentEnd <= oldOffset									// Example: received is 0-100 and 200-300 and segment is 100-200
					) || (
							newSegment->dataSegment.offset < currentSegmentEnd			// Segment is half in gap and half in received
						&&	newSegmentEnd > currentSegmentEnd							// Example: received 0-100 and segment is 50-150
					) || (
							newSegmentEnd > oldOffset									// Segment is half in gap and half in received
						&&	newSegment->dataSegment.offset < oldOffset					// Example: received 0-100 and 400-500 and segment is 350-450
					)
				) {
				pthread_mutex_unlock(&(session->RxSession.segmentListLock));
				return false; // Segment doesn't exist
			}
			oldEnd = currentSegmentEnd;
		}

		if ( newSegment->dataSegment.offset >= currentSegmentOffset ) {
			pthread_mutex_unlock(&(session->RxSession.segmentListLock));
			return true; // Segment exists
		}

		oldOffset = currentSegmentOffset;
	}

	if ( current == empty_list && newSegment->dataSegment.offset < oldOffset ) {
		pthread_mutex_unlock(&(session->RxSession.segmentListLock));
		return false; // Segment doesn't exist
	}

	pthread_mutex_unlock(&(session->RxSession.segmentListLock));

	return true;
}

static List _getClaimsFromSession(LTPSession* session, unsigned int lowerByteToAck, const unsigned int upperByteToAck, bool* isLastRS) {
	List 				current = empty_list;
	List 				claims  = empty_list;
	LTPReportSegmentClaim 	currentClaim;

	if ( session->RxSession.state == ALL_DATA_RECEIVED ) { // Block is delivered to UP -> all is arrived, generated a single claim to ACK everything
		currentClaim.offset = 0;
		currentClaim.length = session->block->blockLength;
		list_push_front(&claims, &currentClaim, sizeof(LTPReportSegmentClaim));
		if ( isLastRS != NULL )
			*isLastRS = true;
		return claims;
	} else { // Block is not delivered to UP
		if ( isLastRS != NULL )
			*isLastRS = false;
	}

	pthread_mutex_lock(&(session->RxSession.segmentListLock));

	if ( session->RxSession.segmentList == empty_list ) {
		pthread_mutex_unlock(&(session->RxSession.segmentListLock));
		return empty_list;
	}

	current = session->RxSession.segmentList;

	currentClaim.offset = 0;
	currentClaim.length = 0;

	while (current != empty_list) {
		LTPSegment* currentSegment = (LTPSegment*) current->data;
		if ( currentSegment->dataSegment.offset + currentSegment->dataSegment.length <= upperByteToAck ) {
			currentClaim.length = currentSegment->dataSegment.length;
			currentClaim.offset = currentSegment->dataSegment.offset;
			current = current->next;
			break;
		}
		current = current->next;
	}
	for (; current != empty_list; current = current->next) {
		LTPSegment* currentSegment = (LTPSegment*) current->data;

		if ( (currentSegment->dataSegment.offset + currentSegment->dataSegment.length) <= lowerByteToAck ) {
			break; // Going over the lowerByteToAck
		}

		if ( currentClaim.offset > (currentSegment->dataSegment.offset + currentSegment->dataSegment.length) ) {  	// Gap found
			list_push_front(&claims, &currentClaim, sizeof(LTPReportSegmentClaim));
			currentClaim.length = currentSegment->dataSegment.length;
		} else { 																									// No gap
			currentClaim.length += currentSegment->dataSegment.length; // Increase the claim length
		}

		currentClaim.offset = currentSegment->dataSegment.offset;
	}

	list_push_front(&claims, &currentClaim, sizeof(LTPReportSegmentClaim));

	pthread_mutex_unlock(&(session->RxSession.segmentListLock));
	return claims;
}

static void _checkAllDataReceived(LTPSession* session) {
	LTPBlock*			block = session->block;
	List 				current;
	unsigned int 		nextOffset = 0;
	unsigned int 		oldOffset;
	bool				gapFound = false;
	bool				isAllDataReceived = false;

	pthread_mutex_lock(&(session->RxSession.segmentListLock));
	current = session->RxSession.segmentList;

	if ( session->RxSession.state == ALL_DATA_RECEIVED) { // Delivered yet -> Skip it
		pthread_mutex_unlock(&(session->RxSession.segmentListLock));
		return;
	}

	if ( !session->RxSession.isEOBarrived || session->RxSession.segmentList == empty_list ) { // EOB is not arrived OR there are no segments -> is not fully arived
		pthread_mutex_unlock(&(session->RxSession.segmentListLock));
		return;
	}

	// First row to set oldOffset
	if ( current != empty_list ) {
		LTPSegment* currentSegment = (LTPSegment*) current->data;
		if ( (currentSegment->dataSegment.offset + currentSegment->dataSegment.length) != block->blockLength ) { // Not the last segment
			gapFound = true;
		} else {
			oldOffset = currentSegment->dataSegment.offset;
		}
		current = current->next;
	}

	// Cycle looking for gaps
	for (; current != empty_list && !gapFound; current = current->next) {
		LTPSegment* currentSegment = (LTPSegment*) current->data;
		if ( currentSegment->dataSegment.offset + currentSegment->dataSegment.length < oldOffset ) {   // Gap found
			gapFound = true;
			break; // Gap found -> exit cycle
		}
		oldOffset = currentSegment->dataSegment.offset;
	}

	isAllDataReceived = ( (!gapFound) && (oldOffset == 0) ); // if (haven't found any gaps) AND (last segment offset is 0) ->  All data arrived

	if ( isAllDataReceived ) {
		// "compact" the segments into the block.data array
		session->block->data = _malloc(session->block->blockLength);
		for (current = session->RxSession.segmentList; current != empty_list; current = current->next ) { // For each segment copy the data in the array
			LTPSegment* currentSegment = (LTPSegment*) current->data;
			memcpy((session->block->data+currentSegment->dataSegment.offset), currentSegment->dataSegment.data, currentSegment->dataSegment.length);
		}
		// Destroy the old segment list & decrement the currentRXBufferUsed
		_clearRXBuffer(session);

		doLog("Session (E%d,#%d):\tBlock completed; to be delivered to upper protocol", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
		deliverBlockToUpperProtocol(session->block); // TO DO: check result

		_free(session->block->data); // Free the data delivered to upper protocol
		session->block->data = NULL;

		// it is delivered --> Change the state
		session->RxSession.state = ALL_DATA_RECEIVED;
		//session->span->currentRxBufferUsed--; // XXX Free one RX buffer
	}

	pthread_mutex_unlock(&(session->RxSession.segmentListLock));
}

static void _clearRXBuffer(LTPSession* session) {
	pthread_mutex_lock(&(session->RxSession.segmentListLock));

	if ( session == NULL || session->sessionType != RX_SESSION || session->RxSession.segmentList == empty_list) {
		pthread_mutex_unlock(&(session->RxSession.segmentListLock));
		return;
	}

	list_for_each(session->RxSession.segmentList, _forEachDestroySegment);
	list_destroy(&(session->RxSession.segmentList));

	session->span->currentRxBufferUsed--;
	doLog("Session (E%d,#%d):\tRX buffer freed", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);

	pthread_mutex_unlock(&(session->RxSession.segmentListLock));
}

static LTPBlock* _newBlock() {
	LTPBlock* result = (LTPBlock*) _malloc(sizeof(LTPBlock));

	result->data = NULL;
	result->blockLength = 0;

	return result;
}

static void _destroyBlock(LTPSession* session) {
	if (session == NULL || session->block == NULL)
		return;

	_free(session->block->data);

	_free(session->block);
	session->block = NULL;
}

static void _generateRS(LTPSession* session, LTPSpan* span, List claims, unsigned int checkpointSerialNumber, const unsigned int lowerByteToAck) {
	LTPSegment reportSegment;
	unsigned int lastUpperBound = lowerByteToAck;

	int currentNumberOfClaims = 0;
	const int totalNumberOfClaims = list_length(claims);
	static int reportNumber = 0;
	if (reportNumber == 0) {
		reportNumber = rand() % 1024 + 1;
	}

	reportSegment.header.typeFlag = LTPRS;
	reportSegment.header.sessionID = session->sessionID;
	reportSegment.header.headerExtensionsCount = 0;
	reportSegment.header.trailerExtensionsCount = 0;
	reportSegment.reportSegment.checkpointSerialNumber = checkpointSerialNumber;

	while (currentNumberOfClaims < totalNumberOfClaims) {
		const int claimsStillToSend = totalNumberOfClaims - currentNumberOfClaims;
		const int claimSent = (MAX_NUMBER_OF_CLAIMS_IN_REPORT_SEGMENT <= claimsStillToSend ? MAX_NUMBER_OF_CLAIMS_IN_REPORT_SEGMENT : claimsStillToSend);
		reportSegment.reportSegment.receptionClaimCount = claimSent;

		// Set this lowerBound to the last claim sent
		reportSegment.reportSegment.lowerBound = lastUpperBound;

		reportSegment.reportSegment.reportSerialNumber = reportNumber;
		reportNumber++; if ( reportNumber == 0 ) reportNumber++; // Protect to overflow

		reportSegment.reportSegment.receptionClaims = _malloc(sizeof(LTPReportSegmentClaim) * claimSent);

		doLog("Session (E%d,#%d):\tBuilding RS %d in response to CP %d\tLower Bound = %d\t", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, reportSegment.reportSegment.reportSerialNumber, reportSegment.reportSegment.checkpointSerialNumber, reportSegment.reportSegment.lowerBound);
		int i;
		for(i = 0; i < claimSent && claims != empty_list; i++, claims = claims->next) {
			reportSegment.reportSegment.receptionClaims[i] = *((LTPReportSegmentClaim*)claims->data);

			if ( reportSegment.reportSegment.receptionClaims[i].offset != 0 ) { // Don't do if the claim is the "confirm all claim" which starts from 0
				// Claim offset in relative to the RS lower & upper bounds -> change from absolute to relative
				reportSegment.reportSegment.receptionClaims[i].offset -= reportSegment.reportSegment.lowerBound;
			}
			doLog("Session (E%d,#%d):\tClaim #%d.\tOffset = %d\tLength = %d\tAbsolute ranges: %d - %d", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, i+1, reportSegment.reportSegment.receptionClaims[i].offset, reportSegment.reportSegment.receptionClaims[i].length, reportSegment.reportSegment.lowerBound+reportSegment.reportSegment.receptionClaims[i].offset+1, reportSegment.reportSegment.lowerBound+reportSegment.reportSegment.receptionClaims[i].offset+reportSegment.reportSegment.receptionClaims[i].length);
		}

		// Set the upperBound to the last byte acked by the last claim added
		lastUpperBound = reportSegment.reportSegment.lowerBound + reportSegment.reportSegment.receptionClaims[i-1].offset + reportSegment.reportSegment.receptionClaims[i-1].length;
		reportSegment.reportSegment.upperBound = lastUpperBound;

		currentNumberOfClaims += claimSent;

		if ( session->RxSession.state == ALL_DATA_RECEIVED ) { // This is the last RS --> Save the RS number
			session->RxSession.lastRSSentNumber = reportSegment.reportSegment.reportSerialNumber;
		}

		SendStruct sendStruct;
		sendStruct.segment = &reportSegment;
		sendStruct.session = session;
		sendStruct.span = span;
		sendStruct.numberOfSent = 0;
		sendStruct.ipAddress = span->ipAddress;
		sendStruct.portNumber = span->portNumber;
		sendStruct.timerID = 0;
		sendSegmentWithHandler(sendStruct, _handlerReportSegmentSent);
	}
}

static void _generateRA(LTPSession* session, LTPSpan* span, LTPSegment* receivedSegment) {
	LTPSegment reportSegmentAck;
	reportSegmentAck.header = receivedSegment->header;
	reportSegmentAck.header.typeFlag = LTPRA;
	reportSegmentAck.reportAckSegment.reportSerialNumber = receivedSegment->reportSegment.reportSerialNumber;
	SendStruct sendStructRA;
	sendStructRA.numberOfSent = 0;
	sendStructRA.session = session;
	sendStructRA.span = span;
	sendStructRA.segment = &reportSegmentAck;
	sendStructRA.ipAddress = span->ipAddress;
	sendStructRA.portNumber = span->portNumber;
	sendSegment(sendStructRA);

	if ( session != NULL )
		doLog("Session (E%d,#%d):\tSent RA to confirm RS %d", session->sessionID.sessionOriginator, session->sessionID.sessionNumber, receivedSegment->reportAckSegment.reportSerialNumber);
}

static void _generateCAR(LTPSession* session, LTPSpan* span, LTPSegment* cancelSegment) {
	LTPSegment cancelReceiverAck;
	SendStruct sendStruct;

	cancelReceiverAck.header.sessionID = cancelSegment->header.sessionID;
	cancelReceiverAck.header.headerExtensionsCount = 0;
	cancelReceiverAck.header.trailerExtensionsCount = 0;
	cancelReceiverAck.header.typeFlag = LTPCAR;

	sendStruct.segment = &cancelReceiverAck;
	sendStruct.session = session;
	sendStruct.span = span;
	sendStruct.numberOfSent = 0;
	sendStruct.ipAddress = span->ipAddress;
	sendStruct.portNumber = span->portNumber;
	sendSegment(sendStruct);
}

static void _generateCAS(LTPSession* session, LTPSpan* span, LTPSegment* cancelSegment) {
	LTPSegment cancelSegmentAck;
	SendStruct sendStruct;

	cancelSegmentAck.header.sessionID = cancelSegment->header.sessionID;
	cancelSegmentAck.header.headerExtensionsCount = 0;
	cancelSegmentAck.header.trailerExtensionsCount = 0;
	cancelSegmentAck.header.typeFlag = LTPCAS;

	sendStruct.segment = &cancelSegmentAck;
	sendStruct.session = session;
	sendStruct.span = span;
	sendStruct.numberOfSent = 0;
	sendStruct.ipAddress = span->ipAddress;
	sendStruct.portNumber = span->portNumber;
	sendSegment(sendStruct);
}

/***** FOR EACH FUNCTION *****/

static void _forEachDestroySegment(void* data, size_t size) {
	destroySegment((LTPSegment*) data);
}

/***** HANDLER FUNCTIONS *****/


static void _handlerSessionTimeoutTX(TimerID timerID, void* _session) {
	LTPSession* session = (LTPSession*) _session;

	doLog("Session (E%d,#%d):\tExpired, signaling CANCEL to upper protocol", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);
	if ( session->TxSession.handlerSessionTerminated != NULL )
		session->TxSession.handlerSessionTerminated(session->TxSession.dataNumber, SessionResultCancel); // Signal the upper protocol this session is completed correctly
	CancelStruct CSStruct = createCancelStruct(CS, SESSION_TIMEOUT, session);
	forwordingSpanNewCancelStructToSend(&CSStruct);
}


static void _handlerSessionTimeoutRX(TimerID timerID, void* _session) {
	LTPSession* session = (LTPSession*) _session;
	doLog("Session (E%d,#%d):\tExpired", session->sessionID.sessionOriginator, session->sessionID.sessionNumber);

	if ( session->sessionColor == SessionColorOrange || session->sessionColor == SessionColorRed ) {
		_clearRXBuffer(session);
		CancelStruct CRStruct = createCancelStruct(CR, SESSION_TIMEOUT, session);
		forwordingSpanNewCancelStructToSend(&CRStruct);
		
	} else {
		cleanAndCloseSession(session);
	}
}

static void _handlerCancelSegmentSent(SendStruct sentStruct) {
	doLog("Session (E%d,#%d):\tCS sent with code %d, waiting for CAS", sentStruct.session->sessionID.sessionOriginator, sentStruct.session->sessionID.sessionNumber, sentStruct.segment->cancelSegment.cancelCode);

	sentStruct.session->TxSession.CSWaitingForCAS = (SendStruct*) _malloc(sizeof(SendStruct));
	memcpy(sentStruct.session->TxSession.CSWaitingForCAS, &sentStruct, sizeof(SendStruct));
	((SendStruct*)sentStruct.session->TxSession.CSWaitingForCAS)->timerID = startTimer(1000 * sentStruct.session->span->rtoTime / (DISTRIBUTED_COPIES ? SIGNAL_SEGMENTS_COPIES : 1), forwordingSpanNewSendStructToSend, sentStruct.session->TxSession.CSWaitingForCAS);

}


static void _handlerCancelReceiverSent(SendStruct sentStruct) {
	doLog("Session (E%d,#%d):\tCR sent with code %d, waiting for CAR", sentStruct.session->sessionID.sessionOriginator, sentStruct.session->sessionID.sessionNumber, sentStruct.segment->cancelSegment.cancelCode);

	sentStruct.session->RxSession.CRWaitingForCAR =  (SendStruct*) _malloc(sizeof(SendStruct));
	memcpy(sentStruct.session->RxSession.CRWaitingForCAR,&sentStruct, sizeof(SendStruct));
	((SendStruct*)sentStruct.session->RxSession.CRWaitingForCAR)->timerID = startTimer(1000 * sentStruct.session->span->rtoTime / (DISTRIBUTED_COPIES ? SIGNAL_SEGMENTS_COPIES : 1), forwordingSpanNewSendStructToSend, sentStruct.session->RxSession.CRWaitingForCAR);

}



static void _handlerCheckpointSent(SendStruct sentStruct) {
	if ( sentStruct.segment->dataSegment.reportSerialNumber > 0 ) {
		doLog("Session (E%d,#%d):\tCP %u sent in response to RS %d. Waiting for RS", sentStruct.session->sessionID.sessionOriginator, sentStruct.session->sessionID.sessionNumber, sentStruct.segment->dataSegment.checkpointSerialNumber, sentStruct.segment->dataSegment.reportSerialNumber);
	} else {
		doLog("Session (E%d,#%d):\tCP %u sent, waiting for RS", sentStruct.session->sessionID.sessionOriginator, sentStruct.session->sessionID.sessionNumber, sentStruct.segment->dataSegment.checkpointSerialNumber);
	}
	SendStruct* addedSendStruct = list_push_front(&(sentStruct.session->TxSession.CPWaitingForRS), &sentStruct, sizeof(SendStruct));

	addedSendStruct->timerID = startTimer(1000 * sentStruct.session->span->rtoTime / (DISTRIBUTED_COPIES ? SIGNAL_SEGMENTS_COPIES : 1), forwordingSpanNewSendStructToSend, addedSendStruct);
	pthread_mutex_lock(&(sentStruct.session->RTXTimersLock));
	list_append(&(sentStruct.session->RTXTimers), &addedSendStruct->timerID, sizeof(addedSendStruct->timerID));
	pthread_mutex_unlock(&(sentStruct.session->RTXTimersLock));
}


static void _handlerReportSegmentSent(SendStruct sentStruct) {
	doLog("Session (E%d,#%d):\tRS %d sent, waiting for RA", sentStruct.session->sessionID.sessionOriginator, sentStruct.session->sessionID.sessionNumber, sentStruct.segment->reportSegment.reportSerialNumber);

	if (    sentStruct.segment->reportSegment.receptionClaimCount > 1   // More than 1 claim
		||  ( 		sentStruct.segment->reportSegment.receptionClaimCount == 1 // 1 claim but not cover everything
				&& 	sentStruct.segment->reportSegment.lowerBound != sentStruct.segment->reportSegment.receptionClaims[0].offset
				&& 	sentStruct.segment->reportSegment.upperBound != sentStruct.segment->reportSegment.lowerBound + sentStruct.segment->reportSegment.receptionClaims[0].offset + sentStruct.segment->reportSegment.receptionClaims[0].length
			)
		) {
		LTPSegment RS;
		RS.header = sentStruct.segment->header;
		RS.reportSegment = sentStruct.segment->reportSegment;
		RS.reportSegment.receptionClaims = (LTPReportSegmentClaim*) _malloc(sizeof(LTPReportSegmentClaim) * sentStruct.segment->reportSegment.receptionClaimCount);
		for (int i = 0; i < RS.reportSegment.receptionClaimCount; i++) {
			RS.reportSegment.receptionClaims[i] = sentStruct.segment->reportSegment.receptionClaims[i];
		}
		list_push_front(&(sentStruct.session->RxSession.RSWaitingForCP), &RS, sizeof(LTPSegment));
	}

	// Add to resend list
	SendStruct* addedSendStruct = (SendStruct*) list_push_front(&(sentStruct.session->RxSession.RSWaitingForRA), &sentStruct, sizeof(SendStruct));
	addedSendStruct->timerID = startTimer(1000 * addedSendStruct->session->span->rtoTime / (DISTRIBUTED_COPIES ? SIGNAL_SEGMENTS_COPIES : 1), forwordingSpanNewSendStructToSend, addedSendStruct);
	pthread_mutex_lock(&(sentStruct.session->RTXTimersLock));
	list_append(&(sentStruct.session->RTXTimers), &addedSendStruct->timerID, sizeof(addedSendStruct->timerID));
	pthread_mutex_unlock(&(sentStruct.session->RTXTimersLock));
}

static void _destroyRSWaitingForRetransmission(void* data, size_t size) {
	SendStruct* sendStruct = (SendStruct*) data;
	stopTimer(sendStruct->timerID);
	pthread_mutex_lock(&(sendStruct->session->RTXTimersLock));
	list_remove_data(&(sendStruct->session->RTXTimers), &sendStruct->timerID, sizeof(sendStruct->timerID), NULL);
	pthread_mutex_unlock(&(sendStruct->session->RTXTimersLock));
	destroySegment(sendStruct->segment);
	_free(sendStruct->segment);
	sendStruct->segment = NULL;
}

static void _destroyRSWaitingForCP(void* data, size_t size) {
	LTPSegment* segment = (LTPSegment*) data;
	destroySegment(segment);
}

static void _destroyCPWaitingForRS(void* data, size_t size) {
	SendStruct* sendStruct = (SendStruct*) data;
	stopTimer(sendStruct->timerID);
	pthread_mutex_lock(&(sendStruct->session->RTXTimersLock));
	list_remove_data(&(sendStruct->session->RTXTimers), &sendStruct->timerID, sizeof(sendStruct->timerID), NULL);
	pthread_mutex_unlock(&(sendStruct->session->RTXTimersLock));
	_free(sendStruct->segment);
	sendStruct->segment = NULL;
}


static void _freezeSessionTimer(void* data, size_t size) {
	TimerID* timerID = (TimerID*) data;
	pauseTimer(*timerID);
}

static void _resumeSessionTimer(void* data, size_t size) {
	TimerID* timerID = (TimerID*) data;
	resumeTimer(*timerID);
}

static void _stopSessionTimer(void* data, size_t size) {
	TimerID* timerID = (TimerID*) data;
	stopTimer(*timerID);
}

/***** COMPARE FUNCTIONS *****/

/*
 * Descending order of offset value. The offset will be descending
 */
static int _insertOrderedSegment(void* newSegment, size_t newSegmentSize, void* segment, size_t segmentSize) {
	return ((LTPSegment*) segment)->dataSegment.offset - ((LTPSegment*) newSegment)->dataSegment.offset;
}

static int _insertOrderedAckedRange(void* _newAckedRange, size_t newAckedRangeSize, void* _currentAckedRange, size_t currentAckedSize) {
	AckedRange* newAckedRange = (AckedRange*) _newAckedRange;
	AckedRange* currentAckedRange = (AckedRange*) _currentAckedRange;
	return (newAckedRange->from - currentAckedRange->from);
}


static int _findCheckpointFromCheckpointNumber(void* _checkpointNumber, size_t checkpointNumberSize, void* _sendStruct, size_t sendStructSize) {
	unsigned int checkpointNumber = *((unsigned int*) _checkpointNumber);
	SendStruct* sendStruct = (SendStruct*) _sendStruct;
	if ( sendStruct->segment->dataSegment.checkpointSerialNumber == checkpointNumber ) {
		return 0;
	} else {
		return 1; // not equals
	}
}



static int _findReportSegmentFromCheckpoint(void* _reportSerialNumber, size_t _reportSerialNumberSize, void* _reportSegment, size_t _reportSegmentSize) {
	unsigned int reportSerialNumber = *((int*) _reportSerialNumber);
	LTPSegment* reportSegment = (LTPSegment*) _reportSegment;
	if ( reportSegment->reportSegment.reportSerialNumber == reportSerialNumber ) {
		return 0;
	} else {
		return 1;
	}
}

/*
 * Finds in the list the session from a LTPSessionID
 */
static int _findSessionFromSessionID(void* _sessionID, size_t _sessionID_size, void* _session, size_t _sessionSize) {
	LTPSessionID* 	sessionID 	= (LTPSessionID*) _sessionID;
	LTPSession*		session 	= (LTPSession*)	_session;
	if (	sessionID->sessionOriginator == session->sessionID.sessionOriginator
		&& 	sessionID->sessionNumber == session->sessionID.sessionNumber) {
		return 0;
	} else {
		return 1; // not equals
	}
}

static int _findAssociateCP (void* _RSSegment, size_t RSSize, void* _data, size_t dataSize){
	if(sizeof(_data) == sizeof(SendStruct**)){
		SendStruct** temp = _data;
		SendStruct* sendStruct = (SendStruct*) temp;
		LTPSegment* RSSegment = (LTPSegment*) _RSSegment;
		// search CP with the same SessionID and CP SerialNumber
		if( isSegmentCheckpoint(*(sendStruct->segment)) &&
			sendStruct->session->sessionID.sessionNumber == RSSegment->header.sessionID.sessionNumber &&
			sendStruct->session->sessionID.sessionOriginator == RSSegment->header.sessionID.sessionOriginator &&
			sendStruct->segment->dataSegment.checkpointSerialNumber == RSSegment->reportSegment.checkpointSerialNumber)
			return 0;
	}
	return 1;
}

static int _findAssociateRSforRA (void* _RASegment, size_t RASize, void* _data, size_t dataSize){
	if(sizeof(_data) == sizeof(SendStruct**)){
		SendStruct** temp = _data;
		SendStruct* sendStruct = (SendStruct*) temp;
		LTPSegment* RASegment = (LTPSegment*) _RASegment;
		// search RS with the same SessionID and Report Serial Number
		if( isReportSegment(*(sendStruct->segment)) &&
			sendStruct->session->sessionID.sessionNumber == RASegment->header.sessionID.sessionNumber &&
			sendStruct->session->sessionID.sessionOriginator == RASegment->header.sessionID.sessionOriginator &&
			sendStruct->segment->reportSegment.reportSerialNumber == RASegment->reportSegment.reportSerialNumber)
			return 0;
	}
	return 1;
}

static int _findAssociateRSforCP (void* _CPSegment, size_t CPSize, void* _data, size_t dataSize){
	if(sizeof(_data) == sizeof(SendStruct**)){
		SendStruct** temp = _data;
		SendStruct* sendStruct = (SendStruct*) temp;
		LTPSegment* CPSegment = (LTPSegment*) _CPSegment;
		// search RS with the same SessionID and Report Serial Number
		if( isReportSegment(*(sendStruct->segment)) &&
			sendStruct->session->sessionID.sessionNumber == CPSegment->header.sessionID.sessionNumber &&
			sendStruct->session->sessionID.sessionOriginator == CPSegment->header.sessionID.sessionOriginator &&
			sendStruct->segment->reportSegment.reportSerialNumber == CPSegment->dataSegment.reportSerialNumber)
			return 0;
	}
	return 1;
}

static int _findAssociateCX (void* _CAXSegment, size_t CAXSize, void* _data, size_t dataSize){
	if(sizeof(_data) == sizeof(SendStruct**)){
		SendStruct** temp = _data;
		SendStruct* sendStruct = (SendStruct*) temp;
		LTPSegment* CAXSegment = (LTPSegment*) _CAXSegment;
		// search CX with the same SessionID
		if( isCXSegment(*(sendStruct->segment)) &&
			sendStruct->session->sessionID.sessionNumber == CAXSegment->header.sessionID.sessionNumber &&
			sendStruct->session->sessionID.sessionOriginator == CAXSegment->header.sessionID.sessionOriginator )
			return 0;
	}
	return 1;
}
