/** \file session.h
 *
 * \brief This file contains the headers of the LTPsession functions and related structures
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_LTPSESSION_SESSION_H_
#define SRC_LTPSESSION_SESSION_H_


#include "sessionStruct.h"


#include "../list/list.h"
#include "../LTPsegment/segment.h"


#include <stdbool.h>



/**
 * \par Function Name:
 *      setMyNodeNumber
 *
 * \brief  Sets the node number
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  _myNodeNumber 		The nodeNumber
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				setMyNodeNumber(unsigned long long _myNodeNumber);

/**
 * \par Function Name:
 *      getMyNodeNumber
 *
 * \brief  Gets my node number
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return unsigned long long
 *
 * \retval  My node number
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
unsigned long long 	getMyNodeNumber();

/**
 * \par Function Name:
 *      createSession
 *
 * \brief  Creates a session
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  session 					The session to create
 * \param  span						The span
 * \param  sessionOriginator		The session originator
 * \param  sessionNumber 			The session number
 * \param  sessionColor				The color of the session
 * \param  sessionType 				The session type (RX / TX)
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				createSession(LTPSession* session, LTPSpan* span, unsigned long long sessionOriginator, unsigned int sessionNumber, LTPSessionColor sessionColor, SessionType sessionType);

/**
 * \par Function Name:
 *      freezeSession
 *
 * \brief  Freezes all timer in this session
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  session 		The session to freeze
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void		 		freezeSession(LTPSession* session);

/**
 * \par Function Name:
 *      resumeSession
 *
 * \brief  Resumes the session
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  session 		The session to resume
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void				resumeSession(LTPSession* session);

/**
 * \par Function Name:
 *     cleanAndCloseSession
 *
 * \brief  Destroys the session
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  session 		The session to destroy
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void				cleanAndCloseSession(LTPSession* session);


/**
 * \par Function Name:
 *      cleanSession
 *
 * \brief  Cleans the session
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  session 		The session
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				cleanSession(LTPSession* session);

/**
 * \par Function Name:
 *      createNewSendSession
 *
 * \brief  Creates new session to send data
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 			The span
 * \param  data				The data to send
 * \param  dataLength		The length of the data
 * \param  dataNumber		The dataNumber
 * \param  handlerSessionTerminated The handler to call the session terminates
 * \param  sessionColor		The session color
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				createNewSendSession(LTPSpan* span, void* data, int dataLength, unsigned int dataNumber, void (*handlerSessionTerminated)(unsigned int, LTPSessionResultCode), LTPSessionColor sessionColor);

/***** RECEIVER *****/

/**
 * \par Function Name:
 *      signalSessionNewDataSegmentReceived
 *
 * \brief  Called when a new data segment is received
 *
 *
 * \par Date Written:
 *      20/04/21
 *
 * \return bool
 *
 * \retval  True if segment is processed well, false otherwise
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 * \param  sessionColor     The color of the segment
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  20/04/21 | D. Filoni       |  Initial Implementation and documentation.
 *****************************************************************************/
bool 				signalSessionNewDataSegmentReceived(LTPSpan* span, LTPSegment* receivedSegment, LTPSessionColor sessionColor);
/**
 * \par Function Name:
 *      signalSessionNewGreenEOBDataSegmentReceived
 *
 * \brief  Called when a green EOB data segment is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \retval  True if segment is processed well, false otherwise
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				signalSessionGreenEOBDataSegmentReceived(LTPSpan* span, LTPSegment* receivedSegment);

/**
 * \par Function Name:
 *      signalSessionNewOrangeEOBDataSegmentReceived
 *
 * \brief  Called when a orange EOB data segment is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \retval  True if segment is processed well, false otherwise
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				signalSessionOrangeEOBDataSegmentReceived(LTPSpan* span, LTPSegment* receivedSegment);
/**
 * \par Function Name:
 *      signalSessionNewCheckpointReceived
 *
 * \brief  Called when a new CP is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				signalSessionCheckpointReceived(LTPSpan* span, LTPSegment* receivedSegment);
/**
 * \par Function Name:
 *      signalSessionNewReportAckSegmentReceived
 *
 * \brief  Called when a new RA is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				signalSessionReportAckSegmentReceived(LTPSpan* span, LTPSegment* receivedSegment);
/**
 * \par Function Name:
 *      signalSessionNewCancelSegmentReceived
 *
 * \brief  Called when a new CS is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				signalSessionCancelSegmentReceived(LTPSpan* span, LTPSegment* receivedSegment);
/**
 * \par Function Name:
 *      signalSessionNewCancelAckReceiverReceived
 *
 * \brief  Called when a new CAR is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				signalSessionCancelAckReceiverReceived(LTPSpan* span, LTPSegment* receivedSegment);

/***** SENDER *****/

/**
 * \par Function Name:
 *      signalSessionNewReportSegmentReceived
 *
 * \brief  Called when a new RS is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				signalSessionReportSegmentReceived(LTPSpan* span, LTPSegment* receivedSegment);
/**
 * \par Function Name:
 *      signalSessionNewCancelAckSegmentReceived
 *
 * \brief  Called when a new CAS is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				signalSessionCancelAckSegmentReceived(LTPSpan* span, LTPSegment* receivedSegment);
/**
 * \par Function Name:
 *      signalSessionNewCancelReceiverSegmentReceived
 *
 * \brief  Called when a new CR is received
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  span 			The span
 * \param  receivedSegment  The segment received
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 				signalSessionCancelReceiverSegmentReceived(LTPSpan* span, LTPSegment* receivedSegment);

/**
 * \par Function Name:
 *      closeSession
 *
 * \brief  Remove session from the Active Session and put session in the Recently Closed Session
 *
 *
 * \par Date Written:
 *      10/04/21
 *
 * \return void
 *
 * \param  session			The pointer to the session
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  10/04/21 | D. Filoni       |  Initial Implementation and documentation.
 *****************************************************************************/
void 		closeSession(LTPSession* session);


/**
 * \par Function Name:
 *      generateCR
 *
 * \brief  Generete CR segment, change session state to CR_SENT and clean session
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  session			The pointer to the session
 * \param  cancelCode		The cancel code to insert into the segment
 * \param  ipAddress	    The recipient IP address
 * \param  portNumber		The recipient port number
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void generateCR(LTPSession* session, LTPCancelCode cancelCode, unsigned int ipAddress, unsigned short portNumber);


/**
 * \par Function Name:
 *      generateCS
 *
 * \brief  Generete CS segment, change session state to CS_SENT and clean session
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  session			The pointer to the session
 * \param  cancelCode		The cancel code to insert into the segment
 * \param  ipAddress	    The recipient IP address
 * \param  portNumber		The recipient port number
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void generateCS(LTPSession* session, LTPCancelCode cancelCode, unsigned int ipAddress, unsigned short portNumber);

/**
 * \par Function Name:
 *      findReportSegmentFromReportSegmentSerialNumber
 *
 * \brief  Find the report segment from the report segment serial number
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return int
 *
 * \retval  The report segment
 *
 * \param  _reportSerialNumber			The report segment serial number
 * \param  _reportSerialNumberSize		The size of the report segment serial number
 * \param  _reportSegment	    		The pointer of the report segment
 * \param  _reportSegmentSize			The size of the report segment
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
int findReportSegmentFromReportSegmentSerialNumber(void* _reportSerialNumber, size_t _reportSerialNumberSize, void* _reportSegment, size_t _reportSegmentSize);
#endif /* SRC_LTPSESSION_SESSION_H_ */
