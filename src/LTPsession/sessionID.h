/** \file sessionID.h
 *
 * \brief This file contains the defines the LTPSessionID structure
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_LTPSESSION_SESSIONID_H_
#define SRC_LTPSESSION_SESSIONID_H_

/*!
 * \brief Session types
 */
typedef enum {
	RX_SESSION, TX_SESSION
} SessionType;

/*!
 * \brief Session identifier
 */
typedef struct {
	/**
	 * \brief Session originator (based on RFC)
	 */
	unsigned long long			sessionOriginator;
	/**
	 * \brief Session number (based on RFC)
	 */
	unsigned int				sessionNumber;
} LTPSessionID;

#endif /* SRC_LTPSESSION_SESSIONID_H_ */
