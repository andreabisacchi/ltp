/** \file ECLSA_lower_protocol.c
 *
 * \brief This file contains one implementation of the lower protocol which uses ECLSA
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/eventfd.h>
#include <sys/select.h>
#include <ctype.h>

#include "lowerProtocolAdapter.h"
#include "../lowerProtocol.h"
#include "../../../config.h"
#include "../../../list/list.h"
#include "../../../generic/generic.h"

static bool _ECLSAinitLowerLevel(char* initString);
static bool _ECLSAinitOutLowerLevel(unsigned int ipAddress, unsigned short portNumber, char* initString);
static void _ECLSAdisposeLowerProtocol();
static bool _ECLSAreceivePacketFromLowerProtocol(char* buffer, int *bufferLength);
static bool _ECLSAsendPacketToLowerProtocol(char* buffer, int bufferLength, unsigned int ipAddress, unsigned short portNumber);
static int  _ECLSAsetFDForSelect (fd_set* fdstruct);
static bool _ECLSAreceivedFromFDForSelect (fd_set* fdstruct);
static int  _findEclsoFDFromID (void* _ID, size_t IDSize, void* _eclsoFD, size_t eclsoFDSize);

/**
 * \brief ECLSI program
 *
 * \hideinitializer
 */
#define ECLSI_PROGRAM "eclsi"

/**
 * \brief ECLSO program
 *
 * \hideinitializer
 */
#define ECLSO_PROGRAM "eclso"

static int 		fdReadFromEclsi;
static List 	fdListToEclso = empty_list;

/**
 * \brief The ID used to get which ECLSO must be used
 */
typedef struct {
	/**
	 * \brief The IP address
	 */
	unsigned int 	ipAddress;
	/**
	 * \brief The port
	 */
	unsigned short 	port;
} EclsoFdID;

/**
 * \brief ECLSO ID and file descriptor (pipe) used to communicate with it
 */
typedef struct {
	/**
	 * \brief The ECLSO ID
	 */
	EclsoFdID		ID;
	/**
	 * \brief The file descriptor (pipe)
	 */
	int 			fd;
} EclsoFD;

LowerProtocolAdapter getECLSALowerProtocolAdapter() {
	LowerProtocolAdapter result;
	result.protocolName 			= "ECLSA";
	result.init 					= _ECLSAinitLowerLevel;
	result.initOut 					= _ECLSAinitOutLowerLevel;

	result.dispose 					= _ECLSAdisposeLowerProtocol;
	result.send 					= _ECLSAsendPacketToLowerProtocol;
	result.receive		 			= _ECLSAreceivePacketFromLowerProtocol;

	result.setFDForSelect 			= _ECLSAsetFDForSelect;
	result.receivedFromFDForSelect	= _ECLSAreceivedFromFDForSelect;
	return result;
}

/***** PRIVATE FUNCTIONS *****/

static bool _ECLSAinitLowerLevel(char* initString) {
	int fd[2];
	if ( pipe(fd) != 0 ) {
		fprintf(stderr, "Error on opening pipe for ECLSI\n");
		return false;
	}

	fdReadFromEclsi = fd[0];

	int childPID = fork();

	if ( childPID < 0 ) {
		fprintf(stderr, "Error on fork\n");
		return false;
	} else if ( childPID == 0 ) { // child

		dup2(fd[1], 3); // Move the pipe to the fd 3
		for (int i = 4; i <= 20; i++) { // Close other files
			if ( i != fd[1] ) close(i);
		}

		int count;
		char** argvString = str_split(initString, ' ', &count);
		char** argv = malloc(sizeof(char*) * (count + 2));

		argv[0] = ECLSI_PROGRAM;
		for(int i = 0; i < count; i++) {
			argv[i+1] = malloc(sizeof(char) * (strlen(argvString[i]) + 1));
			strcpy(argv[i+1], argvString[i]);
		}

		argv[count+1] = NULL;

		execvp(ECLSI_PROGRAM, argv);
		fprintf(stderr, "Failure on opening ECLSI\n");
		exit(0);

	} else { // Father
		close(fd[1]);
	}

	return true;
}

static bool _ECLSAinitOutLowerLevel(unsigned int ipAddress, unsigned short portNumber, char* initString) {
	EclsoFD eclsoFD;

	int fd[2];
	if ( pipe(fd) != 0 ) {
		fprintf(stderr, "Error on opening pipe to ECLSO\n");
		return false;
	}

	int childPID = fork();

	if ( childPID < 0 ) {
		fprintf(stderr, "Error on fork\n");
		return false;
	} else if ( childPID == 0 ) { // child

		dup2(fd[0], 3); // Move the pipe to the fd 3
		for (int i = 4; i <= 20; i++) { // Close other files
			if ( i != fd[0] ) close(i);
		}
		
		int count;
		char** argvString = str_split(initString, ' ', &count);
		char** argv = malloc(sizeof(char*) * (count + 3));

		argv[0] = ECLSO_PROGRAM;
		for(int i = 0; i < count; i++) {
			argv[i+1] = malloc(sizeof(char) * (strlen(argvString[i]) + 1));
			strcpy(argv[i+1], argvString[i]);
		}

		argv[count+1] = "_useless_param_";
		argv[count+2] = NULL;
		
		execvp(ECLSO_PROGRAM, argv);
		fprintf(stderr, "Failure on opening ECLSO\n");
		exit(0);

	} else { // Father
		close(fd[0]);
	}

	eclsoFD.ID.ipAddress = ipAddress;
	eclsoFD.ID.port = portNumber;
	eclsoFD.fd = fd[1];

	unsigned long long myNodeNumber = getMyNodeNumber();
	write(eclsoFD.fd, &myNodeNumber, sizeof(unsigned long long));

	list_append(&fdListToEclso, &eclsoFD, sizeof(eclsoFD));

	return true;
}

static void _ECLSAdisposeLowerProtocol() {
	// TODO chiudere tutti i processi aperti (ECLSI e tutti gli ECLSO)
}

static bool _ECLSAreceivePacketFromLowerProtocol(char* buffer, int *bufferLength) {
	// Read the segment size
	int result = read(fdReadFromEclsi, bufferLength, sizeof(int));
	if ( result < 0 ) {
		fprintf(stderr, "Error, pipe to ECLSI is broken\n");
		exit(-1);
		return false;
	} else if ( result != sizeof(int) ) {
		return false;
	}

	// Read the segment
	result = read(fdReadFromEclsi, buffer, *bufferLength);
	if ( result < 0 ) {
		fprintf(stderr, "Error, pipe to ECLSI is broken\n");
		exit(-1);
		return false;
	} else if ( result != *bufferLength ) {
		return false;
	}

	return true;
}

static bool _ECLSAsendPacketToLowerProtocol(char* buffer, int bufferLength, unsigned int ipAddress, unsigned short portNumber) {
	EclsoFdID ID;
	ID.ipAddress = ipAddress;
	ID.port = portNumber;

	EclsoFD* eclsoFD = (EclsoFD*) list_get_pointer_data(fdListToEclso, &ID, sizeof(ID), _findEclsoFDFromID);

	if ( eclsoFD == NULL ) {
		fprintf(stderr, "Error, ECLSO not found\n");
		exit(-1);
		return false;
	}

	// Write the segment size
	int result = write(eclsoFD->fd, &bufferLength, sizeof(int));
	if ( result < 0 ) {
		fprintf(stderr, "Error, pipe to ECLSI is broken\n");
		exit(-1);
		return false;
	} else if ( result != sizeof(int) ) {
		return false;
	}

	// Read the segment
	result = write(eclsoFD->fd, buffer, bufferLength);
	if ( result < 0 ) {
		fprintf(stderr, "Error, pipe to ECLSI is broken\n");
		exit(-1);
		return false;
	} else if ( result != bufferLength ) {
		return false;
	}

	return true;
}

static int _ECLSAsetFDForSelect (fd_set* fdstruct) {
	FD_SET(fdReadFromEclsi, fdstruct);
	return fdReadFromEclsi;
}

static bool _ECLSAreceivedFromFDForSelect (fd_set* fdstruct) {
	return FD_ISSET(fdReadFromEclsi, fdstruct);
}

static int _findEclsoFDFromID (void* _ID, size_t IDSize, void* _eclsoFD, size_t eclsoFDSize) {
	EclsoFdID* eclsoFdID = (EclsoFdID*) _ID;
	EclsoFD*   eclsoFD 	 = (EclsoFD*) 	_eclsoFD;
	if ( eclsoFdID->ipAddress == eclsoFD->ID.ipAddress && eclsoFdID->port == eclsoFD->ID.port )
		return 0;
	else
		return -1; // Not equals
}

