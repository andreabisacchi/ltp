/** \file UDP_lower_protocol.c
 *
 * \brief This file contains one implementation of the lower protocol which uses UDP
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <netinet/ip_icmp.h>
#include <netinet/udp.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/eventfd.h>
#include <sys/select.h>

#include "lowerProtocolAdapter.h"
#include "../lowerProtocol.h"
#include "../../../config.h"
#include "../../../generic/generic.h"

static bool _UDPinitLowerLevel(char* initString);
static bool _UDPinitOutLowerLevel(unsigned int ipAddress, unsigned short portNumber, char* initString);
static void _UDPdisposeLowerProtocol();
static bool _UDPreceivePacketFromLowerProtocol(char* buffer, int *bufferLength);
static bool _UDPsendPacketToLowerProtocol(char* buffer, int bufferLength, unsigned int ipAddress, unsigned short portNumber);
static int _UDPsetFDForSelect (fd_set* fdstruct);
static bool _UDPreceivedFromFDForSelect (fd_set* fdstruct);


static struct {
	int 				socketDescriptor;
	struct sockaddr		socketName;
	struct sockaddr_in	*inetName;
} udpEnv;

LowerProtocolAdapter getUDPLowerProtocolAdapter() {
	LowerProtocolAdapter result;
	result.protocolName 			= "UDP";
	result.init 					= _UDPinitLowerLevel;
	result.initOut 					= _UDPinitOutLowerLevel;

	result.dispose 					= _UDPdisposeLowerProtocol;
	result.send 					= _UDPsendPacketToLowerProtocol;
	result.receive		 			= _UDPreceivePacketFromLowerProtocol;

	result.setFDForSelect 			= _UDPsetFDForSelect;
	result.receivedFromFDForSelect	= _UDPreceivedFromFDForSelect;
	return result;
}

/***** PRIVATE FUNCTIONS *****/

static bool _UDPinitLowerLevel(char* initString) {
	socklen_t nameLength = sizeof(struct sockaddr);


	int count;
	char** ipPort = str_split(initString, ':', &count);
	char* ip = ipPort[0];
	char* port = (count == 3 ? ipPort[1] : LISTEN_UDP_DEFAULT_PORT);
	_free(ipPort);

	const unsigned short portNumber = htons(atoi(port));
	unsigned int ipAddress;

	if ( strcmp(ip, "0.0.0.0") == 0 ) {
		ipAddress = INADDR_ANY;
	} else {
		struct hostent *he = gethostbyname(ip);
		if (he == NULL) {
			fprintf(stderr, "%s not resolved\n", ip);
			exit(-1);
		}
		memcpy(&ipAddress, he->h_addr_list[0], he->h_length);
	}

	_free(ip);
	if (count == 3)
		_free(port);

	/*const unsigned short portNumber = htons(LISTEN_UDP_DEFAULT_PORT);
	const unsigned int ipAddress = INADDR_ANY;*/

	//ipAddress =  htonl(ipAddress);

	memset((char *) &udpEnv.socketName, 0, sizeof(udpEnv.socketName));
	udpEnv.inetName = (struct sockaddr_in *) &udpEnv.socketName;
	udpEnv.inetName->sin_family = AF_INET; // IPv4
	udpEnv.inetName->sin_port = portNumber;
	memcpy((char *) &(udpEnv.inetName->sin_addr.s_addr), (char *) &ipAddress, 4);

	udpEnv.socketDescriptor = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if ( udpEnv.socketDescriptor < 0 )
	{
		fprintf(stderr, "Lower protocol can't open UDP socket\n");
		return false;
	}

	if ( bind(udpEnv.socketDescriptor, &udpEnv.socketName, nameLength) < 0
		|| getsockname(udpEnv.socketDescriptor, &udpEnv.socketName, &nameLength) < 0 )
	{
		close(udpEnv.socketDescriptor);
		fprintf(stderr, "Can't initialize UDP socket\n");
		return false;
	}

	struct timeval timeout = {0, 1000 * 500};
	if ( setsockopt(udpEnv.socketDescriptor, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0 ) {
		fprintf(stderr, "setsockopt failed on UDP socket\n");
		return false;
	}

	return true;
}

static bool _UDPinitOutLowerLevel(unsigned int ipAddress, unsigned short portNumber, char* initString) {
	// Nothing to do
	return true;
}

static void _UDPdisposeLowerProtocol() {
	close(udpEnv.socketDescriptor);
}

static bool _UDPreceivePacketFromLowerProtocol(char* buffer, int *bufferLength) {
	struct sockaddr_in	fromAddr;
	socklen_t			fromSize = sizeof(struct sockaddr_in);

	if ( bufferLength == NULL ) {
		fprintf(stderr, "bufferLength is NULL\n");
		return false;
	}

	*bufferLength = recvfrom(udpEnv.socketDescriptor, buffer, LOWER_LEVEL_MAX_PACKET_SIZE,	0, (struct sockaddr *) &fromAddr, &fromSize);

	if ( *bufferLength <= 0 ) { // If socket timeouted
		return false;
	}

	return true;
}

static bool _UDPsendPacketToLowerProtocol(char* buffer, int bufferLength, unsigned int ipAddress, unsigned short portNumber) {
	int fd;
	struct sockaddr_in servaddr;

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if ( fd < 0 ) {
		fprintf(stderr, "Cannot open UDP socket for sending\n");
		return false;
	}

	bzero(&servaddr,sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = ipAddress;
	servaddr.sin_port = htons(portNumber);

	if ( sendto(fd, buffer, bufferLength, 0, (struct sockaddr*)&servaddr, sizeof(servaddr)) < 0 ) {
		fprintf(stderr, "Cannot send UDP datagram\n");
		close(fd);
		return false;
	}

	close(fd);
	return true;
}

static int _UDPsetFDForSelect (fd_set* fdstruct) {
	FD_SET(udpEnv.socketDescriptor, fdstruct);
	return udpEnv.socketDescriptor;
}

static bool _UDPreceivedFromFDForSelect (fd_set* fdstruct) {
	return FD_ISSET(udpEnv.socketDescriptor, fdstruct);
}
