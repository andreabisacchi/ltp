/** \file segment.h
 *
 * \brief This file contains the defines of the functions and the structs of an LTPsegment
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_LTPSEGMENT_SEGMENT_H_
#define SRC_LTPSEGMENT_SEGMENT_H_

#include <stdbool.h>

#include "../LTPsession/sessionID.h"

/**
 * \brief The LTP version
 *
 * \hideinitializer
 */
#define LTP_VERSION 0

/*!
 * \brief Possible LTP Segment type codes according to RFC 5326
 */
typedef enum {
	LTPDSRed 				= 0,		/* 	Red data segment 							*/
	LTPDSRedCheckpoint 		= 1,		/* 	Red data segment + checkpoint 				*/
	LTPDSRedEORP 			= 2,		/* 	Red data segment + checkpoint + EORP 		*/
	LTPDSRedEOB 			= 3,		/* 	Red data segment + checkpoint + EORP + EOB 	*/

	LTPDSGreen 				= 4,		/* 	Green data segment 							*/
	LTPDSGreenEOB 			= 7,		/* 	Green data segment + EOB 					*/

	LTPDSOrange				= 5,		/*	Orange data segment							*/
	LTPDSOrangeEOB			= 6,		/*	Orange data segment + CP + EOB				*/

	LTPRS 					= 8,		/* 	Report segment.								*/
	LTPRA 					= 9,		/* 	Report acknowledgment.						*/

	LTPCS 					= 12,		/* 	Cancel by source of block.					*/
	LTPCAS 					= 13,		/* 	CS acknowledgment.							*/

	LTPCR 					= 14,		/* 	Cancel by block receiver (destination).		*/
	LTPCAR 					= 15		/* 	CR acknowledgment.							*/
} LTPSegmentTypeCode;

/*!
 * \brief LTP Cancel code reasons accordind to RFC
 */
typedef enum {
	USR_CNCLD					= 0,		/* 	Client service canceled session 			*/
	UNREACH						= 1,		/* 	Unreachable client service					*/
	RLEXC						= 2,		/* 	Retransmission limit exceeded				*/
	MISCOLORED					= 3,		/* 	Received red data BEFORE green data			*/
	SYS_CNCLD					= 4,		/* 	System error 								*/
	RXMTCYCEXC					= 5,		/* 	Retransmission-Cycles limit 				*/
	INCOMPLETE_ORANGE_BLOCK		= 6,		/* 	Recived incomplete orange block				*/
	SESSION_TIMEOUT				= 7			/*  Session timeout expired						*/
} LTPCancelCode;

/*!
 * \brief LTP Segment header
 */
typedef struct {
	/**
	 * \brief The type of segment header (according to RFC)
	 */
	LTPSegmentTypeCode							typeFlag;
	/**
	 * \brief The number of header extensions
	 */
	unsigned char 								headerExtensionsCount;
	/**
	 * \brief The number of trailer extensions
	 */
	unsigned char 								trailerExtensionsCount;

	/**
	 * \brief The session ID
	 */
	LTPSessionID								sessionID;

} LTPSegmentHeader;

/*!
 * \brief LTP Data segment
 */
typedef struct {
	/**
	 * \brief The client service ID
	 */
	unsigned int 								clientServiceID;
	/**
	 * \brief The offset of the data
	 */
	unsigned int 								offset;
	/**
	 * \brief The length of data present in this segment
	 */
	unsigned int								length;

	/**
	 * \brief The checkpoint serial number. If the segment is not a CP this values is 0
	 */
	unsigned int 								checkpointSerialNumber;
	/**
	 * \brief The report serial number. If the segment is not a CP this values is 0
	 */
	unsigned int								reportSerialNumber;

	/**
	 * \brief The paylaod data
	 */
	void*										data;
} LTPDataSegment;

/*!
 * \brief  Report Segment Claim
 */
typedef struct {
	/**
	 * \brief The claim offset
	 */
	unsigned int 								offset;
	/**
	 * \brief The claim length
	 */
	unsigned int								length;
} LTPReportSegmentClaim;

/*!
 * \brief  Report Segment specific fields
 */
typedef struct {
	/**
	 * \brief The report serial number
	 */
	unsigned int								reportSerialNumber;
	/**
	 * \brief The checkpoint serial number
	 */
	unsigned int								checkpointSerialNumber;
	/**
	 * \brief The RS upper bound
	 */
	unsigned int								upperBound;
	/**
	 * \brief The RS lower bound
	 */
	unsigned int								lowerBound;

	/**
	 * \brief The number of claims
	 */
	unsigned int								receptionClaimCount;
	/**
	 * \brief The array of claims
	 */
	LTPReportSegmentClaim*						receptionClaims;
} LTPReportSegment;

/*!
 * \brief Report Ack specific fields
 */
typedef struct {
	/**
	 * \brief The report serial number of the RS which is confirmed by this RA
	 */
	unsigned int 								reportSerialNumber;
} LTPReportAckSegment;

/*!
 * \brief Cancel segment specific fields
 */
typedef struct {
	/**
	 * \brief The code which tells the why of the cancel
	 */
	LTPCancelCode 								cancelCode;
} LTPCancelSegment;

/*!
 * \brief LTP Segment
 */
typedef struct {
	/**
	 * \brief The LTP segment header
	 */
	LTPSegmentHeader		 					header;

	/**
	 * \brief Adds the additional values (based on the type of segment)
	 */
	union {
		/**
		 * \brief Only if segment is a data segment
		 */
		LTPDataSegment 					dataSegment;
		/**
		 * \brief Only if the segment is a RS
		 */
		LTPReportSegment 				reportSegment;
		/**
		 * \brief Only if the segment is a RA
		 */
		LTPReportAckSegment				reportAckSegment;
		/**
		 * \brief Only if the segment is a CS or CR
		 */
		LTPCancelSegment				cancelSegment;
	};
} LTPSegment;


/**
 * \par Function Name:
 *      parsePacketIntoSegment
 *
 * \brief  Transforms a buffer into a segment
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return int
 *
 * \retval  1 = success. 0 if fails. Negative if an error occurred
 *
 * \param  cursor 			The data to parse
 * \param  bytesRemaining	The amount of data to parse
 * \param  result			The LTP segment which will be filled
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
int 			parsePacketIntoSegment (char *cursor, int bytesRemaining, LTPSegment *result);

/**
 * \par Function Name:
 *      putSegmentInBuffer
 *
 * \brief  Inserts a segment into a buffer
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return int
 *
 * \retval  Number of bytes inserted
 *
 * \param  segment 		The segment to inset into the buffer
 * \param  buffer		The buffer to fill
 * \param  bufferSize	The buffer size
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
unsigned int	putSegmentInBuffer(LTPSegment* segment, char* buffer, unsigned int bufferSize);

/**
 * \par Function Name:
 *      destroySegment
 *
 * \brief  Destroys a segment
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  segment 		The segment to destroy
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 			destroySegment(LTPSegment *segment);



/**
 * \brief Tells if the segment is an EOB segment
 *
 * \hideinitializer
 */
#define 		isSegmentEOB(segment)				( (segment).header.typeFlag == LTPDSRedEOB || (segment).header.typeFlag == LTPDSGreenEOB || (segment).header.typeFlag == LTPDSOrangeEOB )

/**
 * \brief Tells if the segment is a CP segment
 *
 * \hideinitializer
 */
#define 		isSegmentCheckpoint(segment) 		( (segment).header.typeFlag==LTPDSRedEORP || (segment).header.typeFlag==LTPDSRedCheckpoint || (segment).header.typeFlag==LTPDSRedEOB )

/**
 * \brief Tells if the segment is a red data segment
 *
 * \hideinitializer
 */
#define 		isRedDataSegment(segment) 			( (segment).header.typeFlag == LTPDSRed || (segment).header.typeFlag == LTPDSRedCheckpoint || (segment).header.typeFlag == LTPDSRedEOB || (segment).header.typeFlag == LTPDSRedEORP )

/**
 * \brief Tells if the segment is a RS segment
 *
 * \hideinitializer
 */
#define			isReportSegment(segment)			( (segment).header.typeFlag == LTPRS )

/**
 * \brief Tells if the segment is a RA segment
 *
 * \hideinitializer
 */
#define 		isReportAckSegment(segment)			( (segment).header.typeFlag == LTPRA )

/**
 * \brief Tells if the segment is a CS segment
 *
 * \hideinitializer
 */
#define	 		isCancelSegment(segment)			( (segment).header.typeFlag == LTPCS )

/**
 * \brief Tells if the segment is a CAS segment
 *
 * \hideinitializer
 */
#define 		isCancelAckSegment(segment)			( (segment).header.typeFlag == LTPCAS )

/**
 * \brief Tells if the segment is a CR segment
 *
 * \hideinitializer
 */
#define 		isCancelReceiverSegment(segment)	( (segment).header.typeFlag == LTPCR )

/**
 * \brief Tells if the segment is a CAR segment
 *
 * \hideinitializer
 */
#define 		isCancelReceiverAckSegment(segment)	( (segment).header.typeFlag == LTPCAR )

/**
 * \brief Tells if the segment is a green data segment
 *
 * \hideinitializer
 */
#define 		isGreenDataSegment(segment)			( (segment).header.typeFlag == LTPDSGreen || (segment).header.typeFlag == LTPDSGreenEOB )

/**
 * \brief Tells if the segment is a green EOB data segment
 *
 * \hideinitializer
 */
#define 		isGreenEOBDataSegment(segment)			( (segment).header.typeFlag == LTPDSGreenEOB )

/**
 * \brief Tells if the segment is an orange data segment
 *
 * \hideinitializer
 */
#define 		isOrangeDataSegment(segment)			( (segment).header.typeFlag == LTPDSOrange || (segment).header.typeFlag == LTPDSOrangeEOB )

/**
 * \brief Tells if the segment is an orange EOB data segment
 *
 * \hideinitializer
 */
#define 		isOrangeEOBDataSegment(segment)			( (segment).header.typeFlag == LTPDSOrangeEOB )

/**
 * \brief Tells if the segment is a data segment (green or red)
 *
 * \hideinitializer
 */
#define			isDataSegment(segment)				( isRedDataSegment(segment) || isGreenDataSegment(segment) )

/**
 * \brief Tells if the segment is a signal segment
 *
 * \hideinitializer
 */
#define 		isSignalSegment(segment)			( isSegmentCheckpoint(segment) || isReportSegment(segment) || isReportAckSegment(segment) || isCancelSegment(segment) || isCancelAckSegment(segment) || isCancelReceiverSegment(segment) || isCancelReceiverAckSegment(segment) || isGreenEOBDataSegment(segment) || isOrangeEOBDataSegment(segment) )

/**
 * \brief Tells if the segment belongs to a RX session
 *
 * \hideinitializer
 */
#define 		isRXSession(segment)			( isRedDataSegment(segment) || isReportAckSegment(segment) || isCancelSegment(segment)|| isCancelReceiverAckSegment(segment) ||	isGreenDataSegment(segment) || isOrangeDataSegment(segment) )

/**
 * \brief Tells if the segment belongs to a TX session
 *
 * \hideinitializer
 */
#define 		isTXSession(segment)			( isReportSegment(segment) || isCancelAckSegment(segment) || isCancelReceiverSegment(segment) )

/**
 * \brief Tells if the segment belongs to a TX session
 *
 * \hideinitializer
 */
#define 		isCXSegment(segment)			( isCancelReceiverSegment(segment)|| isCancelSegment(segment) )

#endif /* SRC_LTPSEGMENT_SEGMENT_H_ */
