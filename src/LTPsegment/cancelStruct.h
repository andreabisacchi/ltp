/** \file cancelStruct.h
 *
 * \brief This file contains the defines of the functions and the structs of a Cancel Struct
 * 		  that contains the information needed to generrate a CX
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Davide Filoni, davide.filoni2@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_LTPSEGMENT_CANCELSTRUCT_H_
#define SRC_LTPSEGMENT_CANCELSTRUCT_H_

#include "segment.h"
#include "../LTPsession/sessionStruct.h"

/*!
* \brief Possible cancel Types
*/
typedef enum {
	CS 					= 12,		/* 	Cancel by source of block.					*/
	CR 					= 14,		/* 	Cancel by block receiver (destination).		*/
} LTPCancelType;

/*!
* \brief The cancel struct
*/
typedef struct {
	/**
	* \brief The type of cancel segment (according to RFC)
	*/
		LTPCancelType							typeFlag;

	/**
	* \brief The code which tells the why of the cancel
	*/
		LTPCancelCode 								cancelCode;
	/**
	* \brief The session of the segment to generate
	*/
		LTPSession*			session;
} CancelStruct;


/**
 * \par Function Name:
 *      createCancelStruct
 *
 * \brief Creates the cancel struct
 *
 *
 * \par Date Written:
 *      10/04/21
 *
 * \return cancelStruct
 *
 * \retval The new cancel struct created
 *
 * \param  typeFlag 		The type of cancel segment to generate
 * \param  cancelCode		The cancel code of the segment to generate
 * \param  session			The pointer of the session
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
CancelStruct		createCancelStruct(LTPCancelType typeFlag, LTPCancelCode cancelCode, LTPSession* session);



#endif /* SRC_LTPSEGMENT_CANCELSTRUCT_H_ */
