/** \file segment.c
 *
 * \brief This file contains the implementations of the LTP segment functions
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include "segment.h"

#include "../sdnv/sdnv.h"

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "../generic/generic.h"

/***** PRIVATE FUNCTIONS *****/

static bool _putGreenSegment (LTPSegment *segment, char **cursor, int *bytesRemaining);
static bool _parseGreenSegment (char **cursor, int *bytesRemaining, LTPSegment *result);

static bool _putOrangeSegment (LTPSegment *segment, char **cursor, int *bytesRemaining);
static bool _parseOrangeSegment (char **cursor, int *bytesRemaining, LTPSegment *result);

static bool _putRedSegment (LTPSegment *segment, char **cursor, int *bytesRemaining);
static bool _parseRedSegment (char **cursor, int *bytesRemaining, LTPSegment *result);

static bool _putReportSegment (LTPSegment *segment, char **cursor, int *bytesRemaining);
static bool _parseReportSegment (char **cursor, int *bytesRemaining, LTPSegment *result);

static bool _putReportAckSegment (LTPSegment *segment, char **cursor, int *bytesRemaining);
static bool _parseReportAckSegment (char **cursor, int *bytesRemaining, LTPSegment *result);

static bool _putCancelSegment (LTPSegment *segment, char **cursor, int *bytesRemaining);
static bool _parseCancelSegment (char **cursor, int *bytesRemaining, LTPSegment *result);

void destroySegment(LTPSegment* segment) {
	if ( isRedDataSegment(*segment) || isGreenDataSegment(*segment) ) {
		if ( segment->dataSegment.data != NULL ) _free(segment->dataSegment.data);
	} else if ( isReportSegment(*segment) ) {
		if ( segment->reportSegment.receptionClaims != NULL ) _free(segment->reportSegment.receptionClaims);
	} else if ( isReportSegment(*segment) ) {
		if ( segment->reportSegment.receptionClaims != NULL ) _free(segment->reportSegment.receptionClaims);
	}
	// ReportAckSegment 		-> NOTHING to free
	// Cancel segment 			-> NOTHING to free
	// Cancel segment ACK		-> NOTHING to free
	// Cancel receiver			-> NOTHING to free
	// Cancel receiver ACK 		-> NOTHING to free

	memset(segment, 0, sizeof(LTPSegment)); // Sets all to 0, and so also pointers to NULL
}

int parsePacketIntoSegment (char *cursor, int bytesRemaining, LTPSegment *result) {
	if (cursor == NULL || bytesRemaining <= 0)
		return -1; // Skip

	{
		unsigned char version = ((*cursor) >> 4) & 0x0F;
		if (version != LTP_VERSION) {
			return 0; // Skip
		}
	}

	memset(result, sizeof(LTPSegment), 0);

	// Version + type flag
	result->header.typeFlag = (*cursor) & 0x0F;
	cursor++; bytesRemaining--;

	// Session ID
	extractSdnv(&(result->header.sessionID.sessionOriginator), (unsigned char **) &cursor, &bytesRemaining);
	extractSmallSdnv(&(result->header.sessionID.sessionNumber), (unsigned char **) &cursor, &bytesRemaining);

	// Header & trailer extension count
	result->header.headerExtensionsCount = (*cursor >> 4) & 0x0f;
	result->header.trailerExtensionsCount = *cursor & 0x0f;
	cursor++; bytesRemaining--;

	// Header extensions
	for (int i = 0; i < result->header.headerExtensionsCount; i++) {
		// TODO: Parse a single header extension
		return 0; // It cannot handle it
	}

	// Parse specific fields
	bool parsed;
	if ( isRedDataSegment(*result) ) {
		parsed = _parseRedSegment(&cursor, &bytesRemaining, result);

	} else if ( isReportSegment(*result) ) {
		parsed = _parseReportSegment(&cursor, &bytesRemaining, result);

	} else if ( isReportAckSegment(*result) ) {
		parsed = _parseReportAckSegment(&cursor, &bytesRemaining, result);

	} else if ( isCancelSegment(*result) || isCancelReceiverSegment(*result) ) {
		parsed = _parseCancelSegment(&cursor, &bytesRemaining, result);

	} else if ( isCancelAckSegment(*result) || isCancelReceiverAckSegment(*result) ) {
		// Nothing to do, no spiecific fields
		parsed = true;
	} else if ( isGreenDataSegment(*result) ) {
		parsed = _parseGreenSegment(&cursor, &bytesRemaining, result);

	} else if ( isOrangeDataSegment(*result) ) {
		parsed = _parseOrangeSegment(&cursor, &bytesRemaining, result);

	} else {
		parsed = false;
		fprintf(stderr, "Arrived segment with not recognized code %d\n", result->header.typeFlag);
		return 0;
	}
	if ( !parsed ) {
		destroySegment(result);
		return 0; // Skip
	}

	// Trailer extensions
	for (int i = 0; i < result->header.trailerExtensionsCount; i++) {
		// TODO: Parse a single trailer extension
		return 0; // It canot handle it
	}

	return 1;
}

unsigned int putSegmentInBuffer(LTPSegment* segment, char* buffer, unsigned int bufferSize) {
	char* cursor = buffer;
	unsigned int bytesRemaining = bufferSize;

	// Version + type flag
	*cursor = LTP_VERSION;
	*cursor <<= 4;
	*cursor += segment->header.typeFlag;
	cursor++; bytesRemaining--;

	// Session ID
	insertSdnv(segment->header.sessionID.sessionOriginator, &cursor, &bytesRemaining);
	insertSdnv(segment->header.sessionID.sessionNumber, &cursor, &bytesRemaining);

	// Header & trailer extension count
	*cursor = segment->header.headerExtensionsCount;
	*cursor <<= 4;
	*cursor += segment->header.trailerExtensionsCount;
	cursor++; bytesRemaining--;

	// Header extensions
	for(int i = 0; i < segment->header.headerExtensionsCount; i++) {
		// TODO: insert a single header extension
		return 0; // Can't handle it
	}

	// Specific fields
	bool put;
	if ( isRedDataSegment(*segment) ) {
		put = _putRedSegment(segment, &cursor, &bytesRemaining);

	} else if ( isReportSegment(*segment) ) {
		put = _putReportSegment(segment, &cursor, &bytesRemaining);

	} else if ( isReportAckSegment(*segment) ) {
		put = _putReportAckSegment(segment, &cursor, &bytesRemaining);

	} else if ( isCancelSegment(*segment) || isCancelReceiverSegment(*segment) ) {
		put = _putCancelSegment(segment, &cursor, &bytesRemaining);

	} else if ( isCancelAckSegment(*segment) || isCancelReceiverAckSegment(*segment) ) {
		// Nothing to do, no spiecific fields
		put = true;

	} else if ( isGreenDataSegment(*segment) ) {
		put = _putGreenSegment(segment, &cursor, &bytesRemaining);

	} else if ( isOrangeDataSegment(*segment) ) {
		put = _putOrangeSegment(segment, &cursor, &bytesRemaining);

	} else {
		put = false;
		fprintf(stderr, "Segment with not recognized code %d\n", segment->header.typeFlag);
		return 0;
	}

	if ( !put ) {
		return 0; // Error occurred -> Skip
	}

	// Trailer extensions
	for(int i = 0; i < segment->header.trailerExtensionsCount; i++) {
		// TODO: insert a single trailer  extension
		return 0; // It cannot handle it
	}

	return bufferSize - bytesRemaining;
}

/***** PRIVATE FUNCTIONS *****/

static bool _putGreenSegment(LTPSegment *segment, char **cursor, int *bytesRemaining) {
	insertSdnv(segment->dataSegment.clientServiceID, cursor, bytesRemaining);
	insertSdnv(segment->dataSegment.offset, cursor, bytesRemaining);
	insertSdnv(segment->dataSegment.length, cursor, bytesRemaining);

	memcpy(*cursor, segment->dataSegment.data, segment->dataSegment.length);
	(*cursor) += segment->dataSegment.length;
	*bytesRemaining -= segment->dataSegment.length;

	return true;
}

static bool _parseGreenSegment (char **cursor, int *bytesRemaining, LTPSegment *result) {
	if (!extractSmallSdnv(&(result->dataSegment.clientServiceID), (unsigned char **) cursor, bytesRemaining)) 				return false;
	if (!extractSmallSdnv(&(result->dataSegment.offset), (unsigned char **) cursor, bytesRemaining)) 						return false;
	if (!extractSmallSdnv(&(result->dataSegment.length), (unsigned char **) cursor, bytesRemaining)) 						return false;

	result->dataSegment.data = (char*) _malloc( result->dataSegment.length );
	if (result->dataSegment.data == NULL)																					return false;

	memcpy(result->dataSegment.data, *cursor, result->dataSegment.length);
	(*cursor) += result->dataSegment.length;
	*bytesRemaining -= result->dataSegment.length;

	return true;
}

static bool _putOrangeSegment(LTPSegment *segment, char **cursor, int *bytesRemaining) {
	insertSdnv(segment->dataSegment.clientServiceID, cursor, bytesRemaining);
	insertSdnv(segment->dataSegment.offset, cursor, bytesRemaining);
	insertSdnv(segment->dataSegment.length, cursor, bytesRemaining);

	if ( isOrangeEOBDataSegment(*segment) ) {
		insertSdnv(segment->dataSegment.checkpointSerialNumber, cursor, bytesRemaining);
	}

	memcpy(*cursor, segment->dataSegment.data, segment->dataSegment.length);
	(*cursor) += segment->dataSegment.length;
	*bytesRemaining -= segment->dataSegment.length;

	return true;
}

static bool _parseOrangeSegment (char **cursor, int *bytesRemaining, LTPSegment *result) {

	if (!extractSmallSdnv(&(result->dataSegment.clientServiceID), (unsigned char **) cursor, bytesRemaining)) 				return false;
	if (!extractSmallSdnv(&(result->dataSegment.offset), (unsigned char **) cursor, bytesRemaining)) 						return false;
	if (!extractSmallSdnv(&(result->dataSegment.length), (unsigned char **) cursor, bytesRemaining)) 						return false;

	// If checkpoint -> have to parse checkpointSerialNumber & reportSerialNumber
	if ( isOrangeEOBDataSegment(*result) ) {
		if (!extractSmallSdnv(&(result->dataSegment.checkpointSerialNumber), (unsigned char **) cursor, bytesRemaining)) 	return false;
	}

	result->dataSegment.data = (char*) _malloc( result->dataSegment.length );
	if (result->dataSegment.data == NULL)																					return false;

	memcpy(result->dataSegment.data, *cursor, result->dataSegment.length);
	(*cursor) += result->dataSegment.length;
	*bytesRemaining -= result->dataSegment.length;

	return true;
}

static bool _putRedSegment(LTPSegment *segment, char **cursor, int *bytesRemaining) {
	insertSdnv(segment->dataSegment.clientServiceID, cursor, bytesRemaining);
	insertSdnv(segment->dataSegment.offset, cursor, bytesRemaining);
	insertSdnv(segment->dataSegment.length, cursor, bytesRemaining);

	if ( isSegmentCheckpoint(*segment) ) {
		insertSdnv(segment->dataSegment.checkpointSerialNumber, cursor, bytesRemaining);
		insertSdnv(segment->dataSegment.reportSerialNumber, cursor, bytesRemaining);
	}

	memcpy(*cursor, segment->dataSegment.data, segment->dataSegment.length);
	(*cursor) += segment->dataSegment.length;
	*bytesRemaining -= segment->dataSegment.length;

	return true;
}

static bool _parseRedSegment (char **cursor, int *bytesRemaining, LTPSegment *result) {

	if (!extractSmallSdnv(&(result->dataSegment.clientServiceID), (unsigned char **) cursor, bytesRemaining)) 				return false;
	if (!extractSmallSdnv(&(result->dataSegment.offset), (unsigned char **) cursor, bytesRemaining)) 						return false;
	if (!extractSmallSdnv(&(result->dataSegment.length), (unsigned char **) cursor, bytesRemaining)) 						return false;

	// If checkpoint -> have to parse checkpointSerialNumber & reportSerialNumber
	if ( isSegmentCheckpoint(*result) ) {
		if (!extractSmallSdnv(&(result->dataSegment.checkpointSerialNumber), (unsigned char **) cursor, bytesRemaining)) 	return false;
		if (!extractSmallSdnv(&(result->dataSegment.reportSerialNumber), (unsigned char **) cursor, bytesRemaining)) 		return false;
	}

	result->dataSegment.data = (char*) _malloc( result->dataSegment.length );
	if (result->dataSegment.data == NULL)																					return false;

	memcpy(result->dataSegment.data, *cursor, result->dataSegment.length);
	(*cursor) += result->dataSegment.length;
	*bytesRemaining -= result->dataSegment.length;

	return true;
}

static bool _putReportSegment (LTPSegment *segment, char **cursor, int *bytesRemaining) {
	insertSdnv(segment->reportSegment.reportSerialNumber, cursor, bytesRemaining);
	insertSdnv(segment->reportSegment.checkpointSerialNumber, cursor, bytesRemaining);
	insertSdnv(segment->reportSegment.upperBound, cursor, bytesRemaining);
	insertSdnv(segment->reportSegment.lowerBound, cursor, bytesRemaining);

	insertSdnv(segment->reportSegment.receptionClaimCount, cursor, bytesRemaining);
	for(int i = 0; i < segment->reportSegment.receptionClaimCount; i++) {
		insertSdnv(segment->reportSegment.receptionClaims[i].offset, cursor, bytesRemaining);
		insertSdnv(segment->reportSegment.receptionClaims[i].length, cursor, bytesRemaining);
	}

	return true;
}

static bool _parseReportSegment (char **cursor, int *bytesRemaining, LTPSegment *result) {
	if (!extractSmallSdnv(&(result->reportSegment.reportSerialNumber), (unsigned char **) cursor, bytesRemaining)) 				return false;
	if (!extractSmallSdnv(&(result->reportSegment.checkpointSerialNumber), (unsigned char **) cursor, bytesRemaining)) 			return false;
	if (!extractSmallSdnv(&(result->reportSegment.upperBound), (unsigned char **) cursor, bytesRemaining)) 						return false;
	if (!extractSmallSdnv(&(result->reportSegment.lowerBound), (unsigned char **) cursor, bytesRemaining)) 						return false;

	if (!extractSmallSdnv(&(result->reportSegment.receptionClaimCount), (unsigned char **) cursor, bytesRemaining)) 			return false;

	result->reportSegment.receptionClaims = _malloc(sizeof(LTPReportSegmentClaim) * result->reportSegment.receptionClaimCount);

	for(int i = 0; i < result->reportSegment.receptionClaimCount; i++) {
		if (!extractSmallSdnv(&(result->reportSegment.receptionClaims[i].offset), (unsigned char **) cursor, bytesRemaining)) 	return false;
		if (!extractSmallSdnv(&(result->reportSegment.receptionClaims[i].length), (unsigned char **) cursor, bytesRemaining)) 	return false;
	}

	return true;
}

static bool _putReportAckSegment (LTPSegment *segment, char **cursor, int *bytesRemaining) {
	insertSdnv(segment->reportAckSegment.reportSerialNumber, cursor, bytesRemaining);
	return true;
}

static bool _parseReportAckSegment (char **cursor, int *bytesRemaining, LTPSegment *result) {
	if (!extractSmallSdnv(&(result->reportAckSegment.reportSerialNumber), (unsigned char**) cursor, bytesRemaining))			return false;
	return true;
}

static bool _putCancelSegment (LTPSegment *segment, char **cursor, int *bytesRemaining) {
	**cursor = segment->cancelSegment.cancelCode;
	(*cursor)++;	(*bytesRemaining)--;
	return true;
}

static bool _parseCancelSegment (char **cursor, int *bytesRemaining, LTPSegment *result) {
	result->cancelSegment.cancelCode = **cursor;
	(*cursor)++;	(*bytesRemaining)--;
	return true;
}
