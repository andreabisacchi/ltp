/** \file contact.h
 *
 * \brief This file contains the headers of contact functions
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_CONTACT_CONTACT_H_
#define SRC_CONTACT_CONTACT_H_

#include <sys/time.h>

/*!
 * \brief Contact
 */
typedef struct
{
	/**
	 * \brief Sender node (ipn node number)
	 */
	unsigned long long fromNode;
	/**
	 * \brief Receiver node (ipn node number)
	 */
	unsigned long long toNode;
	/**
	 * \brief Start transmit time
	 */
	time_t fromTime;
	/**
	 * \brief Stop transmit time
	 */
	time_t toTime;
	/**
	 * \brief Tx rate (byte/s) 
	 */
	long unsigned int xmitRate;
} Contact;


/**
 * \par Function Name:
 *      createContact
 *
 * \brief  It creates a contact
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 		createContact	(Contact* contact, unsigned long long fromNode, unsigned long long toNode, time_t fromTime, time_t toTime, long unsigned int xmitRate);

/**
 * \par Function Name:
 *      compareContacts
 *
 * \brief  It compares 2 contacts
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return int
 *
 * \retval  0 if equal, not 0 otherwise
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
int 		compareContacts	(Contact *a, Contact *b);


#endif /* SRC_CONTACT_CONTACT_H_ */
