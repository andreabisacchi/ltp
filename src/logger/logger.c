/** \file logger.c
 *
 * \brief This file contains the implementations of the log functions
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include "logger.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

/***** PRIVATE FUNCTIONS *****/
static void _logger_write(FILE* fp, const char *string, va_list argList);

static bool loggerEnabled = false;


void enableLogger() {
	loggerEnabled = true;
}

void disableLogger() {
	loggerEnabled = false;
}

void doLog(const char *string, ...) {
	if ( loggerEnabled ) {
		va_list lp;
		va_start(lp, string);
		_logger_write(stdout, string, lp);
		va_end(lp);
	}
}

/***** PRIVATE FUNCTIONS *****/

static void _logger_write(FILE* fp, const char *string, va_list argList) {
	if (fp == NULL) {
		return;
	}

	vfprintf(fp, string, argList);
	fprintf(fp, "\n");
	fflush(fp);
}


