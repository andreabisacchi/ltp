/** \file range.h
 *
 * \brief This file contains the headers of range functions
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_RANGE_RANGE_H_
#define SRC_RANGE_RANGE_H_

#include <sys/time.h>

/*!
 * \brief Range
 */
typedef struct
{
	/**
	 * \brief Sender node (ipn node number)
	 */
	unsigned long long fromNode;

	/**
	 * \brief Receiver node (ipn node number)
	 */
	unsigned long long toNode;

	/**
	 * \brief Start transmit time
	 */
	time_t fromTime;

	/**
	 * \brief Stop transmit time
	 */
	time_t toTime;

	/**
	 * \brief Distance (in light seconds)
	 */
	long unsigned int seconds;
} Range;


/**
 * \par Function Name:
 *      createRange
 *
 * \brief Creates a range
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  range 			The range is going to be created
 * \param  fromNode 		The from node
 * \param  toNode			The to node
 * \param  fromTime			The from time
 * \param  toTime			The to time
 * \param  seconds			The seconds
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 		createRange		(Range* range, unsigned long long fromNode, unsigned long long toNode, time_t fromTime, time_t toTime, long unsigned int seconds);

/**
 * \par Function Name:
 *      compareRanges
 *
 * \brief Comapres 2 ranges
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return int
 *
 * \retval Returns 0 if are equals. Not 0 otherwise
 *
 * \param  a 		The first range
 * \param  b		The second range
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
int 		compareRanges	(Range *a, Range *b);

#endif /* SRC_RANGE_RANGE_H_ */
