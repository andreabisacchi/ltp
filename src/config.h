/** \file config.h
 *
 * \brief This file contains the defines that can be configured by an expert user
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_CONFIG_CONFIG_H_
#define SRC_CONFIG_CONFIG_H_


/**
 * \brief Max number of retransmissions before giving up
 *
 * \hideinitializer
 */
#define MAX_RETX_NUMBER (5) //10

/**
 * \brief Number of segment which are sent for each signal segment transmitted
 *
 * \hideinitializer
 */
#define SIGNAL_SEGMENTS_COPIES (1)

/**
 * \brief 0 = Disabled. 1 = Enabled.<br>
 * If enabled the number of SIGNAL_SEGMENTS_BURST is divided into RTO time
 *
 * \hideinitializer
 */
#define DISTRIBUTED_COPIES (0)

/**
 * \brief Seconds before checking to reuse terminatedSessionIDs
 *
 * \hideinitializer
 */
#define RECENTLY_CLOSED_LIST_UPDATE_TIME (1)

/**
 * \brief Millis to sleep in congection control before increase the available space
 *
 * \hideinitializer
 */
#define TOKEN_UPDATE_TIME (50)

/**
 * \brief Millis to sleep in timers
 *
 * \hideinitializer
 */
#define TIMER_CHECK_STATE (500)

/**
 * \brief Seconds before trying to update contact plan
 *
 * \hideinitializer
 */
#define CONTACT_PLAN_UPDATE_TIME (600) //60

/**
 * \brief Max claim number is a single Report Segment
 *
 * \hideinitializer
 */
#define MAX_NUMBER_OF_CLAIMS_IN_REPORT_SEGMENT (20)

/**
 * \brief Name of JSON config file
 *
 * \hideinitializer
 */
#define CONFIG_FILE_NAME "spans.json"

/**
 * \brief Listen UDP default port
 *
 * \hideinitializer
 */
#define LISTEN_UDP_DEFAULT_PORT "1113"

/**
 * \brief Max time between two green segments (after that time the session will expire)
 *
 * \hideinitializer
 */
#define SESSION_INTERSEGMENT_MAX_TIME (3)

#ifndef BUILD_FOR_ION
/**
 * \brief Enable (1) to build UniboLTP for ION. Disable (0) otherwise.
 *
 * \hideinitializer
 */
#define BUILD_FOR_ION (1)
#endif
/**
 * \brief Define how handle the session after and during the pause.
 *  1 = All active sessions will be deleted
 *  0 = All active sessions will be paused
 *
 * \hideinitializer
 */
#define DELETE_SESSION_WHEN_SPAN_PAUSED (0)

/**
 * \brief Define the time to set the timer to cancel a too long RX session (all colors)
 *
 * \hideinitializer
 */
#define CANCEL_RX_SESSION_TIME (1000*60*24)

/**
 * \brief Define the time to set the timer to cancel a too long TX red session
 *
 * \hideinitializer
 */
#define CANCEL_TX_RED_SESSION_TIME (1000*60*24)

#endif /* SRC_CONFIG_CONFIG_H_ */
