/** \file sender.h
 *
 * \brief This file contains the headers of the functions used to send LTP segments
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_SENDER_SENDER_H_
#define SRC_SENDER_SENDER_H_

#include "sendStruct.h"


/**
 * \par Function Name:
 *      sendSegment
 *
 * \brief It sends a segment
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  sendStruct 		The struct in which there are the data of the segment to send
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void		sendSegment(SendStruct sendStruct);

/**
 * \par Function Name:
 *      sendSegmentWithHandler
 *
 * \brief It sends a segment and then it calls the handler
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  sendStruct 		The struct in which there are the data of the segment to send
 * \param  handler			The handler
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void		sendSegmentWithHandler(SendStruct sendStruct, void (*handler)(SendStruct) );

/**
 * \par Function Name:
 *      resendSegment
 *
 * \brief It re-sends a segment and calls the handler
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  sendStruct 		The struct in which there are the data of the segment to send
 * \param  handler			The handler
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void 		resendSegment(SendStruct* sendStruct, void (*handler)(SendStruct*));


#endif /* SRC_SENDER_SENDER_H_ */
