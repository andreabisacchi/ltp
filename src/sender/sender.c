/** \file sender.c
 *
 * \brief This file contains the implementation of the functions used to send LTP segments
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/time.h>

#include "sender.h"

#include "../adapters/protocol/lowerProtocol.h"
#include "../adapters/protocol/upperProtocol.h"
#include "../generic/generic.h"
#include "../list/list.h"
#include "../config.h"
#include "../LTPspan/span.h"

/***** PRIVATE FUNCTIONS *****/
static void _sendSegment(LTPSegment* segment, LTPSpan* span, unsigned int ipAddress, unsigned short portNumber);

/*!
 * \brief Send Struct with handler
 */
typedef struct SendStructWithHandler {
	/**
	 * \brief Send struct
	 */
	SendStruct			sendStruct;
	/**
	 * \brief Handler
	 */
	void				(*handler)(SendStruct segmentToSend);
} SendStructWithHandler;

inline void sendSegment(SendStruct sendStruct) {
	sendSegmentWithHandler(sendStruct, NULL);
}

void sendSegmentWithHandler(SendStruct _sendStruct, void (*handler)(SendStruct) ) {
	SendStructWithHandler sendStruct;
	sendStruct.handler = handler;
	sendStruct.sendStruct = _sendStruct;

	LTPSpan* span = sendStruct.sendStruct.span;

	int burst = ( (isSignalSegment(*sendStruct.sendStruct.segment) && DISTRIBUTED_COPIES == 0) ? SIGNAL_SEGMENTS_COPIES : 1 );

	for (int i = 1; i <= burst; i++) {
		_sendSegment(sendStruct.sendStruct.segment, span, sendStruct.sendStruct.ipAddress, sendStruct.sendStruct.portNumber);
	}

	sendStruct.sendStruct.numberOfSent++;
	if (handler != NULL) {
		// Make a copy of the segment
		sendStruct.sendStruct.segment = (LTPSegment*) _malloc(sizeof(LTPSegment));
		memcpy(sendStruct.sendStruct.segment, _sendStruct.segment, sizeof(LTPSegment));

		// Call the handler
		handler(sendStruct.sendStruct); // The handler will dealloc the segment
	}
}

void resendSegment(SendStruct* sendStruct, void (*handler)(SendStruct*)) {
	if ( sendStruct == NULL ) return;

	LTPSpan* span = sendStruct->span;

	int burst = ( (isSignalSegment(*sendStruct->segment) && DISTRIBUTED_COPIES == 0) ? SIGNAL_SEGMENTS_COPIES : 1 );

	for (int i = 1; i <= burst; i++) {
		_sendSegment(sendStruct->segment, span, sendStruct->ipAddress, sendStruct->portNumber);
	}

	sendStruct->numberOfSent++;
	if ( handler != NULL ) {
		handler(sendStruct);
	}
}

/***** PRIVATE FUNCTIONS *****/

static void _sendSegment(LTPSegment* segment, LTPSpan* span, unsigned int ipAddress, unsigned short portNumber) {
	if ( segment == NULL ) return;

	char buffer[LOWER_LEVEL_MAX_PACKET_SIZE];

	memset(&(buffer[0]), '\0', LOWER_LEVEL_MAX_PACKET_SIZE); // XXX: theorically useless

	unsigned int segmentTotalSize = putSegmentInBuffer(segment, &(buffer[0]), LOWER_LEVEL_MAX_PACKET_SIZE);

	if ( span != NULL )
		waitUntillCanSend(span, segmentTotalSize);

	if (!sendPacketToLowerProtocol(span->lowerProtocol, buffer, segmentTotalSize, ipAddress, portNumber)) {
		fprintf(stderr, "Error on sendPacketToLowerProtocol\n");
		//exit(1);
	}
}









