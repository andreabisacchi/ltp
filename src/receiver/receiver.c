/** \file receiver.c
 *
 * \brief This file contains the implementations of the LTP receiver functions
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/time.h>
#include <unistd.h>

#include "receiver.h"

#include "../logger/logger.h"
#include "../adapters/protocol/lowerProtocol.h"
#include "../LTPspan/span.h"
#include "../generic/generic.h"

static bool isReceiverRunning = false;

void *mainReceiver(void *_) {
	LTPSegment receivedSegment;

	isReceiverRunning = true;
	while (isReceiverRunning) {
		char buffer[LOWER_LEVEL_MAX_PACKET_SIZE];
		int bufferLenght;

		//memset(&(buffer[0]), 0, LOWER_LEVEL_MAX_PACKET_SIZE);

		if ( !receivePacketFromLowerProtocol(buffer, &bufferLenght) ) {
			continue; // Timeout
		}
		if (parsePacketIntoSegment(buffer, bufferLenght, &receivedSegment) <= 0) {
			doLog("Data received, but the LTP segment cannot be parsed.");
			continue;
		}

		signalSpanNewSegmentReceived(&receivedSegment);
	}

	doLog("Receiver ending...");
	return NULL;
}

void stopReceiver() {
	isReceiverRunning = false;
	doLog("Stopping receiver");
}
