/** \file sdnv.c
 *
 * \brief This file contains the implementations of the functions to convert from/to SDNV values. This functions are partially copied from ION code. Please check out ION code: https://sourceforge.net/projects/ion-dtn/
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "sdnv.h"

/*!
 * \brief Sdnv
 */
typedef struct
{
	/**
	 * \brief The length
	 */
	int		length;
	/**
	 * \brief The text
	 */
	unsigned char	text[10];
} Sdnv;

static int _decodeSdnv(unsigned long long *val, unsigned char *sdnvTxt);
static void	_encodeSdnv(Sdnv *sdnv, unsigned long long val);

void insertSdnv(unsigned long long value, char** to, int* remnant)
{
	Sdnv sdnv;
	_encodeSdnv(&sdnv, value);
	memcpy(*to, sdnv.text, sdnv.length);
	*to += sdnv.length;
	*remnant -= sdnv.length;
}

int	extractSdnv(unsigned long long *into, unsigned char **from, int *remnant)
{
	int sdnvLength;

	if (into == NULL || from == NULL || remnant == NULL)
	{
		fprintf(stderr, "SDNV error - NULL value found\n");
		return 0;
	}

	if (*remnant < 1)
	{
		fprintf(stderr, "SDNV error\n");
		return 0;
	}

	sdnvLength = _decodeSdnv(into, *from);
	if (sdnvLength < 1)
	{
		fprintf(stderr, "SDNV error\n");
		return 0;
	}

	(*from) += sdnvLength;
	(*remnant) -= sdnvLength;
	return sdnvLength;
}

int	extractSmallSdnv(unsigned int *into, unsigned char **from, int *remnant)
{
	int					sdnvLength;
	unsigned long long	val;

	if (into == NULL || from == NULL || remnant == NULL)
	{
		fprintf(stderr, "SDNV error - NULL value found\n");
		return 0;
	}

	if (*remnant < 1)
	{
		fprintf(stderr, "SDNV error\n");
		return 0;
	}

	sdnvLength = _decodeSdnv(&val, *from);
	if (sdnvLength < 1)
	{
		fprintf(stderr, "SDNV error\n");
		return 0;
	}

	*into = val;				/*	Truncate.	*/
	(*from) += sdnvLength;
	(*remnant) -= sdnvLength;
	return sdnvLength;
}



/***** PRIVATE FUNCTIONS *****/

static int _decodeSdnv(unsigned long long *val, unsigned char *sdnvTxt)
{
	int				sdnvLength = 0;
	unsigned char	*cursor;

	*val = 0;
	cursor = sdnvTxt;

	while (1)
	{
		sdnvLength++;
		if (sdnvLength > 10)
		{
			return 0;	/*	More than 70 bits.	*/
		}

		/*	Shift numeric value 7 bits to the left (that
		 *	is, multiply by 128) to make room for 7 bits
		 *	of SDNV byte value.				*/

		*val <<= 7;

		/*	Insert SDNV text byte value (with its high-
		 *	order bit masked off) as low-order 7 bits of
		 *	the numeric value.				*/

		*val |= (*cursor & 0x7f);

		/*	If this SDNV text byte's high-order bit is
		 *	1, then it's the last byte of the SDNV text.	*/

		if (((*cursor) & 0x80) == 0)	/*	Last SDNV byte.	*/
		{
			return sdnvLength;
		}

		/*	Haven't reached the end of the SDNV yet.	*/

		cursor++;
	}
}

static void	_encodeSdnv(Sdnv *sdnv, unsigned long long val)
{
	static unsigned long long	sdnvMask = ((unsigned long long) -1) / 128;
	unsigned long long		remnant = val;
	char		result[10];
	int		length = 1;
	unsigned char	*text;

	/*	Thanks to Cheol Koo of KARI for optimizing this
	 *	function.  29 August 2019				*/

	if (sdnv == NULL)
		return;

	/*	First extract the value of what will become the low-
	 *	order byte of the SDNV text; its high-order bit is 0.	*/

	result[0] = remnant & (unsigned long long) 0x7f;
	remnant = (remnant >> 7) & sdnvMask;

	/*	Now extract the values of all remaining bytes, in
	 *	increasing order, setting high-order bit to 1 for
	 *	each one.  The results array will contain the values
	 *	of the bytes of the SDNV text in reverse order.		*/

	while (remnant)
	{
		result[length] = (remnant & (unsigned long long) 0x7f) | 0x80;
		remnant = (remnant >> 7) & sdnvMask;
		length++;
	}

	/*	Now copy the extracted values into the text of the
	 *	SDNV, starting with the highest-order value.		*/

	sdnv->length = length;
	text = sdnv->text;
	while (length)
	{
		length--;
		*text = result[length];
		text++;
	}
}
