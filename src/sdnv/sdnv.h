/** \file sdnv.h
 *
 * \brief This file contains the defines the functions to convert from/to SDNV values. This functions are partially copied from ION code. Please check out ION code: https://sourceforge.net/projects/ion-dtn/
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef SRC_SDNV_SDNV_H_
#define SRC_SDNV_SDNV_H_


/**
 * \par Function Name:
 *      insertSdnv
 *
 * \brief Insert the value into the buffer as a SDNV value
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return void
 *
 * \param  value 			The value to add
 * \param  to				The buffer
 * \param remnant			The buffer remnant bytes
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
void insertSdnv(unsigned long long value, char** to, int* remnant);

/**
 * \par Function Name:
 *      extractSdnv
 *
 * \brief Extracts the SDNV value
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return int
 *
 * \retval The number of bytes it converted
 *
 * \param  into 			The destination value (converted)
 * \param  from				The buffer from which it parses SDNV value
 * \param  remnant			The remnant bytes
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
int	extractSdnv(unsigned long long *into, unsigned char **from, int *remnant);

/**
 * \par Function Name:
 *      extractSmallSdnv
 *
 * \brief Extracts the SDNV value
 *
 *
 * \par Date Written:
 *      11/01/21
 *
 * \return int
 *
 * \retval The number of bytes it converted
 *
 * \param  into 			The destination value (converted)
 * \param  from				The buffer from which it parses SDNV value
 * \param  remnant			The remnant bytes
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  11/01/21 | A. Bisacchi     |  Initial Implementation and documentation.
 *****************************************************************************/
int	extractSmallSdnv(unsigned int *into, unsigned char **from, int *remnant);


#endif /* SRC_SDNV_SDNV_H_ */
