/** \file timerStruct.h
 *
 * \brief This file contains the defininition of the Timer structure
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 * \authors Davide Filoni, davide.filoni2@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *
 * \par Change made:
 *			remove from the structure restart and thread
 *
 ***********************************************/

#ifndef SRC_TIMER_TIMERSTRUCT_H_
#define SRC_TIMER_TIMERSTRUCT_H_

#include <stdbool.h>


/**
 * \brief Timer ID
 */
typedef unsigned int TimerID;

/**
 * \brief Timer Handler
 */
typedef void(*TimerHandler)(TimerID,void*);

/*!
 * \brief Timer
 */
typedef struct {
	/**
	 * \brief The identifier of the timer
	 */
	TimerID					timerID;

	/**
	 * \brief It tells if this timer must be stopped (deleted)
	 */
	bool					stopped;

	/**
	 * \brief It tells if this timer is paused
	 */
	bool					paused;

	/**
	* \brief It tells if this timer is paused
	*/
	bool					restart;

	/**
	 * \brief The remaining time to sleep (in ms)
	 */
	long 			millisToSleep;

	/**
	 * \brief The initial time to sleep 
	 */
	long 			startMillisToSleep;


	/**
	 * \brief The handler to be called at timer timeout
	 */
	TimerHandler 			handler;

	/**
	 * \brief The data to be passed to the handler
	 */
	void* 					data;
} Timer;


#endif /* SRC_TIMER_TIMERSTRUCT_H_ */
