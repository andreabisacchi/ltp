/** \file timer.c
 *
 * \brief This file contains the Timer implementation
 *
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \par License
 *
 *    This file is part of UniboLTP.                                             <br>
 *                                                                               <br>
 *    UniboLTP is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.                                        <br>
 *    UniboLTP is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.                               <br>
 *                                                                               <br>
 *    You should have received a copy of the GNU General Public License
 *    along with UniboLTP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 * \authors Davide Filoni, davide.filoni2@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include "timer.h"

#include <pthread.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "../list/list.h"
#include "../generic/generic.h"
#include "../config.h"


static int _findTimerID(void* _timerID, size_t _timerIDSize, void* _timer, size_t _timerSize);
static Timer* _getTimerFromTimerID(TimerID timerID);
static void* _timerController(void *_);
static void _handlerTimer(void* _timer, size_t size);
static bool _isStoppedTimer(void* _timer, size_t size);

static pthread_t timerThread;

static bool isInitialized = false;
static bool isTimerMainRunning = false;


static pthread_mutex_t lockAllTimers;
static List allTimers = empty_list;
static TimerID nextTimerID = 1;
static long millisToSubstract=0;

TimerID startTimer (unsigned int millisToSleep, TimerHandler handler, void* data) {
	Timer timer;



	timer.timerID = nextTimerID;
	nextTimerID++; if (nextTimerID==0) nextTimerID++; // To avoid overflow

	timer.stopped=false;
	timer.paused=false;
	timer.restart=false;
	timer.millisToSleep = millisToSleep;
	timer.startMillisToSleep = millisToSleep;
	timer.handler = handler;
	timer.data = data;

	pthread_mutex_lock(&lockAllTimers);
	list_push_front(&allTimers, &timer, sizeof(Timer));
	pthread_mutex_unlock(&lockAllTimers);

	return timer.timerID;
}

bool stopTimer(TimerID timerID) {
	if ( timerID == 0 )
		return false;

	Timer* timer = _getTimerFromTimerID(timerID);

	if ( timer != NULL  ) {
		timer->stopped = true;
		return true;
	}

	return false;
}

void pauseTimer(TimerID timerID) {
	Timer* timer = _getTimerFromTimerID(timerID);
	if ( timer != NULL  )
		timer->paused = true;
}

void resumeTimer(TimerID timerID) {
	Timer* timer = _getTimerFromTimerID(timerID);
	if ( timer != NULL )
		timer->paused = false;
}


void restartTimer (TimerID timerID) {
	Timer* timer = _getTimerFromTimerID(timerID);
	if ( timer != NULL )
		timer->restart = true;
}

void initTimer(){

	pthread_mutexattr_t ma;
	pthread_mutexattr_init(&ma);
	pthread_mutexattr_settype(&ma, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&lockAllTimers, &ma);

	isInitialized = true;
	isTimerMainRunning = true;

	pthread_attr_t pthreadAttribute;
	pthread_attr_init(&pthreadAttribute);
	pthread_attr_setdetachstate(&pthreadAttribute, PTHREAD_CREATE_DETACHED);
	pthread_create(&timerThread, &pthreadAttribute, _timerController, NULL);

}


void destroyTimer(){

	isTimerMainRunning = false;
	pthread_mutex_lock(&lockAllTimers);
	list_destroy(&allTimers);
	pthread_cancel(timerThread);
	pthread_mutex_unlock(&lockAllTimers);
	pthread_mutex_destroy(&lockAllTimers);

}

/***** PRIVATE FUNCTIONS *****/


static Timer* _getTimerFromTimerID(TimerID timerID) {

	pthread_mutex_lock(&lockAllTimers);
	Timer* result = (Timer*) list_get_pointer_data(allTimers, &timerID, sizeof(timerID), _findTimerID);
	pthread_mutex_unlock(&lockAllTimers);
	return result;
}

static int _findTimerID(void* _timerID, size_t _timerIDSize, void* _timer, size_t _timerSize) {
	TimerID* timerID = (TimerID*) _timerID;
	Timer* timer = (Timer*) _timer;
	if ( timer->timerID == *timerID )
		return 0;
	else
		return 1;
}

static void* _timerController(void *_) {

		struct timeval tv;
		struct timezone tz;
		long millis_0;
		long millis_1;

		millisToSubstract = MICROSEC_TO_SLEEP/1000;

		while(isTimerMainRunning){

			gettimeofday(&tv, &tz);
			millis_0 = tv.tv_sec*1000+tv.tv_usec/1000;

			usleep(MICROSEC_TO_SLEEP);

			if(allTimers != empty_list){

				pthread_mutex_lock(&lockAllTimers);
				list_for_each(allTimers,_handlerTimer);
				list_remove_if(&allTimers,_isStoppedTimer);
				pthread_mutex_unlock(&lockAllTimers);
			}

			gettimeofday(&tv, &tz);
			millis_1 = tv.tv_sec*1000+tv.tv_usec/1000;

			millisToSubstract = millis_1-millis_0;
		}

		return NULL;
}

static void _handlerTimer(void* _timer, size_t size){
	Timer *timer = (Timer*) _timer;

	if(timer->stopped) return;

	else if(timer->restart) {
		timer->millisToSleep = timer->startMillisToSleep;
		timer->restart=false;
	}
	else if(!(timer->paused)){

		timer->millisToSleep -= millisToSubstract;

		if(timer->millisToSleep <=0){ // timer expited
			TimerHandler handler = timer->handler;
			void* timerData = timer->data;
			TimerID timerID = timer->timerID;

			if ( !(timer->stopped) ) { // Make sure it's still valid
					timer->stopped = true;
					handler(timerID, timerData); // Go for handle
				}
		}
	}
}

static bool _isStoppedTimer(void* _timer, size_t size){
	Timer *timer =(Timer*) _timer;
	if(timer->stopped) return true;
	return false;
}
