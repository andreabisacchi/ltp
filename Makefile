PROJECT_ROOT = $(dir $(abspath $(lastword $(MAKEFILE_LIST))))/src/

ION_DIR=
DEBUG=0
BP=bpv6
INSTALL_PATH=/usr/local/bin

ifeq ($(ION_DIR),)
all: help
else
all: bin
endif

C_FILES  = $(PROJECT_ROOT)/*.c
C_FILES += $(PROJECT_ROOT)/receiver/*.c
C_FILES += $(PROJECT_ROOT)/sender/*.c
C_FILES += $(PROJECT_ROOT)/adapters/protocol/*.c
C_FILES += $(PROJECT_ROOT)/adapters/protocol/lowerProtocols/*.c
C_FILES += $(PROJECT_ROOT)/adapters/protocol/upperProtocol/*.c
C_FILES += $(PROJECT_ROOT)/sdnv/*.c
C_FILES += $(PROJECT_ROOT)/LTPsegment/*.c
C_FILES += $(PROJECT_ROOT)/LTPsession/*.c
C_FILES += $(PROJECT_ROOT)/LTPspan/*.c
C_FILES += $(PROJECT_ROOT)/contact/*.c
C_FILES += $(PROJECT_ROOT)/range/*.c
C_FILES += $(PROJECT_ROOT)/list/*.c
C_FILES += $(PROJECT_ROOT)/timer/*.c
C_FILES += $(PROJECT_ROOT)/generic/*.c
C_FILES += $(PROJECT_ROOT)/spanFile/*.c
C_FILES += $(PROJECT_ROOT)/logger/*.c


BIN_NAME = UniboLTP

INSTALLED=$(wildcard $(INSTALL_PATH)/$(BIN_NAME))

LDFLAGS = -lpthread -lbp -lici -lm -ljson-c

ifeq ($(DEBUG),0)
CFLAGS = -O2
else
CFLAGS = -g -fno-inline -O0
endif

CFLAGS += -rdynamic -I "$(ION_DIR)" -I "$(ION_DIR)/$(BP)/include" -I "$(ION_DIR)/$(BP)/library" -I "$(ION_DIR)/ici/include"

OBJS = *.o

bin:
# Check if ION_DIR is correct, if not exists print error & exit
	@if test ! -d "$(ION_DIR)"; then echo "Directory \"$(ION_DIR)\" not exists. Please check out ION_DIR variable"; false; else true; fi
	@if test ! -d "$(ION_DIR)/$(BP)"; then echo "Directory \"$(ION_DIR)/$(BP)\" not exists. Please check out BP variable"; false; else true; fi

	$(CC) $(CFLAGS) $(C_FILES) $(LDFLAGS) -o $(BIN_NAME) $^

uninstall:
	@if test `echo $(INSTALLED) | wc -w` -eq 1 -a -f "$(INSTALLED)"; then rm -rf $(INSTALLED); else echo "NOT INSTALLED"; fi

install:
	cp $(BIN_NAME) $(INSTALL_PATH)/

clean:
	rm -fr $(BIN_NAME) $(OBJS)

help:
	@echo "Usage: make ION_DIR=<ion_dir>"
	@echo "To compile with debug symbols add DEBUG=1"
	@echo "To change the BP version use BP=bpv7, BP=bpv6 is default. For old ION versions use BP=bp"

